#!/usr/bin/env python

"""
Thirring model (one dimension, one flavor, staggered).
"""

import itertools
import numbers
import picos
import numpy as np
import sys
from scipy import sparse
matrix = sparse.dok_matrix
kron = sparse.kron
eye = sparse.eye

from algebra import *

VERBOSE = False

############
# Arguments

# Size
L = int(sys.argv[1])
m = float(sys.argv[2])
mu = float(sys.argv[3])
g2 = float(sys.argv[4])

# We have two families of anticommuting operators, each with one operator at
# every site. However, these families are pairwise-related, so that once we
# construct N anticommuting operators, the other N come by taking the
# transpose.
P = [matrix(eye(1)*(1.+0j))]*L

sx = matrix([[0,1],[1,0]])
sz = matrix([[1,0],[0,-1]])

for n in range(L):
    for i in range(L):
        if n < i:
            P[i] = kron(P[i], sz, format='dok')
        elif n == i:
            P[i] = kron(P[i], matrix([[0,0],[1,0]]), format='dok')
        else:
            P[i] = kron(P[i], eye(2), format='dok')

Pd = [matrix(o.H) for o in P]



##############
# Hamiltonian

H = 0*Operator()
for x in range(L):
    y = (x+1)%L
    a = BasisOperator(x)
    b = BasisOperator(y)
    H += m * (-1)**x * a.dag()*a
    H -= mu * a.dag()*a
    if L > 1:
        H += (-1)**x * (a.dag()*b + b.dag()*a)/2
        H -= g2 * a*a.dag()*b.dag()*b

N = 0*Operator()
for x in range(L):
    a = BasisOperator(x)
    N += a.dag()*a

def make_basis(ops):
    basis = set()

    for op in ops:
        for b, _ in op:
            basis.add(b)

    basis.update({b.dag() for b in basis})
    basis = list(basis)

    if VERBOSE:
        print(f'Operators: {len(basis)}')
        print(basis)

    return basis




def check(basis):
    ########
    # Solve

    evars = {op: picos.ComplexVariable(f'<{op}>') for op in basis}
    evars.update({b: picos.ComplexVariable(f'<{b}>') for b,c in H})
    for x in range(L):
        for y in range(L):
            b = BasisOperator(x).dag()*BasisOperator(y)
            evars[b] = picos.ComplexVariable(f'<{b}>')

    def expect(op):
        if isinstance(op,BasisOperator):
            return evars[op]
        r = picos.Constant(0)
        for (b,c) in op:
            r += c*evars[b]
        return r

    problem = picos.Problem(verbosity=0)
    problem.set_objective('min', expect(H).real)
    M = picos.HermitianVariable("M", len(basis))
    problem += M >> 0
    for (i1, b1) in enumerate(basis):
        for (i2, b2) in enumerate(basis):
            try:
                problem += M[i1,i2] == expect(b1.dag()*b2)
                diff=(b1.dag()*b2).matrix() - (b1.dag().matrix() @ b2.matrix())
                s = abs(diff).sum()
                if s > 1e-5:
                    print(b1, '     ', b2, '      ', b1.dag()*b2)
            except KeyError as missing:
                if False:
                    print('Missing: ', b1.dag()*b2)

########
# Basis
ops = {H}
#ops = set()
ops.update({BasisOperator(x) for x in range(L)})
ops.update({BasisOperator(x)*BasisOperator(x).dag() for x in range(L)})
basis = make_basis(ops)
check(basis)

