#!/usr/bin/env python

"""
Thirring model (one dimension, one flavor, staggered).
"""

import itertools
import numbers
import picos
import numpy as np
from scipy.special import factorial, hermite
import sys

from algebra import *

VERBOSE = False

############
# Arguments

# Size
L = int(sys.argv[1])
m = float(sys.argv[2])
mu = float(sys.argv[3])
g2 = float(sys.argv[4])

level = int(sys.argv[5])


########################
# Hamiltonian (density)

H = 0*Operator()
if True:
    a = BasisOperator(0)
    b = BasisOperator(1)
    c = BasisOperator(2)
    H += m*a.dag()*a
    H -= m*b.dag()*b
    H -= mu*a.dag()*a
    H -= mu*b.dag()*b
    H += (a.dag()*b + b.dag()*a)/2
    H -= g2 * a*a.dag()*b.dag()*b
    H -= (b.dag()*c + c.dag()*b)/2
    H -= g2 * b*b.dag()*c.dag()*c

if False:
    a = BasisOperator(0)
    b = BasisOperator(1)
    H += m*a.dag()*a
    H -= m*b.dag()*b
    H -= mu*a.dag()*a
    H -= mu*b.dag()*b
    H += a.dag()*b + b.dag()*a
    H -= 2*g2 * a*a.dag()*b.dag()*b

if False:
    a = BasisOperator(0)
    b = BasisOperator(1)
    c = BasisOperator(-1)
    d = BasisOperator(2)
    H += m * (a.dag()*a - b.dag()*b + 0.5*(d.dag()*d - c.dag()*c))
    H -= mu * (a.dag()*a + b.dag()*b + 0.5*(d.dag()*d + c.dag()*c))
    H += (a.dag()*b + b.dag()*a)/2
    H -= (b.dag()*d + d.dag()*b)/2
    H -= (a.dag()*c + c.dag()*a)/2
    H -= g2 * a*a.dag()*b.dag()*b
    H -= g2 * b*b.dag()*d.dag()*d
    H -= g2 * a*a.dag()*c.dag()*c

N = -1*Operator()
for x in range(2):
    a = BasisOperator(x)
    N += a.dag()*a

def make_basis(ops):
    basis = set()

    for op in ops:
        for b, _ in op:
            basis.add(b)

    basis.update({b.dag() for b in basis})
    basis = list(basis)

    if VERBOSE:
        print(f'Operators: {len(basis)}')
        print(basis)

    return basis


def bound(basis):
    ########
    # Solve

    evars = {op: picos.ComplexVariable(f'<{op}>') for op in basis}
    evars.update({b: picos.ComplexVariable(f'<{b}>') for b,c in H})
    for x in range(L):
        for y in range(L):
            b = BasisOperator(x).dag()*BasisOperator(y)
            evars[b] = picos.ComplexVariable(f'<{b}>')

    def expect(op):
        if isinstance(op,BasisOperator):
            return evars[op]
        r = picos.Constant(0)
        for (b,c) in op:
            r += c*evars[b]
        return r

    problem = picos.Problem(verbosity=0)
    problem.set_objective('min', expect(H).real)
    M = picos.HermitianVariable("M", len(basis))
    problem += M >> 0
    for (i1, b1) in enumerate(basis):
        for (i2, b2) in enumerate(basis):
            try:
                problem += M[i1,i2] == expect(b1.dag()*b2)
            except KeyError as missing:
                if False:
                    print('Missing: ', b1.dag()*b2)
    for b1 in evars:
        for b2 in evars:
            for x in range(-L,L+2):
                if b1.translate(x) == b2:
                    if x % 2 == 0 and x != 0:
                        problem += expect(b1) == expect(b2)
    problem += expect(Operator()) == 1.
    #print(problem)
    if VERBOSE:
        print('Solving...')
    sol = problem.solve(solver='mosek', primals=True, duals=None)
    #print(sol.reported_value, expect(N).real)
    return sol.reported_value, float(expect(N).real)
#print(len(basis), sol.reported_value)

########
# Basis
r = range(-L,L+2)

ops = set()
ops.update({BasisOperator(x) for x in r})

if level >= 0:
    ops.update({BasisOperator(x)*BasisOperator(x).dag() for x in r})

if level >= 1:
    ops.update({BasisOperator(x)*BasisOperator(x+1).dag() for x in r})

if level >= 2:
    ops.update({BasisOperator(x).dag()*BasisOperator(x)*BasisOperator(x+1).dag()*BasisOperator(x+1) for x in r})

if level >= 3:
    ops.update({BasisOperator(x).dag()*BasisOperator(x+1)*BasisOperator(x+2).dag()*BasisOperator(x+2) for x in range(L)})

if False:
    if level >= 1:
        ops.update({BasisOperator(x)*BasisOperator(y).dag() for x in r for y in r})

    if level >= 2:
        ops.update({BasisOperator(x).dag()*BasisOperator(x)*BasisOperator(y).dag()*BasisOperator(y) for x in r for y in r})

basis = make_basis(ops)

print(*bound(basis))

