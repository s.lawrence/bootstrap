#!/usr/bin/env python

"""
Thirring model (one dimension, one flavor, staggered).
"""

import itertools
import numbers
import picos
import numpy as np
import sys

from algebra import *

VERBOSE = False

############
# Arguments

# Size
L = int(sys.argv[1])
m = float(sys.argv[2])
mu = float(sys.argv[3])
g2 = float(sys.argv[4])

level = int(sys.argv[5])

##############
# Hamiltonian

H = 0*Operator()
for x in range(L):
    y = (x+1)%L
    a = BasisOperator(x)
    b = BasisOperator(y)
    H += m * (-1)**x * a.dag()*a
    H -= mu * a.dag()*a
    if L > 1:
        H += (-1)**x * (a.dag()*b + b.dag()*a)/2
        H -= g2 * a*a.dag()*b.dag()*b

N = 0*Operator()
for x in range(L):
    a = BasisOperator(x)
    N += a.dag()*a

def make_basis(ops):
    basis = set()

    for op in ops:
        for b, _ in op:
            basis.add(b)

    basis.update({b.dag() for b in basis})
    basis = list(basis)

    if VERBOSE:
        print(f'Operators: {len(basis)}')
        print(basis)

    return basis


def bound(basis):

    ########
    # Solve

    evars = {op: picos.ComplexVariable(f'<{op}>') for op in basis}
    evars.update({b: picos.ComplexVariable(f'<{b}>') for b,c in H})
    for x in range(L):
        for y in range(L):
            b = BasisOperator(x).dag()*BasisOperator(y)
            evars[b] = picos.ComplexVariable(f'<{b}>')

    def expect(op):
        if isinstance(op,BasisOperator):
            return evars[op]
        r = picos.Constant(0)
        for (b,c) in op:
            r += c*evars[b]
        return r

    problem = picos.Problem(verbosity=0)
    problem.set_objective('min', expect(H).real)
    M = picos.HermitianVariable("M", len(basis))
    problem += M >> 0
    for (i1, b1) in enumerate(basis):
        for (i2, b2) in enumerate(basis):
            try:
                problem += M[i1,i2] == expect(b1.dag()*b2)
            except KeyError as missing:
                if False:
                    print('Missing: ', b1.dag()*b2)
    problem += expect(Operator()) == 1.
    problem.options["*_tol"] = None # Use MOSEK defaults.
    if VERBOSE:
        print('Solving...')
    sol = problem.solve(solver='mosek', primals=True, duals=None)
    #print(sol.reported_value, expect(N).real)
    return sol.reported_value, float(expect(N).real)-L//2
#print(len(basis), sol.reported_value)

########
# Basis
#ops = {H}
ops = set()
ops.update({BasisOperator(x) for x in range(L)})
ops.update({BasisOperator(x)*BasisOperator(x).dag() for x in range(L)})
#ops.update({BasisOperator(x)*BasisOperator(y) for x in range(L) for y in range(L)})
#ops.update({BasisOperator(x).dag()*BasisOperator(y)*BasisOperator(z) for x in range(L) for y in range(L) for z in range(L)})
#ops.update({BasisOperator(0).dag()*BasisOperator(y)*BasisOperator(z) for x in range(L) for y in range(L) for z in range(L)})

if False:
    ops.update({BasisOperator(0).dag()*BasisOperator(2).dag()*BasisOperator(y)*BasisOperator(z) for y in range(L) for z in range(L)})
    ops.update({BasisOperator(1).dag()*BasisOperator(3).dag()*BasisOperator(y)*BasisOperator(z) for y in range(L) for z in range(L)})

if level >= 1:
    ops.update({BasisOperator(x)*BasisOperator((x+1)%L).dag() for x in range(L)})

if level >= 2:
    ops.update({BasisOperator(x).dag()*BasisOperator(x)*BasisOperator((x+1)%L).dag()*BasisOperator((x+1)%L) for x in range(L)})

if level >= 3:
    ops.update({BasisOperator(x).dag()*BasisOperator((x+1)%L)*BasisOperator((x+2)%L).dag()*BasisOperator((x+2)%L) for x in range(L)})
    #ops.update({BasisOperator(x)*BasisOperator((x+1)%L)*BasisOperator((x+2)%L) for x in range(L)})
    #ops.update({BasisOperator(x).dag()*BasisOperator(x)*BasisOperator((x+1)%L).dag()*BasisOperator((x+1)%L)*BasisOperator((x+2)%L).dag()*BasisOperator((x+2)%L) for x in range(L)})
    #ops.update({BasisOperator(x)*BasisOperator((x+2)%L) for x in range(L)})
    #ops.update({BasisOperator(x)*BasisOperator((x+2)%L).dag() for x in range(L)})
    #ops.update({BasisOperator(x).dag()*BasisOperator(x)*BasisOperator((x+2)%L).dag()*BasisOperator((x+2)%L) for x in range(L)})
    #ops.update({BasisOperator(x)*BasisOperator((x+3)%L).dag() for x in range(L)})
    #ops.update({BasisOperator(x).dag()*BasisOperator(x)*BasisOperator((x+3)%L).dag()*BasisOperator((x+3)%L) for x in range(L)})
    #ops.update({BasisOperator(x).dag()*BasisOperator(x+1)*BasisOperator((x+2)%L).dag()*BasisOperator((x+3)%L) for x in range(L)})
    #ops.update({BasisOperator(x).dag()*BasisOperator(x+2)*BasisOperator((x+1)%L).dag()*BasisOperator((x+3)%L) for x in range(L)})
    #ops.update({BasisOperator(x)*BasisOperator(y).dag() for x in range(L) for y in range(L)})
    #ops.update({BasisOperator(x).dag()*BasisOperator(y).dag() for x in range(L) for y in range(L)})
    #ops.update({BasisOperator(x)*BasisOperator(y) for x in range(L) for y in range(L)})

if level >= 4:
    ops.update({BasisOperator(x).dag()*BasisOperator(x)*BasisOperator(y).dag()*BasisOperator(y) for x in range(L) for y in range(L)})

if False:
    # This didn't help at all
    ops.update({BasisOperator(x)*BasisOperator(y).dag()*BasisOperator(y) for x in range(L) for y in range(L)})

if False:
    ops.update({BasisOperator(x)*BasisOperator(y).dag()*BasisOperator(0)*BasisOperator(0).dag() for x in range(L) for y in range(L)})
    #ops.update({BasisOperator(x)*BasisOperator(y).dag()*BasisOperator(0)*BasisOperator(1).dag() for x in range(L) for y in range(L)})

basis = make_basis(ops)
#print(basis)
#print(len(basis))
print(*bound(basis))

if False:
    best, _ = bound(basis)
    print(best)

    for op in basis:
        b = basis.copy()
        b.remove(op)
        this, _ = bound(b)
        print(op, this, best-this)

