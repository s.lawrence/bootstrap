import numbers

class BasisOperator:
    """
    Second-quantized operators.

    Each basis operator is a product of creation and annihilation operators.

    The list of modes annihilated is stored in `annihilate`, and the list of
    modes created is stored in `create`. All annihilation operators are applied
    first (in increasing order of mode number), and then all creation operators
    (in the opposite order) follow. As a result the adjoint of any
    BasisOperator is itself a BasisOperator, without any sign.
    """
    def __init__(self, n=None):
        """
        If `n` is specified, the operator is the annihilation operator of the
        given mode. Otherwise, the operator represented is the identity.
        """
        self.create = set()
        if n is None:
            self.annihilate = set()
        else:
            self.annihilate = {n}

    def translate(self, x):
        r = BasisOperator()
        r.create = {n+x for n in self.create}
        r.annihilate = {n+x for n in self.annihilate}
        return r

    def __repr__(self):
        return repr(self.create) + repr(self.annihilate)

    def __hash__(self):
        return hash(repr(self))

    def __eq__(self, o):
        return repr(self) == repr(o)

    def dag(self):
        r = BasisOperator()
        r.create = self.annihilate.copy()
        r.annihilate = self.create.copy()
        return r

    def __neg__(self):
        return -1*self

    def __add__(self, op):
        return Operator(self) + Operator(op)

    def __sub__(self, op):
        return Operator(self) - Operator(op)

    def __mul__(self, op):
        if isinstance(op, Operator):
            return Operator(self)*op
        assert isinstance(op, BasisOperator)
        # We approach this recursively. Only if the right operator (`op`) has
        # zero or one creation operators, the multiplication can be done
        # immediately.
        if len(op.create) > 1:
            # Recurse! Split the operator into two parts.
            m = list(sorted(op.create))[0]
            op1 = BasisOperator(m).dag()
            op2 = op.copy()
            op2.create.discard(m)
            # Multiply the first, then the second, then return.
            return (self*op1)*op2

        if len(op.create) == 1 and list(sorted(op.create))[0] in self.annihilate:
            # Now there are two terms.
            m = list(sorted(op.create))[0]
            
            # The number of swaps needed to get the creation and annihilation
            # operators to be adjacent.
            sign1 = (-1)**len(list(filter(lambda n: n<m,self.annihilate)))
            self1 = self.copy()
            self1.annihilate.discard(m)
            op1 = op.copy()
            op1.create.discard(m)
            term1 = sign1 * self1*op1

            # In the second term, the two operators just pick up a minus sign
            # in the usual way. This term only exists if the creation operator
            # isn't already present in `self`.
            if m in self.create:
                return term1
            sign2 = (-1)**len(self.annihilate)
            sign2 *= (-1)**len(list(filter(lambda n: n>m,self.create)))
            self2 = self.copy()
            self2.create.add(m)
            op2 = op.copy()
            op2.create.discard(m)
            term2 = sign2 * self2*op2
            return term1+term2

        if len(self.create.intersection(op.create)) > 0 or len(self.annihilate.intersection(op.annihilate)) > 0:
            return Operator()*0

        r = self.copy()
        sign = 1

        # Pull `op.create` in.
        for n in sorted(op.create):
            r.create.add(n)
            sign *= (-1)**len(self.annihilate)
            sign *= (-1)**len(list(filter(lambda m: m>n,self.create)))

        # Now pull `op.annihilate` in. This just means counting the number of
        # swaps required.
        r.annihilate.update(op.annihilate)
        sign *= (-1)**sum(len(list(filter(lambda m: m<n, self.annihilate))) for n in sorted(op.annihilate))
        return sign*r
 
    def __rmul__(self, x):
        if x == 1.:
            return self
        return Operator(self)*x

    def __str__(self):
        creates = ','.join(map(str,self.create))
        annihilates = ','.join(map(str,self.annihilate))
        return f'create({creates})*annihilate({annihilates})'

    def copy(self):
        r = BasisOperator()
        r.create = self.create.copy()
        r.annihilate = self.annihilate.copy()
        return r

    def __iter__(self):
        yield self, 1.

    def matrix(self):
        M = eye(2**L)
        for i in sorted(self.create):
            M = M @ Pd[i]
        for i in reversed(sorted(self.annihilate)):
            M = M @ P[i]
        return M

class Operator:
    def __init__(self, b=BasisOperator()):
        if isinstance(b,BasisOperator):
            self.terms = {b: 1.}
        else:
            self.terms = b.terms.copy()

    def copy(self):
        r = Operator()
        r.terms = self.terms.copy()
        return r

    def translate(self, x):
        r = Operator()
        r.terms = {o.translate(x): c for (o,c) in self.terms.items()}
        return r

    def matrix(self):
        M = 0.*eye(2**L)
        for (o,c) in self.terms.items():
            M += c*o.matrix()
        return M

    def dag(self):
        r = Operator()
        r.terms = {o.dag(): c.conjugate() for (o,c) in self.terms.items()}
        return r

    def __iadd__(self, op):
        if isinstance(op,BasisOperator):
            op = Operator(op)
        for (o,c) in op.terms.items():
            if o not in self.terms:
                self.terms[o] = 0.
            self.terms[o] += c
        return self

    def __add__(self, op):
        r = self.copy()
        r += op
        return r

    def __sub__(self, op):
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __radd__(self, x):
        assert x == 0
        return self.copy()

    def __mul__(self, op):
        if isinstance(op,numbers.Number):
            return op*self
        if isinstance(op,BasisOperator):
            op = Operator(op)
        r = Operator()
        r.terms = {}
        for (o,c) in self.terms.items():
            for (o_,c_) in op.terms.items():
                r += c*c_*(o*o_)
        return r

    def __rmul__(self, x):
        assert isinstance(x, numbers.Number)
        r = Operator()
        r.terms = {o: c*x for (o,c) in self.terms.items()}
        return r

    def __truediv__(self, x):
        assert isinstance(x, numbers.Number)
        r = Operator()
        r.terms = {o: c/x for (o,c) in self.terms.items()}
        return r

    def __str__(self):
        return ' + '.join(f'{c}*{str(o)}' for (o,c) in self.terms.items())

    def __iter__(self):
        yield from self.terms.items()


