#!/usr/bin/env python

"""
Thirring model (one dimension, one flavor, staggered). In momentum space!
"""

import itertools
import numbers
import picos
import numpy as np
import sys

from algebra import *

VERBOSE = False

############
# Arguments

N = int(sys.argv[1]) # (Number of modes)
m = float(sys.argv[2])
mu = float(sys.argv[3])
g2 = float(sys.argv[4])

level = int(sys.argv[5])

##############
# Hamiltonian

def k(n):
    """ Return the (floating-point) momentum for a numbered mode.
    """
    return 2*n*np.pi/N

if VERBOSE:
    print('Momenta:', ' '.join(f'{momentum(n)}' for n in range(N)))

H = 0*Operator()
# Free Hamiltonian
for n in range(N):
    a = BasisOperator(n)
    h = 0. # TODO
    H += h * a.dag() * a
# Interactions
# TODO

number = 0*Operator()
for n in range(N):
    a = BasisOperator(n)
    number += a.dag()*a

########
# Basis

def make_basis(ops):
    basis = set()

    for op in ops:
        for b, _ in op:
            basis.add(b)

    basis.update({b.dag() for b in basis})
    basis = list(basis)

    if VERBOSE:
        print(f'Operators: {len(basis)}')
        print(basis)

    return basis

ops = set()
ops.update({BasisOperator(n) for n in range(N)})
ops.update({BasisOperator(n)*BasisOperator(n).dag() for n in range(N)})

if level >= 1:
    pass

########
# Bound

def bound(basis):
    evars = {op: picos.ComplexVariable(f'<{op}>') for op in basis}
    evars.update({b: picos.ComplexVariable(f'<{b}>') for b,c in H})

    def expect(op):
        if isinstance(op,BasisOperator):
            return evars[op]
        r = picos.Constant(0)
        for (b,c) in op:
            r += c*evars[b]
        return r

    problem = picos.Problem(verbosity=0)
    problem.set_objective('min', expect(H).real)
    M = picos.HermitianVariable("M", len(basis))
    problem += M >> 0
    for (i1, b1) in enumerate(basis):
        for (i2, b2) in enumerate(basis):
            try:
                problem += M[i1,i2] == expect(b1.dag()*b2)
            except KeyError as missing:
                if False:
                    print('Missing: ', b1.dag()*b2)
    problem += expect(Operator()) == 1.
    problem.options["*_tol"] = None # Use MOSEK defaults.
    if VERBOSE:
        print('Solving...')
    sol = problem.solve(solver='mosek', primals=True, duals=None)
    #print(sol.reported_value, expect(N).real)
    return sol.reported_value, float(expect(number).real)

basis = make_basis(ops)
print(*bound(basis))

