#!/usr/bin/env python3

"""
Extracting the desired bare mass.

Exact solution to Thirring in 1+1 dimensions, with staggered fermions.
"""

import numpy as np
#from numpy import *
import sys
from scipy.sparse.linalg import eigs,eigsh,expm
from scipy.optimize import fsolve

if True:
    from scipy import sparse
    matrix = sparse.dok_matrix
    kron = sparse.kron
    eye = sparse.eye
else:
    from numpy import *


# Number of sites.
N = int(sys.argv[1])

# Fermion mass
m_f = float(sys.argv[2])

# Coupling constant.
g2 = float(sys.argv[3])

def masses(N, m, g2):
    mu = 0
    # We have two families of anticommuting operators, each with one operator at
    # every site. However, these families are pairwise-related, so that once we
    # construct N anticommuting operators, the other N come by taking the
    # transpose.
    P = [matrix(eye(1)*(1.+0j))]*N

    sx = matrix([[0,1],[1,0]])
    sz = matrix([[1,0],[0,-1]])

    for n in range(N):
        for i in range(N):
            if n < i:
                P[i] = kron(P[i], sz, format='dok')
            elif n == i:
                P[i] = kron(P[i], matrix([[0,0],[1,0]]), format='dok')
            else:
                P[i] = kron(P[i], eye(2), format='dok')

    Pd = [matrix(o.H) for o in P]

    num = matrix((2**N,2**N), dtype='complex128')
    #num = matrix(zeros((2**N,2**N)) + 0j)
    for n in range(N):
        num += Pd[n] * P[n]

    #print(len(num.nonzero()[0]))

    H = 0*eye(2**N, dtype='complex128')
    num = 0*eye(2**N, dtype='complex128')
    #H = matrix((2**N,2**N), dtype='complex128')
    #H = matrix(zeros((2**N,2**N)) + 0j)
    for x in range(N):
        y = (x+1)%N
        H += m * (-1)**x * Pd[x] @ P[x]
        H -= mu * Pd[x] @ P[x]
        num += Pd[x] @ P[x]
        #H += 1j * (Pd[x] * P[y] - Pd[y] * P[x])/2
        H += (-1)**x * (Pd[x] @ P[y] + Pd[y] @ P[x])/2
        #H -= g2/2 * (Pd[x] * P[x] * P[y] * Pd[y])
        H -= g2 * (P[x] @ Pd[x] @ Pd[y] @ P[y])
    Hcsc = sparse.csc_matrix(H)
    Hvals, Hdiag = np.linalg.eigh(H.todense())
    #Hvals, Hdiag = eigsh(H,k=6,which='SA')
    E0 = Hvals[0]
    Hvals -= Hvals[0]
    Hvecs = [np.matrix(v).T for v in Hdiag.T]

    gnd = Hvecs[0]

    # Find lowest fermion
    for i in range(1,len(Hvals)):
        vec = Hvecs[i]
        n = (vec.conj().T @ num @ vec)[0,0].real
        if abs(n-N/2+1) < 1e-5:
            f_i = i
            break

    # Find lowest boson
    for i in range(1,len(Hvals)):
        vec = Hvecs[i]
        n = (vec.conj().T @ num @ vec)[0,0].real
        if abs(n-N/2) < 1e-5:
            b_i = i
            break

    return Hvals[f_i], Hvals[b_i]

def mass_diff(m):
    m = m[0]
    return m_f - masses(N, m, g2)[0]

m = fsolve(mass_diff, m_f)[0]
print(m)
