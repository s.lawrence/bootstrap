#!/usr/bin/env python3

"""
Exact solution to Thirring in 1+1 dimensions, with staggered fermions.
"""

import numpy as np
#from numpy import *
import sys
from scipy.sparse.linalg import eigs,eigsh,expm

if True:
    from scipy import sparse
    matrix = sparse.dok_matrix
    kron = sparse.kron
    eye = sparse.eye
else:
    from numpy import *


# Number of sites.
N = int(sys.argv[1])

# Mass.
m = float(sys.argv[2])

# Chemical potential.
mu = float(sys.argv[3])

# Coupling constant.
g2 = float(sys.argv[4])


# We have two families of anticommuting operators, each with one operator at
# every site. However, these families are pairwise-related, so that once we
# construct N anticommuting operators, the other N come by taking the
# transpose.
P = [matrix(eye(1)*(1.+0j))]*N

sx = matrix([[0,1],[1,0]])
sz = matrix([[1,0],[0,-1]])

for n in range(N):
    for i in range(N):
        if n < i:
            P[i] = kron(P[i], sz, format='dok')
        elif n == i:
            P[i] = kron(P[i], matrix([[0,0],[1,0]]), format='dok')
        else:
            P[i] = kron(P[i], eye(2), format='dok')

Pd = [matrix(o.H) for o in P]

num = matrix((2**N,2**N), dtype='complex128')
#num = matrix(zeros((2**N,2**N)) + 0j)
for n in range(N):
    num += Pd[n] * P[n]

#print(len(num.nonzero()[0]))

H = 0*eye(2**N, dtype='complex128')
num = 0*eye(2**N, dtype='complex128')
#H = matrix((2**N,2**N), dtype='complex128')
#H = matrix(zeros((2**N,2**N)) + 0j)
for x in range(N):
    y = (x+1)%N
    H += m * (-1)**x * Pd[x] @ P[x]
    H -= mu * Pd[x] @ P[x]
    num += Pd[x] @ P[x]
    #H += 1j * (Pd[x] * P[y] - Pd[y] * P[x])/2
    H += (-1)**x * (Pd[x] @ P[y] + Pd[y] @ P[x])/2
    #H -= g2/2 * (Pd[x] * P[x] * P[y] * Pd[y])
    H -= g2 * (P[x] @ Pd[x] @ Pd[y] @ P[y])
Hcsc = sparse.csc_matrix(H)
Hvals, Hdiag = np.linalg.eigh(H.todense())
#Hvals, Hdiag = eigsh(H,k=6,which='SA')
E0 = Hvals[0]
Hvals -= Hvals[0]
Hvecs = [np.matrix(v).T for v in Hdiag.T]

gnd = Hvecs[0]
print(E0,(gnd.conj().T @ num @ gnd)[0,0].real-N//2)

if False:
    for i in range(len(Hvals)):
        vec = Hvecs[i]
        print(Hvals[i], (vec.conj().T @ num @ vec)[0,0].real)

#E, v = zip(*list(sorted(zip(Hvals.real, Hvecs), key=lambda x: x[0])))

