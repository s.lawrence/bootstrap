#!/usr/bin/env python

# Computations for the free theory, at infinite volume.

import sys
import numpy as np

m = float(sys.argv[1])
mu = float(sys.argv[2])

a = mu**2 - m**2
if a < 1. and a > 0.:
    kmax = np.arcsin(np.sqrt(mu**2 - m**2))
elif a <= 0.:
    kmax = 0
else:
    kmax = np.arcsin(1.)

print(2*kmax/np.pi)

