#!/usr/bin/env python

import numpy as np
import picos
import sys

#########################
# Manipulating operators

class Operator:
    def __init__(self):
        pass # TODO

    def __iadd__(self, op):
        pass # TODO

    def __sub__(self, op):
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __add__(self, op):
        pass # TODO

    def __mul__(self, op):
        pass # TODO

    def __rmul__(self, x):
        pass # TODO

    def dag(self):
        pass # TODO

    def __str__(self):
        pass # TODO


##############
# Hamiltonian

Lx = int(sys.argv[1])
Ly = int(sys.argv[2])
t = 1.
U = float(sys.argv[3])
mu = float(sys.argv[4])


############################
# Semi-definite programming


problem = picos.Problem(verbosity=1)

sol = problem.solve(solver = 'mosek', primals=None, duals=None)


