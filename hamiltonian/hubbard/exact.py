#!/usr/bin/env python

import numpy as np
import sys

Lx = int(sys.argv[1])
Ly = int(sys.argv[2])
t = 1.
U = float(sys.argv[3])
mu = float(sys.argv[4])

V = Lx*Ly
D = 2**(2*V)

pauli_1 = np.array([[1,0],[0,1]],dtype=np.complex128)
pauli_x = np.array([[0,1],[1,0]],dtype=np.complex128)
pauli_y = np.array([[0,-1j],[1j,0]],dtype=np.complex128)
pauli_z = np.array([[1,0],[0,-1]],dtype=np.complex128)
pauli_p = (pauli_x + 1j * pauli_y)/2
pauli_m = (pauli_x - 1j * pauli_y)/2

c_p = [[np.eye(1,dtype=np.complex128) for y in range(Ly)] for x in range(Lx)]
c_m = [[np.eye(1,dtype=np.complex128) for y in range(Ly)] for x in range(Lx)]
for x in range(Lx):
    for y in range(Ly):
        for xp in range(Lx):
            for yp in range(Ly):
                if x < xp or (x == xp and y < yp):
                    c_p[x][y] = np.kron(pauli_z, c_p[x][y])
                    c_p[x][y] = np.kron(pauli_z, c_p[x][y])
                    c_m[x][y] = np.kron(pauli_z, c_m[x][y])
                    c_m[x][y] = np.kron(pauli_z, c_m[x][y])
                elif x == xp and y == yp:
                    c_p[x][y] = np.kron(pauli_z, c_p[x][y])
                    c_p[x][y] = np.kron(pauli_p, c_p[x][y])
                    c_m[x][y] = np.kron(pauli_p, c_m[x][y])
                    c_m[x][y] = np.kron(pauli_1, c_m[x][y])
                else:
                    c_p[x][y] = np.kron(pauli_1, c_p[x][y])
                    c_p[x][y] = np.kron(pauli_1, c_p[x][y])
                    c_m[x][y] = np.kron(pauli_1, c_m[x][y])
                    c_m[x][y] = np.kron(pauli_1, c_m[x][y])

c_p = np.array(c_p)
c_m = np.array(c_m)

cdag_p = np.einsum('abcd->abdc',c_p.conj())
cdag_m = np.einsum('abcd->abdc',c_m.conj())
n_p = np.einsum('abcd,abde->abce', cdag_p, c_p)
n_m = np.einsum('abcd,abde->abce', cdag_m, c_m)
n = n_p + n_m



