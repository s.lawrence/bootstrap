#!/usr/bin/env python

import numpy as np
import picos
import sys


#########################
# Manipulating operators

def product_terms(x1,p1,x2,p2):
    if p1 == 0 or x2 == 0:
        yield ((x1+x2,p1+p2), 1.)
        return
    yield from product_terms(x1+1,p1,x2-1,p2)
    for ((x,p),c) in product_terms(x1,p1-1,x2-1,p2):
        yield ((x,p),-1j*p1*c)

class Operator:
    def __init__(self, x=None):
        if isinstance(x, str):
            if x == 'x':
                self.c = {'x': complex(1)}
            elif x == 'p':
                self.c = {'p': complex(1)}
            else:
                raise "must be x or p"
        elif x is not None:
            self.c = {'': complex(x)}
        else:
            self.c = dict()

    def __add__(self, op):
        r = Operator()
        for term in set.union(set(self.c), set(op.c)):
            r.c[term] = 0.
            if term in self.c:
                r.c[term] += self.c[term]
            if term in op.c:
                r.c[term] += op.c[term]
        return r

    def __sub__(self, op):
        return self+(-op)

    def __neg__(self):
        r = Operator()
        for term in self.c:
            r.c[term] = -self.c[term]
        return r

    def __mul__(self, op):
        r = Operator()
        for t1 in self.c:
            for t2 in op.c:
                x1 = t1.count('x')
                p1 = t1.count('p')
                x2 = t2.count('x')
                p2 = t2.count('p')
                for ((x,p),c) in product_terms(x1,p1,x2,p2):
                    if 'x'*x+'p'*p not in r.c:
                        r.c['x'*x+'p'*p] = 0.
                    r.c['x'*x+'p'*p] += c*self.c[t1]*op.c[t2]
        return r

    def __rmul__(self, x):
        r = Operator()
        for term in self.c:
            r.c[term] = x * self.c[term]
        return r

    def __str__(self):
        return str(self.c)

def operator(desc):
    op = Operator(1.)
    for c in desc:
        op = op*Operator(c)
    return op

def commutator(a, b):
    return a*b - b*a


##############
# Hamiltonian

H_sho = 0.5*(operator('xx') + operator('pp'))
H = H_sho + 0.25*operator('xxxx')

##############
# Constraints

# PARAMETERS TO MODIFY:
Nx = int(sys.argv[1])   +1
Np = 1   +1


Tx = 2*Nx+1
Tp = 2*Np+1

evar_names = ['x'*x+'p'*p for x in range(Tx) for p in range(Tp)]
evars = {n: picos.ComplexVariable(f'<{n}>') for n in evar_names}
idx = {evar_names[i]: i for i in range(len(evar_names))}

problem = picos.Problem(verbosity=0)

# We target the energy
eH = 0
for (t,coef) in H.c.items():
    eH += coef*evars[t]
problem.set_objective('min', eH.real)

if True:
    # Positivity bounds
    ops = ['x'*x for x in range(Nx)] + ['p'*p for p in range(Np)]
    M = picos.HermitianVariable("M", len(ops))
    for t1 in ops:
        for t2 in ops:
            i1 = ops.index(t1)
            i2 = ops.index(t2)
            op = operator(t1+t2[::-1])
            e = picos.Constant(0)
            for (t,c) in op.c.items():
                e += c*evars[t]
            problem.add_constraint(M[i1,i2] == e)
    problem.add_constraint(M >> 0)
else:
    # Positivity bounds
    ops = ['x'*x for x in range(Nx)] + ['p'*p for p in range(Np)]
    #ops = ['x'*x for x in range(15)] + ['p'*p for p in range(5)]
    M = [[picos.Constant(0) for _ in range(len(ops))] for _ in range(len(ops))]
    for t1 in ops:
        for t2 in ops:
            i1 = ops.index(t1)
            i2 = ops.index(t2)
            op = operator(t1+t2[::-1])
            e = picos.Constant(0)
            for (t,c) in op.c.items():
                e += c*evars[t]
            M[i1][i2] = e

    # Convert to picos matrix, and add PSD constraint.
    rows = []
    for m in M:
        row = m[0]
        for e in m[1:]:
            row = row & e
        rows.append(row)
    M_ = rows[0]
    for row in rows[1:]:
        M_ = M_ // row
    problem.add_constraint(M_.hermitianized >> 0)
    problem.add_constraint(M_ - M_.conj.T == 0)

# Fix normalization
problem.add_constraint(evars[''] == 1.)

if False:
    # Commutators must vanish
    for x in range(Tx):
        for p in range(Tp):
            try:
                op = operator('x'*x+'p'*p)
                com = commutator(H,op)
                ecom = 0
                for (t,c) in com.c.items():
                    ecom += c*evars[t]
            except KeyError:
                continue
            problem.add_constraint(ecom == 0)

picos.modeling.file_out.write(problem, 'problem.mosek', 'mosek')
sol = problem.solve(solver = 'mosek', primals=None, duals=None)
print(sol.reported_value)
print(evars['xx'], evars['xxxx'])
