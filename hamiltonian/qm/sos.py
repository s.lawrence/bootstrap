#!/usr/bin/env python

import picos

problem = picos.Problem(verbosity=1)

Hxx = 0.5
Hpp = 0.5
Hxxxx = 0

ar = picos.RealVariable('cr')
br = picos.RealVariable('xr')
cr = picos.RealVariable('xxr')
dr = picos.RealVariable('pr')

ai = picos.RealVariable('ci')
bi = picos.RealVariable('xi')
ci = picos.RealVariable('xxi')
di = picos.RealVariable('pi')

a = ar+1j*ai
b = br+1j*bi

#print(ar*br - ai*bi + (1j*ar*bi+1j*ai*br))

problem.set_objective('min', ar)
problem.add_constraint(ar*ar+ai*ai <= 0.5)
problem.add_constraint(ci*br+bi*cr == 0)

if False:
    a = picos.ComplexVariable('c')
    b = picos.ComplexVariable('x')
    c = picos.ComplexVariable('xx')
    d = picos.ComplexVariable('p')

    problem.set_objective('min', a.real)
    problem.add_constraint((c.conj*b).imag == 0)
    #problem.add_constraint(a.real*a.real +a.imag*a.imag <= 0.5)
    #problem.add_constraint(a.conj == a)

    #problem.add_constraint(a.real**2 >= 1)
    #print(a.conj*b)
    #print(a.conj, a, a*a)
    #problem.add_constraint((a.conj*a).real >= 0.5)

sol = problem.solve(solver = 'mosek', primals=None, duals=None)
#sol = problem.solve(solver = 'cvxopt', primals=None, duals=None)

#print(ar, ai)

