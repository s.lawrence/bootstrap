#!/usr/bin/env python

# Based on arXiv:2108.08757

import numpy as np

# Hamiltonian parameters
g = 0.5
h = 0.5

N = 5

H = 0.559
x2 = 0.41

def ok(H,x2):
    # Generate moments
    x4 = (H - 2 * g * x2) / (3*h)

    e = np.zeros(2*N)
    e[0] = 1
    e[2] = x2
    e[4] = x4
    for m in range(3,2*N-3):
        e[m+3] = (1/4*m*(m-1)*(m-2)*e[m-3] + 2*m*H*e[m-1] - (2*g+2*m*g)*e[m+1]) / (4*h + 2*m*h)

    M = np.zeros((N,N))
    for i in range(N):
        for j in range(N):
            M[i,j] = e[i+j]

    vals, _ = np.linalg.eig(M)
    if np.all(vals >= 0):
        return 1
    else:
        return 0

H = np.linspace(0,12,300)
X2 = np.linspace(0,3,300)
#H = np.linspace(0.,1.,300)
#X2 = np.linspace(0.,1.,300)
H,X2 = np.meshgrid(H,X2)

import matplotlib.pyplot as plt


plt.figure()
plt.contour(H,X2,np.vectorize(ok)(H,X2),levels=[0.5])
plt.xlabel('E')
plt.ylabel('x2')
plt.show()

#print(ok(H,x2))

