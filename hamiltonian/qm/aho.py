#!/usr/bin/env python

import numpy as np
import sys

m = 1
g = 0.25
#m = float(sys.argv[1])
#g = float(sys.argv[2])
N = 500

a = np.zeros((N,N))
for i in range(N-1):
    a[i,i+1] = np.sqrt(i+1)*np.sqrt(m)
adag = a.transpose().conjugate()

x = 1/(m*np.sqrt(2)) * (a + adag)
p = 1j*np.sqrt(m/2) * (adag - a)
H = adag@a + 1/2 * np.eye(N) + g*x@x@x@x

E, U = np.linalg.eigh(H)
print(E[0])

gnd = U[:,0]
#print('norm: ', gnd.conj().T @ gnd)
print('xx: ', gnd.conj().T @ x @ x @ gnd)
print('xxxx: ', gnd.conj().T @ x @ x @ x @ x @ gnd)

