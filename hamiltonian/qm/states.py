#!/usr/bin/env python

# Based on arXiv:2108.08757

import numpy as np
import picos

#########################
# Manipulating operators

def product_terms(x1,p1,x2,p2):
    if p1 == 0 or x2 == 0:
        yield ((x1+x2,p1+p2), 1.)
        return
    p1_ = p1-1
    x2_ = x2-1
    yield from (product_terms(x1+1,p1_,x2_,p2+1))
    for ((x,p),c) in product_terms(x1,p1_,x2_,p2):
        yield ((x,p),c*-1j)

class Operator:
    def __init__(self, x=None):
        if isinstance(x, str):
            if x == 'x':
                self.c = {'x': complex(1)}
            elif x == 'p':
                self.c = {'p': complex(1)}
            else:
                raise "must be x or p"
        elif x is not None:
            self.c = {'': complex(x)}
        else:
            self.c = dict()

    def __add__(self, op):
        r = Operator()
        for term in set.union(set(self.c), set(op.c)):
            r.c[term] = 0.
            if term in self.c:
                r.c[term] += self.c[term]
            if term in op.c:
                r.c[term] += op.c[term]
        return r

    def __sub__(self, op):
        return self+(-op)

    def __neg__(self):
        r = Operator()
        for term in self.c:
            r.c[term] = -self.c[term]
        return r

    def __mul__(self, op):
        r = Operator()
        for t1 in self.c:
            for t2 in op.c:
                x1 = t1.count('x')
                p1 = t1.count('p')
                x2 = t2.count('x')
                p2 = t2.count('p')
                for ((x,p),c) in product_terms(x1,p1,x2,p2):
                    if 'x'*x+'p'*p not in r.c:
                        r.c['x'*x+'p'*p] = 0.
                    r.c['x'*x+'p'*p] += c*self.c[t1]*op.c[t2]
        return r

    def __rmul__(self, x):
        r = Operator()
        for term in self.c:
            r.c[term] = x * self.c[term]
        return r

    def __str__(self):
        return str(self.c)

def operator(desc):
    op = Operator(1.)
    for c in desc:
        op = op*Operator(c)
    return op

def commutator(a, b):
    return a*b - b*a


##############
# Hamiltonian

H_sho = 0.5*(operator('xx') + operator('pp'))
H = H_sho + 0.25*operator('xxxx')

for E in np.linspace(0,4,10000):
    print(E)

problem = picos.Problem()
x = picos.RealVariable('x')
problem.set_objective('min', x)
problem.add_constraint(x*x >= 2)
sol = problem.solve(solver = 'mosek')
print(sol)
print(sol.reported_value)
