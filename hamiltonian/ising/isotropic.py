#!/usr/bin/env python

# Semidefinite programs for the isotropic transverse Ising model, in 2
# (spatial) dimensions.

import itertools
import numpy as np
import pickle
import picos
import progress.bar
import random
import sys


#########################
# Manipulating operators

def pauli_mul_1(a, b):
    if a == 'I':
        return b, 1.+0j
    if b == 'I':
        return a, 1.+0j
    if a == b:
        return 'I', 1.+0j
    if a == 'X' and b == 'Y':
        return 'Z', 1j
    if a == 'Y' and b == 'Z':
        return 'X', 1j
    if a == 'Z' and b == 'X':
        return 'Y', 1j
    r, c = pauli_mul(b, a)
    return r, -c

def pauli_mul(a, b):
    assert len(a) == len(b)
    c = 1.
    r = []
    for i in range(len(a)):
        s, d = pauli_mul_1(a[i], b[i])
        r.append(s)
        c *= d
    return ''.join(r), c

class Operator:
    def __init__(self, pauli=None, N=None):
        assert pauli is None or N is None
        assert pauli is not None or N is not None
        if N is not None:
            self.N = N
            self.terms = dict()
        if pauli is not None:
            self.N = len(pauli)
            self.terms = {pauli: 1.+0j}

    def __iadd__(self, op):
        assert self.N == op.N
        for s, c in op.terms.items():
            if s not in self.terms:
                self.terms[s] = 0.j
            self.terms[s] += c
        return self

    def __sub__(self, op):
        assert self.N == op.N
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __add__(self, op):
        assert self.N == op.N
        r = Operator(N=self.N)
        r += self
        r += op
        return r

    def __mul__(self, op):
        assert self.N == op.N
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            for sp, cp in op.terms.items():
                ns, nc = pauli_mul(s,sp)
                nc *= c*cp
                r.terms[ns] = nc
        return r

    def __rmul__(self, x):
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            r.terms[s] = x*c
        return r

    def dag(self):
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            r.terms[s] = c.conjugate()
        return r

    def __str__(self):
        return ' + '.join([f'{self.terms[t]}*{str(t)}' for t in self.terms])


############
# Utilities

# Translations
def shift(Lx, Ly, l, x, y):
    la = np.array(list(l)).reshape((Lx,Ly))
    if x >= 0:
        lax = la[Lx-x:,:]
    else:
        lax = la[:-x,:]
    if y >= 0:
        lay = la[:,Ly-y:]
    else:
        lay = la[:,:-y]
    if np.all(lax == 'I') and np.all(lay == 'I'):
        return ''.join(np.roll(la, (x,y), axis=(0,1)).reshape(Lx*Ly))
    else:
        return None


###########################
# Constructing and solving

def neighbors(op1, op2):
    for (s,c) in (Operator(op1).dag()*Operator(op2)).terms.items():
        yield s
    for (s,c) in (Operator(op1)*Operator(op2)).terms.items():
        yield s

def candidates(Lx, Ly, ops):
    for x in range(-Lx+1,Lx):
        for y in range(-Ly+1,Ly):
            for op1 in ops:
                for op2 in ops:
                    op1_ = shift(Lx,Ly,op1,x,y)
                    if op1_ is not None:
                        yield from neighbors(op1_,op2)

def bound(H, Lx, Ly, ops):
    # Now we can list all the operators we need.
    V = Lx*Ly
    evar_names = ['I'*V]
    for t1 in ops:
        for t2 in ops:
            i1 = ops.index(t1)
            i2 = ops.index(t2)
            op = Operator(t1).dag() * Operator(t2)
            for (s,c) in op.terms.items():
                if s not in evar_names:
                    evar_names.append(s)

    evars = {n: picos.ComplexVariable(f'<{n}>') for n in evar_names}
    idx = {evar_names[i]: i for i in range(len(evar_names))}

    problem = picos.Problem(verbosity=0)

    # We target the energy
    eH = 0
    for (s,c) in H.terms.items():
        eH += c*evars[s]
    problem.set_objective('min', eH.real)

    # Positivity bounds
    M = picos.HermitianVariable("M", len(ops))
    for t1 in ops:
        for t2 in ops:
            i1 = ops.index(t1)
            i2 = ops.index(t2)
            op = Operator(t1).dag() * Operator(t2)
            e = picos.Constant(0)
            for (s,c) in op.terms.items():
                e += c*evars[s]
            problem += M[i1,i2] == e
    problem += M >> 0

    # Translational invariance
    for op in ops:
        for x in range(-Lx+1,Lx):
            for y in range(-Ly+1,Ly):
                opp = shift(Lx, Ly, op, x, y)
                if op in evars and opp in evars:
                    problem.add_constraint(evars[op] == evars[opp])

    # Fix normalization
    problem.add_constraint(evars['I'*V] == 1.)

    sol = problem.solve(solver = 'mosek', primals=True, duals=None)
    return sol.reported_value, evars['X' + 'I'*(Lx*Ly-1)].real

def hamiltonian(Lx, Ly, mu):
    H = 0.*Operator('I'*Lx*Ly)
    H -= mu*Operator('X' + 'I'*(Lx*Ly-1))
    H -= Operator('ZZ' + 'I'*(Lx*Ly-2))
    H -= Operator('Z' + 'I'*(Lx-1) + 'Z' + 'I'*(Lx*(Ly-1)-1))
    return H

def initial_ops(Lx, Ly):
    V = Lx*Ly
    ops = ['I'*V, 'X'+'I'*(V-1)]
    if True:
        if False:
            for i in range(V):
                for j in range(V):
                    if i == j:
                        continue
                    op = ['I']*V
                    op[i] = 'Z'
                    op[j] = 'Z'
                    ops.append(''.join(op))
                    op[i] = 'Y'
                    op[j] = 'Z'
                    ops.append(''.join(op))
        elif False:
            for i in range(V-1):
                if False:
                    ops.append('X' + 'I'*i + 'X' + 'I'*(V-2-i))
                    ops.append('Y' + 'I'*i + 'Y' + 'I'*(V-2-i))
                if False:
                    ops.append('Z' + 'I'*i + 'Y' + 'I'*(V-2-i))
                    ops.append('Y' + 'I'*i + 'Z' + 'I'*(V-2-i))
                ops.append('Z' + 'I'*i + 'Z' + 'I'*(V-2-i))
        else:
            for i in range(Lx-1):
                ops.append('Z' + 'I'*i + 'Z' + 'I'*(V-2-i))
            ops.append('Z' + 'I'*(Lx-1) + 'Z' + 'I'*(Lx*(Ly-1)-1))
    else:
        ops.append('ZZ' + 'I'*(Lx*Ly-2))
        ops.append('Z' + 'I'*(Lx-1) + 'Z' + 'I'*(Lx*(Ly-1)-1))
    return ops


if __name__ == '__main__':
    ######################
    # Hamiltonian density

    Lx = int(sys.argv[1])
    Ly = int(sys.argv[2])
    mu = float(sys.argv[3])
    H = hamiltonian(Lx, Ly, mu)

    ############################
    # Semi-definite programming

    # The set of operators that generate the positivity bounds.
    ops = initial_ops(Lx, Ly)

    base, _ = bound(H, Lx, Ly, ops)
    ops_history = [ops.copy()]
    while True:
        print(len(ops), base)
        best = base-1
        best_op = None
        for op in progress.bar.Bar().iter(set(candidates(Lx, Ly, ops))):
            if op in ops:
                continue
            ops_new = ops+[op]
            new, _ = bound(H, Lx, Ly, ops_new)
            if False:
                print(f'                              tried {op}   :   {new}')
            if new > best:
                best = new
                best_op = op
        ops.append(best_op)
        print(f'             added {best_op}')
        base = best
        # Write operators to file
        ops_history.append(ops.copy())
        with open('operators.pickle', 'wb') as f:
            pickle.dump((Lx, Ly, ops_history),f)

