#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import sys
import progress.bar

dat = np.array([[float(x) for x in l.split()] for l in sys.stdin.readlines()])
x, y = dat[:,0], dat[:,1]

plt.figure()
plt.plot(x,np.gradient(y,x))
plt.tight_layout()
plt.show()

