#!/usr/bin/env python

import scipy.sparse as sp
import scipy.sparse.linalg as splinalg
import numpy as np
import sys

mat = sp.lil_matrix

def measure(Lx,Ly,mu):
    V = Lx*Ly
    D = 2**V

    pauli_i = mat([[1,0],[0,1]])
    pauli_x = mat([[0,1],[1,0]])
    pauli_z = mat([[1,0],[0,-1]])

    def idx(x,y):
        return x*Ly+y

    sigma_x = [mat([[1.]]) for _ in range(V)]
    sigma_z = [mat([[1.]]) for _ in range(V)]

    for i in range(V):
        for j in range(V):
            if i == j:
                sigma_x[i] = sp.kron(sigma_x[i], pauli_x, format='lil')
                sigma_z[i] = sp.kron(sigma_z[i], pauli_z, format='lil')
            else:
                sigma_x[i] = sp.kron(sigma_x[i], pauli_i, format='lil')
                sigma_z[i] = sp.kron(sigma_z[i], pauli_i, format='lil')

    H = mat((D,D))
    for x in range(Lx):
        for y in range(Ly):
            xp = (x+1)%Lx
            yp = (y+1)%Ly
            i = idx(x,y)
            ixp = idx(xp,y)
            iyp = idx(x,yp)
            H -= mu*sigma_x[i]
            H -= sigma_z[i]@sigma_z[ixp]
            H -= sigma_z[i]@sigma_z[iyp]

    H = sp.csr_matrix(H)
    vals, vecs = splinalg.eigsh(H, k=1, which='SA')

    return vals[0]/V, (vecs[:,0] @ sigma_x[0] @ vecs[:,0].T.conj()).real

if __name__ == '__main__':
    Lx = int(sys.argv[1])
    Ly = int(sys.argv[2])
    mu = float(sys.argv[3])
    print(*measure(Lx,Ly,mu))

