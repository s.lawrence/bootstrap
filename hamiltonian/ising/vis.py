#!/usr/bin/env python

"""
Visualizing the selected operators.
"""

import numpy as np
import pickle

with open('operators.pickle', 'rb') as f:
    Lx, Ly, ops_history = pickle.load(f)

ops = ops_history[-1]
for n, op in enumerate(ops):
    opa = np.reshape(list(op), (Lx,Ly))
    print('\n'.join(''.join(l) for l in opa))
    print()
