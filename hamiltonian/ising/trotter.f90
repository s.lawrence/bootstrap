program trotter

    real, parameter :: pi = 3.141592653589793238462643383279

    character(len=50) :: Lxstr, Lystr, mustr, betastr, dtstr
    integer :: Lx, Ly, V
    real :: mu, beta, dt

    if (command_argument_count() /= 5) then
        stop "Lx Ly mu beta dt"
    end if

    call get_command_argument(1, Lxstr)
    call get_command_argument(2, Lystr)
    call get_command_argument(3, mustr)
    call get_command_argument(4, betastr)
    call get_command_argument(5, dtstr)

    read (Lxstr,*) Lx
    read (Lystr,*) Ly
    read (mustr,*) mu
    read (betastr,*) beta
    read (dtstr,*) dt

    V = Lx*Ly

    if (.false.) then
        block
            integer, allocatable :: seed(:)
            integer :: sz
            call random_seed(size=sz)
            allocate(seed(sz))
            seed = 0
            call random_seed(put=seed)
            deallocate(seed)
        end block
    end if

    call main

contains

subroutine main
    complex, allocatable :: ket(:), ketp(:), Hket(:)
    integer :: n
    real :: t, tp

    allocate(ket(2**(Lx*Ly)))
    allocate(ketp(2**(Lx*Ly)))
    allocate(Hket(2**(Lx*Ly)))

    ! Initialize a random wavefunction.
    do n = 1,size(ket)
        call random_complex(ket(n))
    end do
    ! Normalize
    ket = ket / sqrt(sum(abs(ket)**2))

    ! Thermal evolution.
    t = 0
    tp = 0
    do while (t < beta)
        t = t + dt

        ketp = ket
        call hamiltonian(ketp, Hket)
        ket = ket - dt * Hket
        ketp = Hket
        call hamiltonian(ketp, Hket)
        ket = ket + dt*dt/2. * Hket

        ! Keep the state normalized.
        ket = ket / sqrt(sum(abs(ket)**2))
    end do

    call hamiltonian(ket, Hket)
    call mag(ket, ketp)
    print *, real(dot_product(ket, ketp))/V

    deallocate(ket)
    deallocate(ketp)
    deallocate(Hket)
end subroutine

subroutine hamiltonian(psi, Hpsi)
    complex, allocatable, intent(in) :: psi(:)
    complex, allocatable, intent(inout) :: Hpsi(:)
    integer :: x,y,xp,yp
    integer :: n,np,diff,up

    ! The Hamiltonian has one magnetic field term and two hopping terms.
    Hpsi = 0

    do x = 1,Lx
        do y = 1,Ly
            do n = 1,2**(Lx*Ly)
                ! Magnetic field
                np = ibflip(n-1,site(x,y)) + 1
                Hpsi(np) = Hpsi(np) - mu * psi(n)

                ! Adjacent sites.
                xp = mod(x,Lx)+1
                yp = mod(y,Ly)+1

                ! X-coupling
                diff = ieor(ibits(n-1,site(x,y),1), ibits(n-1,site(xp,y),1))
                Hpsi(n) = Hpsi(n) + 2.*diff * psi(n)

                ! Y-coupling
                diff = ieor(ibits(n-1,site(x,y),1), ibits(n-1,site(x,yp),1))
                Hpsi(n) = Hpsi(n) + 2.*diff * psi(n)
            end do
        end do
    end do
end subroutine

subroutine mag(psi, Mpsi)
    complex, allocatable, intent(in) :: psi(:)
    complex, allocatable, intent(inout) :: Mpsi(:)
    integer :: x,y
    integer :: n,np,diff

    Mpsi = 0

    do x = 1,Lx
        do y = 1,Ly
            do n = 1,2**(Lx*Ly)
                ! Magnetic field
                np = ibflip(n-1,site(x,y)) + 1
                Mpsi(np) = Mpsi(np) + psi(n)
            end do
        end do
    end do
end subroutine

function ibflip(i, pos) result(j)
    integer, intent(in) :: i, pos
    integer :: j
    j = ieor(i, ibset(0,pos))
end function

function site(x,y) result(n)
    integer, intent(in) :: x,y
    integer n
    n = (y-1)*Lx+(x-1)
end function

! Sample two variables from the normal distribution with given mean and standard
! deviation. This function exists because, for the Box-Muller transform, it's more natural
! and efficient to sample two variables at once.
subroutine random_normals(x, y, dev, mean)
	real, intent(in), optional :: dev, mean
	real, intent(out) :: x, y
	real :: a, b, d, m
	d = 1
	m = 0
	if (present(dev)) d = dev
	if (present(mean)) m = mean
	call random_number(a)
	call random_number(b)
	a = a * 2 * pi
	b = - log(b)
	x = b*cos(a)*d + m
	y = b*sin(a)*d + m
end subroutine

subroutine random_complex(z)
    complex, intent(out) :: z
    real :: x,y
    call random_normals(x, y, 1., 0.)
    z = complex(x,y)
end subroutine

end program
