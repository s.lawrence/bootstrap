#!/bin/sh

Lx=$1
Ly=$2

for mu in `seq 1.5 0.1 3.5`; do
	echo -n "$mu "
	./trotter $Lx $Ly $mu 40. 0.002
done
