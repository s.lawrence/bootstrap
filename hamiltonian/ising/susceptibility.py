#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import sys
import progress.bar
from exact import measure

Lx = int(sys.argv[1])
Ly = int(sys.argv[2])

#TARGET='E'
#TARGET='DE'
#TARGET='DDE'
#TARGET='M'
TARGET='DM'
#TARGET='DDM'

plt.figure()
mus = np.linspace(0,5,51)
energies = []
mags = []
for mu in progress.bar.Bar('').iter(mus):
    energy, mag = measure(Lx, Ly, mu)
    energies.append(energy)
    mags.append(mag)
if TARGET == 'E':
    plt.plot(mus,energies)
    plt.ylabel('$\\epsilon$')
if TARGET == 'DE':
    plt.plot(mus,np.gradient(energies,mus))
    plt.ylabel('$\\frac{d\\epsilon}{d\\mu}$')
if TARGET == 'DDE':
    plt.plot(mus,np.gradient(np.gradient(energies,mus),mus))
    plt.ylabel('$\\frac{d^2\\epsilon}{d\\mu^2}$')
if TARGET == 'M':
    plt.plot(mus,mags)
    plt.ylabel('$M$')
if TARGET == 'DM':
    plt.plot(mus,np.gradient(mags,mus))
    plt.ylabel('$\\frac{dM}{d\\mu}$')
if TARGET == 'DDM':
    plt.plot(mus,np.gradient(np.gradient(mags,mus),mus))
    plt.ylabel('$\\frac{d^2M}{d\\mu^2}$')
plt.xlabel('$\\mu$')
plt.tight_layout()
plt.show()

