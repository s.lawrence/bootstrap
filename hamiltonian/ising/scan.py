#!/usr/bin/env python

import isotropic
import matplotlib.pyplot as plt
import numpy as np
import pickle
import progress.bar

#TARGET='E'
#TARGET='DE'
#TARGET='M'
TARGET='DM'

with open('operators.pickle', 'rb') as f:
    Lx, Ly, ops_history = pickle.load(f)

plt.figure()
mus = np.linspace(0,5,51)
for ops in ops_history:
    energies = []
    mags = []
    for mu in progress.bar.Bar('').iter(mus):
        H = isotropic.hamiltonian(Lx, Ly, mu)
        energy, mag = isotropic.bound(H, Lx, Ly, ops)
        energies.append(energy)
        mags.append(mag)
    if TARGET == 'E':
        plt.plot(mus,energies,label=f'{len(ops)}')
    if TARGET == 'DE':
        plt.plot(mus,np.gradient(energies,mus),label=f'{len(ops)}')
    if TARGET == 'M':
        plt.plot(mus,mags,label=f'{len(ops)}')
    if TARGET == 'DM':
        plt.plot(mus,np.gradient(mags,mus),label=f'{len(ops)}')
if TARGET == 'E':
    plt.ylabel('$\\epsilon$')
if TARGET == 'DE':
    plt.ylabel('$\\frac{d\\epsilon}{d\\mu}$')
if TARGET == 'M':
    plt.ylabel('$M$')
if TARGET == 'DM':
    plt.ylabel('$\\frac{dM}{d\\mu}$')
plt.xlabel('$\\mu$')
plt.legend(loc='best')
plt.tight_layout()
plt.show()

