#!/usr/bin/env python

# Kitaev chain

import numpy as np
import picos
import sys


#########################
# Manipulating operators

majorana_mul_1 = {
        ('I','I'): 'I',
        ('I','G'): 'G',
        ('G','I'): 'G',
        ('G','G'): 'I',
}


def majorana_mul(a,b):
    assert len(a) == len(b)
    # Construct the string.
    r = []
    for i in range(len(a)):
        r.append(majorana_mul_1[a[i],b[i]])
    # Calculate the sign
    s = 1.
    for i in range(len(a)):
        for j in range(len(b)):
            if a[i] == 'G' and b[j] == 'G':
                if i > j:
                    s *= -1
    return ''.join(r), s

def majorana_dag(a):
    n = sum(1 for c in a if c == 'G')
    return (-1)**((n*(n-1))/2)

class Operator:
    def __init__(self, maj=None, N=None):
        assert maj is None or N is None
        assert maj is not None or N is not None
        if N is not None:
            self.N = N
            self.terms = dict()
        if maj is not None:
            self.N = len(maj)
            self.terms = {maj: 1.+0j}

    def __iadd__(self, op):
        assert self.N == op.N
        for s, c in op.terms.items():
            if s not in self.terms:
                self.terms[s] = 0.j
            self.terms[s] += c
        return self

    def __sub__(self, op):
        assert self.N == op.N
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __add__(self, op):
        assert self.N == op.N
        r = Operator(N=self.N)
        r += self
        r += op
        return r

    def __mul__(self, op):
        assert self.N == op.N
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            for sp, cp in op.terms.items():
                ns, nc = majorana_mul(s,sp)
                nc *= c*cp
                r.terms[ns] = nc
        return r

    def __rmul__(self, x):
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            r.terms[s] = x*c
        return r

    def dag(self):
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            r.terms[s] = c.conjugate() * majorana_dag(s)
        return r

    def __str__(self):
        return ' + '.join([f'{self.terms[t]}*{str(t)}' for t in self.terms])


##############
# Hamiltonian

L = int(sys.argv[1])
mu = float(sys.argv[2])
t = float(sys.argv[3])
g = float(sys.argv[4])

H = 0.*Operator('I'*L)
for x in range(L-1):
    op = Operator('I'*x + 'GG' + 'I'*(L-x-2))
    if x%2 == 0:
        H += 1j * mu / 2. * op
    else:
        H += 1j * t * op

for x in range(L-3):
    op = Operator('I'*x + 'GGGG' + 'I'*(L-x-4))
    H += g * op


############################
# Semi-definite programming

# The set of operators that generate the positivity bounds.
ops = ['I'*L]
ops += ['I'*x + 'G' + 'I'*(L-x-1) for x in range(L)]
#ops += ['I'*x + 'G' + 'I'*y + 'G' + 'I'*(L-x-y-2) for x in range(L-1) for y in range(L-x-1)]
ops += ['I'*x + 'GG' + 'I'*(L-x-2) for x in range(L-1)]

# Now we can list all the operators we need.
evar_names = ['I'*L]
for t1 in ops:
    for t2 in ops:
        i1 = ops.index(t1)
        i2 = ops.index(t2)
        op = Operator(t1).dag() * Operator(t2)
        for (s,c) in op.terms.items():
            if s not in evar_names:
                evar_names.append(s)

evars = {n: picos.ComplexVariable(f'<{n}>') for n in evar_names}
idx = {evar_names[i]: i for i in range(len(evar_names))}

# Energy expectation value
eH = 0
for (s,c) in H.terms.items():
    eH += c*evars[s]

# The p.s-d. matrix.
M = picos.HermitianVariable("M", len(ops))

def make():
    problem = picos.Problem(verbosity=0)

    # Positivity bounds
    for t1 in ops:
        for t2 in ops:
            i1 = ops.index(t1)
            i2 = ops.index(t2)
            op = Operator(t1).dag() * Operator(t2)
            e = picos.Constant(0)
            for (s,c) in op.terms.items():
                e += c*evars[s]
            problem.add_constraint(M[i1,i2] == e)
    problem.add_constraint(M >> 0)

    # Fix normalization
    problem.add_constraint(evars['I'*L] == 1.)
    return problem

if __name__ == '__main__':
    # Just getting the ground state
    problem = make()
    problem.set_objective('min', eH.real)
    sol = problem.solve(solver = 'mosek', primals=True, duals=None)
    E0 = sol.reported_value
    print(f'# Energy: {E0}')

    for x in range(L):
        problem = make()
        problem.add_constraint(eH.real == E0 + 0.05)
        problem.set_objective('max', evars['I'*x+'G'+'I'*(L-1-x)].real)
        sol = problem.solve(solver = 'mosek', primals=True, duals=None)
        print(f'{x:3} {sol.reported_value}')

