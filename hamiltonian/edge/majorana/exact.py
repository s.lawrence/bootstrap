#!/usr/bin/env python3

"""
Kitaev chain
"""

import numpy as np
#from numpy import *
import sys
from scipy.sparse.linalg import eigs,eigsh,expm

if True:
    from scipy import sparse
    matrix = sparse.dok_matrix
    kron = sparse.kron
    eye = sparse.eye
else:
    from numpy import *


N = int(sys.argv[1])
mu = float(sys.argv[2])
t = float(sys.argv[3])
g = float(sys.argv[4])


G = [matrix(eye(1)*(1.+0j))]*N

sx = matrix([[0,1],[1,0]])
sz = matrix([[1,0],[0,-1]])

for n in range(N):
    for i in range(N):
        if n < i:
            G[i] = kron(G[i], sz, format='dok')
        elif n == i:
            G[i] = kron(G[i], sx, format='dok')
        else:
            G[i] = kron(G[i], eye(2), format='dok')

H = 0*eye(2**N, dtype='complex128')
for x in range(N-1):
    y = x+1
    if x%2 == 0:
        H += 1j * mu/2. * G[y] @ G[x]
    else:
        H += 1j * t * G[y] @ G[x]
for x in range(N-3):
    H += g * G[x] @ G[x+1] @ G[x+2] @ G[x+3]
Hcsc = sparse.csc_matrix(H)
Hvals, Hdiag = np.linalg.eigh(H.todense())
#Hvals, Hdiag = eigsh(H,k=6,which='SA')
E0 = Hvals[0]
Hvals -= Hvals[0]
Hvecs = [np.matrix(v).T for v in Hdiag.T]

print(E0)

