#!/usr/bin/env python

import numpy as np
import sys
from matplotlib import pyplot as plt

def read(filename):
    with open(filename) as f:
        dat = np.array([[float(x) for x in l.split()] for l in f.readlines()[1:]])
    return dat

dat_trivial = read('20_0.7_0.2_.dat')
dat_edge = read('20_0.1_1.0_.dat')
dat_diffuse = read('20_0.7_1.0_.dat')

plt.figure(figsize=(4.5,3.2), dpi=600)
plt.xlabel('$x$')
plt.ylabel('Maximum of $\langle \gamma_x \\rangle$')
plt.ylim([0.,1.1])
plt.plot(dat_trivial[:,0], dat_trivial[:,1], label='$\mu=0.7$, $t=0.2$', color='black', ls='dotted')
plt.plot(dat_diffuse[:,0], dat_diffuse[:,1], label='$\mu=0.7$, $t=1.0$', color='green', ls='dashed')
plt.plot(dat_edge[:,0], dat_edge[:,1], label='$\mu=0.1$, $t=1.0$', color='blue', ls='solid')
plt.legend(loc='best')
plt.tight_layout()
#plt.show()
plt.savefig('comparison.png')
#plt.savefig(sys.argv[1])

dat_trivial = read('interacting_20_0.7_0.0_0.2_.dat')
dat_edge = read('interacting_20_0.7_0.7_0.2_.dat')
dat_diffuse = read('interacting_20_0.1_0.7_0.2_.dat')

plt.figure(figsize=(4.5,3.2), dpi=600)
plt.xlabel('$x$')
plt.ylabel('Maximum of $\langle \gamma_x \\rangle$')
plt.ylim([0.,1.1])
plt.plot(dat_trivial[:,0], dat_trivial[:,1], label='$\mu=0.7$, $t=0.0$, $g=0.2$', color='black', ls='dotted')
plt.plot(dat_diffuse[:,0], dat_diffuse[:,1], label='$\mu=0.7$, $t=0.7$, $g=0.2$', color='green', ls='dashed')
plt.plot(dat_edge[:,0], dat_edge[:,1], label='$\mu=0.1$, $t=0.7$, $g=0.2$', color='blue', ls='solid')
plt.legend(loc='best')
plt.tight_layout()
#plt.show()
plt.savefig('interacting.png')
#plt.savefig(sys.argv[1])

