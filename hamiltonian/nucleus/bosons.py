#!/usr/bin/env python

import picos
import numpy as np

#######################
# Semidefinite program

problem = picos.Problem(verbosity=1)

sol = problem.solve(solver = 'mosek', primals=None, duals=None)

