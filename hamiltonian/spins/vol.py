#!/usr/bin/env python

# Working directly in the infinite-volume limit

import numpy as np
import picos
import sys


#########################
# Manipulating operators

def pauli_mul_1(a, b):
    if a == 'I':
        return b, 1.+0j
    if b == 'I':
        return a, 1.+0j
    if a == b:
        return 'I', 1.+0j
    if a == 'X' and b == 'Y':
        return 'Z', 1j
    if a == 'Y' and b == 'Z':
        return 'X', 1j
    if a == 'Z' and b == 'X':
        return 'Y', 1j
    r, c = pauli_mul(b, a)
    return r, -c

def pauli_mul(a, b):
    assert len(a) == len(b)
    c = 1.
    r = []
    for i in range(len(a)):
        s, d = pauli_mul_1(a[i], b[i])
        r.append(s)
        c *= d
    return ''.join(r), c

class Operator:
    def __init__(self, pauli=None, N=None):
        assert pauli is None or N is None
        assert pauli is not None or N is not None
        if N is not None:
            self.N = N
            self.terms = dict()
        if pauli is not None:
            self.N = len(pauli)
            self.terms = {pauli: 1.+0j}

    def __iadd__(self, op):
        assert self.N == op.N
        for s, c in op.terms.items():
            if s not in self.terms:
                self.terms[s] = 0.j
            self.terms[s] += c
        return self

    def __sub__(self, op):
        assert self.N == op.N
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __add__(self, op):
        assert self.N == op.N
        r = Operator(N=self.N)
        r += self
        r += op
        return r

    def __mul__(self, op):
        assert self.N == op.N
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            for sp, cp in op.terms.items():
                ns, nc = pauli_mul(s,sp)
                nc *= c*cp
                r.terms[ns] = nc
        return r

    def __rmul__(self, x):
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            r.terms[s] = x*c
        return r

    def dag(self):
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            r.terms[s] = c.conjugate()
        return r

    def __str__(self):
        return ' + '.join([f'{self.terms[t]}*{str(t)}' for t in self.terms])


######################
# Hamiltonian density

#L = int(sys.argv[1])
#mu = float(sys.argv[2])
mu = 0

if False:
    H = 0.*Operator('I'*L)
    H -= mu*Operator('X' + 'I'*(L-1))
    H -= 1/3 * Operator('XX' + 'I'*(L-2))
    H -= 1/2 * Operator('YY' + 'I'*(L-2))
    H -= Operator('ZZ' + 'I'*(L-2))

H = -mu*(Operator('IXII')+Operator('IIXI')) - 1/3 * Operator('IXXI') - 1/2 * Operator('IYYI') - 1 * Operator('IZZI')

############################
# Semi-definite programming

# The set of operators that generate the positivity bounds.
#ops = ['I'*L, 'X'+'I'*(L-1), 'XX'+'I'*(L-2), 'YY'+'I'*(L-2), 'ZZ'+'I'*(L-2)]
ops = ['IIII', 'IXII', 'IIXI', 'IXXI', 'IYYI', 'IZZI']
ops_ = ['IIII', 'IXII', 'IIXI', 'IXXI', 'IYYI', 'IZZI', 'ZIZI']

# The set of operators that we might add to generate positivity bounds.
#ops_ = ['I'*L]
#ops_ += ['I'*x + 'X' + 'I'*(L-x-1) for x in range(L)]
#ops_ += ['I'*x + 'ZZ' + 'I'*(L-x-2) for x in range(L-1)]
#ops_ += ['I'*x + 'Y' + 'I'*(L-x-1) for x in range(L)]
#ops_ += ['I'*x + 'Z' + 'I'*(L-x-1) for x in range(L)]
#ops_ += ['I'*x + 'XX' + 'I'*(L-x-2) for x in range(L-1)]
#ops_ += ['I'*x + 'YY' + 'I'*(L-x-2) for x in range(L-1)]
#ops_ += ['I'*x + 'XIX' + 'I'*(L-x-3) for x in range(L-2)]
#ops_ += ['I'*x + 'YIY' + 'I'*(L-x-3) for x in range(L-2)]
#ops_ += ['I'*x + 'ZIZ' + 'I'*(L-x-3) for x in range(L-2)]
#ops_ += ['I'*x + 'XIIX' + 'I'*(L-x-4) for x in range(L-3)]
#ops_ += ['I'*x + 'YIIY' + 'I'*(L-x-4) for x in range(L-3)]
#ops_ += ['I'*x + 'ZIIZ' + 'I'*(L-x-4) for x in range(L-3)]

def bound(ops):
    # Now we can list all the operators we need.
    evar_names = ['IIII']
    for t1 in ops:
        for t2 in ops:
            i1 = ops.index(t1)
            i2 = ops.index(t2)
            op = Operator(t1).dag() * Operator(t2)
            for (s,c) in op.terms.items():
                if s not in evar_names:
                    evar_names.append(s)

    evars = {n: picos.ComplexVariable(f'<{n}>') for n in evar_names}
    idx = {evar_names[i]: i for i in range(len(evar_names))}

    problem = picos.Problem()

    # We target the energy
    eH = 0
    for (s,c) in H.terms.items():
        eH += c*evars[s]
    problem.set_objective('min', eH.real)

    # Positivity bounds
    M = picos.HermitianVariable("M", len(ops))
    for t1 in ops:
        for t2 in ops:
            i1 = ops.index(t1)
            i2 = ops.index(t2)
            op = Operator(t1).dag() * Operator(t2)
            e = picos.Constant(0)
            for (s,c) in op.terms.items():
                e += c*evars[s]
            problem.add_constraint(M[i1,i2] == e)
    problem.add_constraint(M >> 0)

    # Translational invariance.
    def rot(l, x):
        return l[-x:] + l[:-x]

    for op in ops:
        for x in range(-3,3):
            opp = rot(op, x)
            try:
                problem.add_constraint(evars[op] == evars[opp])
            except:
                pass

    # Fix normalization
    problem.add_constraint(evars['IIII'] == 1.)

    picos.modeling.file_out.write(problem, 'problem.mosek', 'mosek')
    sol = problem.solve(solver = 'mosek', primals=True, duals=None)
    return sol.reported_value

print(bound(ops))
print(bound(ops_))

