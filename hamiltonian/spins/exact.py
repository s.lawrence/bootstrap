#!/usr/bin/env python

import numpy as np
import sys

L = int(sys.argv[1])
mu = float(sys.argv[2])

#Jx = 1/3.
#Jy = 0.
#Jz = 0.

Jx = 0.
Jy = 0.
Jz = 1.

pauli_i = np.array([[1,0],[0,1]],dtype=np.complex128)
pauli_x = np.array([[0,1],[1,0]],dtype=np.complex128)
pauli_y = np.array([[0,-1j],[1j,0]],dtype=np.complex128)
pauli_z = np.array([[1,0],[0,-1]],dtype=np.complex128)

sigma_x = [np.ones((1,1), dtype=np.complex128) for _ in range(L)]
sigma_y = [np.ones((1,1), dtype=np.complex128) for _ in range(L)]
sigma_z = [np.ones((1,1), dtype=np.complex128) for _ in range(L)]

for x in range(L):
    for y in range(L):
        if x == y:
            sigma_x[x] = np.kron(sigma_x[x], pauli_x)
            sigma_y[x] = np.kron(sigma_y[x], pauli_y)
            sigma_z[x] = np.kron(sigma_z[x], pauli_z)
        else:
            sigma_x[x] = np.kron(sigma_x[x], pauli_i)
            sigma_y[x] = np.kron(sigma_y[x], pauli_i)
            sigma_z[x] = np.kron(sigma_z[x], pauli_i)

D = 2**L
H = np.zeros((D,D), dtype=np.complex128)
for x in range(L):
    xp = (x+1)%L
    H -= mu*sigma_x[x]
    if L == 1:
        continue
    if xp == 0:
        continue
    H -= Jx*sigma_x[x]@sigma_x[xp]
    H -= Jy*sigma_y[x]@sigma_y[xp]
    H -= Jz*sigma_z[x]@sigma_z[xp]

vals, vecs = np.linalg.eigh(H)

print(f'{vals[0]} ({vals[0]/L})')
