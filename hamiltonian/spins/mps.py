#!/usr/bin/env python

import numpy as np
from scipy.optimize import minimize
import sys

L = int(sys.argv[1])
mu = float(sys.argv[2])
D = int(sys.argv[3])


######################
# Hamiltonian density

s0 = np.eye(2,dtype=np.complex128)
sx = np.array([[0,1],[1,0]],dtype=np.complex128)
sy = np.array([[0,-1j],[1j,0]],dtype=np.complex128)
sz = np.array([[1,0],[0,-1]],dtype=np.complex128)

J_x = 1/3.
J_y = 1/2.
J_z = 1.

H = -0.5*mu*(np.einsum('ij,kl->ijkl',sx,s0) + np.einsum('ij,kl->ijkl',s0,sx))
H -= J_x*np.einsum('ij,kl->ijkl',sx,sx)
H -= J_y*np.einsum('ij,kl->ijkl',sy,sy)
H -= J_z*np.einsum('ij,kl->ijkl',sz,sz)


###################
# MPS contractions

def mps_norm(A):
    B = np.einsum('sij,skl->ijkl', A, A.conj())
    C = B
    for n in range(L-1):
        C = np.einsum('ijkl,jmln->imkn', C, B)
    return np.einsum('iijj->', C).real

def mps_op1(A, M):
    B = np.einsum('sij,skl->ijkl', A, A.conj())
    Bop = np.einsum('sij,st,tkl->ijkl', A, M, A.conj())
    C = Bop
    for n in range(L-1):
        C = np.einsum('ijkl,jmln->imkn', C, B)
    return np.einsum('iijj->', C).real

def mps_op2(A, M):
    B = np.einsum('sij,skl->ijkl', A, A.conj())
    Bop = np.einsum('dab,ebc,dfeg,fhi,gij->achj', A, A, M, A.conj(), A.conj())
    C = Bop
    for n in range(L-2):
        C = np.einsum('ijkl,jmln->imkn', C, B)
    return np.einsum('iijj->', C).real


#########
# Search

def energy(A_ri):
    A_ri = A_ri.reshape(2,D,D,2)
    A = A_ri[...,0] + 1j*A_ri[...,1]
    return mps_op2(A,H) / mps_norm(A)

opt = minimize(energy, np.random.normal(size=(2,D,D,2)))
A_ri = opt.x.reshape(2,D,D,2)
A = A_ri[...,0] + 1j*A_ri[...,1]
print(mps_op2(A,H) / mps_norm(A))

