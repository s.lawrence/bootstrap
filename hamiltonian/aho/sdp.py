#!/usr/bin/env python

from mpmath import mp
import sys

mp.dps = int(sys.argv[1])

##################
# SDP formulation

def sdp(x2, x4, x6, x8, x10, x12, x14, x16, p2, px, px3, px5, px7):
    # 1, p, x, x2, x3, x4, x5, x6, x7, x8
    X = mp.matrix(10)

    X[0,0] = 1
    X[0,3] = X[3,0] = x2
    X[0,5] = X[5,0] = x4
    X[0,7] = X[7,0] = x6
    X[0,9] = X[9,0] = x8

    X[1,1] = p2

    X[1,2] = px
    X[1,4] = px3
    X[1,6] = px5
    X[1,8] = px7

    X[2,1] = px + 1j
    X[4,1] = px3 + 3j*x2
    X[6,1] = px5 + 5j*x4
    X[8,1] = px7 + 7j*x6

    X[2,2] = x2
    X[4,2] = X[3,3] = X[2,4] = x4
    X[6,2] = X[5,3] = X[4,4] = X[3,5] = X[2,6] = x6
    X[8,2] = X[7,3] = X[6,4] = X[5,5] = X[4,6] = X[3,7] = X[2,8] = x8
    X[9,3] = X[8,4] = X[7,5] = X[6,6] = X[5,7] = X[4,8] = X[3,9] = x10
    X[9,5] = X[8,6] = X[7,7] = X[6,8] = X[5,9] = x12
    X[9,7] = X[8,8] = X[7,9] = x14
    X[9,9] = x16

    return X

#################
# Starting point

# Get initial guess from direct diagonalization.
def initial_expectations(N):
    m = mp.mpf(1.)
    g = mp.mpf(1.)

    a = mp.matrix(N)
    for i in range(N-1):
        a[i,i+1] = mp.sqrt(i+1)*mp.sqrt(m)
    a = mp.matrix(a)
    adag = a.transpose().conjugate()

    x = 1/(m*mp.sqrt(2)) * (a + adag)
    p = 1j*mp.sqrt(m/2) * (adag - a)
    H = adag@a + 1/2 * mp.eye(N) + g*x@x@x@x
    #H = p@p + x@x@x@x

    E, U = mp.eighe(H)
    gnd = U[:,0]
    def e(op):
        return (gnd.conjugate().transpose() @ op @ gnd)[0,0]
    p2 = p @ p
    x2 = x @ x
    x3 = x2 @ x
    x4 = x3 @ x
    x5 = x4 @ x
    x6 = x5 @ x
    x7 = x6 @ x
    x8 = x7 @ x
    x10 = x8 @ x2
    x12 = x10 @ x2
    x14 = x12 @ x2
    x16 = x14 @ x2
    return e(x2),e(x4),e(x6),e(x8),e(x10),e(x12),e(x14),e(x16),e(p2),e(p@x),e(p@x3),e(p@x5),e(p@x7)

for N in range(10,100,5):
    expects = initial_expectations(N)
    X = sdp(*expects)
    print(N, mp.nstr(mp.eighe(X, eigvals_only=True)[0], 5))
