#!/usr/bin/env python

from mpmath import mp
import numpy as np
import sys

mp.dps = int(sys.argv[1])
N = int(sys.argv[2])

m = mp.mpf(1)
g = mp.mpf(0.25)

a = np.zeros((N,N))
for i in range(N-1):
    a[i,i+1] = mp.sqrt(i+1)*mp.sqrt(m)
adag = a.transpose().conjugate()

x = 1/(m*mp.sqrt(2)) * (a + adag)
p = 1j*mp.sqrt(m/2) * (adag - a)
H = adag@a + 1/2 * np.eye(N) + g*x@x@x@x

E, U = mp.eigh(mp.matrix(H))
print(E[0])

