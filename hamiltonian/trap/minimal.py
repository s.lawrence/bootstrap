#!/usr/bin/env python

"""
One species of fermions in a one-dimensional harmonic trap.
"""

import itertools
import numbers
import picos
import numpy as np
from scipy.special import factorial, hermite
import sys

############
# Arguments

# Number of particles
N = int(sys.argv[1])
# Truncation
K = int(sys.argv[2])
# Harmonic trap
m = float(sys.argv[3])
omega = float(sys.argv[4])
# Interaction potential
V0 = float(sys.argv[5])
r0 = float(sys.argv[6])


############################
# Harmonic oscillator basis

def wavefunction(n, x):
    r = 1/np.sqrt(2**n * factorial(n)) * (m*omega/np.pi)**(1/4)
    r *= np.exp(-m*omega*x**2/2)
    r *= hermite(n)(np.sqrt(m*omega)*x)
    return r

# Check normalizations
if True:
    x = np.linspace(-10,10,10000)
    for n in range(20):
        wf = wavefunction(n,x)
        norm = np.trapz(wf.conj()*wf,x)
        if np.abs(norm-1) > 1e-8:
            raise "Bad normalization!"


############
# Operators

class BasisOperator:
    """
    Second-quantized operators.

    Each basis operator is a product of creation and annihilation operators.

    The list of modes annihilated is stored in `annihilate`, and the list of
    modes created is stored in `create`. All annihilation operators are applied
    first (in increasing order of mode number), and then all creation operators
    (in the opposite order) follow. As a result the adjoint of any
    BasisOperator is itself a BasisOperator, without any sign.
    """
    def __init__(self, n=None):
        """
        If `n` is specified, the operator is the annihilation operator of the
        given mode. Otherwise, the operator represented is the identity.
        """
        self.create = set()
        if n is None:
            self.annihilate = set()
        else:
            self.annihilate = {n}

    def __repr__(self):
        return repr(self.create) + repr(self.annihilate)

    def __hash__(self):
        return hash(repr(self))

    def __eq__(self, o):
        return repr(self) == repr(o)

    def dag(self):
        r = BasisOperator()
        r.create = self.annihilate.copy()
        r.annihilate = self.create.copy()
        return r

    def __add__(self, op):
        return Operator(self) + Operator(op)

    def __mul__(self, op):
        if isinstance(op, Operator):
            return Operator(self)*op
        assert isinstance(op, BasisOperator)
        # We approach this recursively. Only if the right operator (`op`) has
        # zero or one creation operators, the multiplication can be done
        # immediately.
        if len(op.create) > 1:
            # Recurse! Split the operator into two parts.
            m = list(op.create)[0]
            op1 = BasisOperator(m).dag()
            op2 = op.copy()
            op2.create.discard(m)
            # Multiply the first, then the second, then return.
            return (self*op1)*op2

        if len(op.create) == 1 and list(op.create)[0] in self.annihilate:
            # Now there are two terms.
            m = list(op.create)[0]
            
            # The number of swaps needed to get the creation and annihilation
            # operators to be adjacent.
            sign1 = (-1)**len(list(filter(lambda n: n<m,self.annihilate)))
            self1 = self.copy()
            self1.annihilate.discard(m)
            op1 = op.copy()
            op1.create.discard(m)
            term1 = sign1 * self1*op1

            # In the second term, the two operators just pick up a minus sign
            # in the usual way. This term only exists if the creation operator
            # isn't already present in `self`.
            if m in self.create:
                return term1
            sign2 = (-1)**len(self.annihilate)
            sign2 *= (-1)**len(list(filter(lambda n: n>m,self.create)))
            self2 = self.copy()
            self2.create.add(m)
            op2 = op.copy()
            op2.create.discard(m)
            term2 = sign2 * self2*op2
            return term1+term2

        if len(self.create.intersection(op.create)) > 0 or len(self.annihilate.intersection(op.annihilate)) > 0:
            return Operator()*0

        r = self.copy()
        sign = 1

        # Pull `op.create` in.
        for n in op.create:
            r.create.add(n)
            sign *= (-1)**len(self.annihilate)
            sign *= (-1)**len(list(filter(lambda m: m>n,self.create)))

        # Now pull `op.annihilate` in. This just means counting the number of
        # swaps required.
        r.annihilate.update(op.annihilate)
        sign *= (-1)**sum(len(list(filter(lambda m: m<n, self.annihilate))) for n in op.annihilate)
        return sign*r
 
    def __rmul__(self, x):
        if x == 1.:
            return self
        return Operator(self)*x

    def __str__(self):
        creates = ','.join(map(str,self.create))
        annihilates = ','.join(map(str,self.annihilate))
        return f'create({creates})*annihilate({annihilates})'

    def copy(self):
        r = BasisOperator()
        r.create = self.create.copy()
        r.annihilate = self.annihilate.copy()
        return r

class Operator:
    def __init__(self, b=BasisOperator()):
        if isinstance(b,BasisOperator):
            self.terms = {b: 1.}
        else:
            self.terms = b.terms.copy()

    def copy(self):
        r = Operator()
        r.terms = self.terms.copy()
        return r

    def dag(self):
        r = Operator()
        r.terms = {o.dag(): c.conjugate() for (o,c) in self.terms.items()}
        return r

    def __iadd__(self, op):
        if isinstance(op,BasisOperator):
            op = Operator(op)
        for (o,c) in op.terms.items():
            if o not in self.terms:
                self.terms[o] = 0.
            self.terms[o] += c
        return self

    def __add__(self, op):
        r = self.copy()
        r += op
        return r

    def __sub__(self, op):
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __radd__(self, x):
        assert x == 0
        return self.copy()

    def __mul__(self, op):
        if isinstance(op,numbers.Number):
            return op*self
        if isinstance(op,BasisOperator):
            op = Operator(op)
        r = Operator()
        r.terms = {}
        for (o,c) in self.terms.items():
            for (o_,c_) in op.terms.items():
                r += c*c_*(o*o_)
        return r

    def __rmul__(self, x):
        assert isinstance(x, numbers.Number)
        r = Operator()
        r.terms = {o: c*x for (o,c) in self.terms.items()}
        return r

    def __truediv__(self, x):
        assert isinstance(x, numbers.Number)
        r = Operator()
        r.terms = {o: c/x for (o,c) in self.terms.items()}
        return r

    def __str__(self):
        return ' + '.join(f'{c}*{str(o)}' for (o,c) in self.terms.items())

    def __iter__(self):
        yield from self.terms.items()


##############
# Hamiltonian

H = 0*Operator()
# Free
for k in range(K):
    a = BasisOperator(k)
    n = a.dag()*a
    H += omega*(k+0.5)*n

# Interactions
# TODO


#########
# Number

Nop = 0*Operator()
for k in range(K):
    a = BasisOperator(k)
    n = a.dag()*a
    Nop += n


########
# Basis

basis = {b for b, c in H}
basis.update({BasisOperator(k) for k in range(K)})

basis.update({b.dag() for b in basis})
basis = list(basis)


########
# Solve

evars = {op: picos.ComplexVariable(f'<{op}>') for op in basis}

def expect(op):
    if isinstance(op,BasisOperator):
        return evars[op]
    r = picos.Constant(0)
    for (b,c) in op:
        r += c*evars[b]
    return r

problem = picos.Problem(verbosity=0)
problem.set_objective('min', expect(H).real)
M = picos.HermitianVariable("M", len(basis))
problem += M >> 0
for (i1, b1) in enumerate(basis):
    for (i2, b2) in enumerate(basis):
        try:
            problem += M[i1,i2] == expect(b1.dag()*b2)
        except KeyError as missing:
            nop = missing.args[0]
            print('Missing: ', nop)
problem += expect(Operator()) == 1.
problem += expect(Nop) == N
sol = problem.solve(solver='mosek', primals=True, duals=None)
print(sol.reported_value)

