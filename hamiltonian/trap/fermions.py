#!/usr/bin/env python

"""
Fermions in a harmonic trap. Supports either first or second quantization.
"""

SECOND_QUANTIZED = True

import itertools
import picos
import sys

############
# Operators

if not SECOND_QUANTIZED:
    # TODO need swap operators
    class BasisOperator:
        """
        First-quantized operators.

        The dictionary `parts` maps (species,number) to (x,y). The operator
        being represented is obtained by lexicographically sorting the keys,
        and then taking the product of all (adag^x a^y).
        """
        def __init__(self, sn=None):
            """
            If no arguments are given, the identity operator is created.
            Otherwise, the operator represented is the annihilation operator on
            the oscillator corresponding to the species and number given by
            `sn`.
            """
            if sn is None:
                self.parts = {}
            else:
                self.parts = {sn: (0,1)}

        def __repr__(self):
            return repr(self.parts)

        def __hash__(self):
            return hash(repr(self))

        def __eq__(self, o):
            return repr(self) == repr(o)

        def dag(self):
            r = BasisOperator()
            r.parts = {(s,n): (c,d) for ((s,n),(c,d)) in self.parts.items()}
            return r

        def __add__(self, op):
            return Operator(self) + Operator(op)

        def __mul__(self, op):
            # TODO
            pass

        def __rmul__(self, x):
            if x == 1.:
                return self
            return Operator(self)*x

else:
    class BasisOperator:
        """
        Second-quantized operators.

        Each basis operator is a product of fermionic modes; each mode is
        identified by a non-negative integer (number basis of the harmonic
        oscillator) and a species. To each mode is associated an integer: -1
        for the annihilation operator, 1 for the creation operator, and so on.
        """
        def __init__(self, ns=None):
            """
            If `ns` is specified, it is an ordered pair giving the HO mode and
            the species --- the annihilation operator of that mode is
            represented.  Otherwise, the operator represented is the identity.
            """
            if ns is None:
                self.modes = {}
            else:
                self.modes = {ns: -1}

        def __repr__(self):
            return repr(self.modes)

        def __hash__(self):
            return hash(repr(self))

        def __eq__(self, o):
            return repr(self) == repr(o)

        def dag(self):
            r = BasisOperator()
            for ((n,s),k) in sorted(self.modes.items()):
                op = BasisOperator()
                op.modes = {(n,s): -k}
                r *= op
            return r

        def __add__(self, op):
            return Operator(self) + Operator(op)

        def __mul__(self, op):
            if len(op.modes) > 1:
                # TODO
                pass
            if len(op.modes) == 0:
                return self.copy()
            r = self.copy()
            for ((n,s),k) in sorted(self.modes.items()):
                op_ = BasisOperator()
                op_.modes = {(n,s): k}
                r *= op
            return r

        def __rmul__(self, x):
            if x == 1.:
                return self
            return Operator(self)*x

        def __str__(self):
            # TODO
            return 'BASISOPERATOR'

        def copy(self):
            r = BasisOperator()
            r.modes = self.modes.copy()
            return r

class Operator:
    def __init__(self, b=BasisOperator()):
        self.terms = {b: 1.}

    def copy(self):
        r = Operator()
        r.terms = self.terms.copy()
        return r

    def dag(self):
        r = Operator()
        r.terms = {o.dag(): c.conjugate() for (o,c) in self.terms}
        return r

    def __iadd__(self, op):
        for (o,c) in op.terms:
            if o not in self.terms:
                self.terms[o] = 0.
            self.terms[o] += c

    def __add__(self, op):
        r = self.copy()
        r += op
        return r

    def __sub__(self, op):
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __radd__(self, x):
        assert x == 0
        return self.copy()

    def __mul__(self, op):
        if isinstance(op,BasisOperator):
            op = Operator(op)
        r = Operator()
        r.terms = []
        for (o,c) in self.terms:
            for (o_,c_) in op.terms:
                r += c*c_*(o*o_)
        return r

    def __rmul__(self, x):
        r = Operator()
        r.terms = [(o,c*x) for (o,c) in self.terms]
        return r

    def __truediv__(self, x):
        assert isinstance(x, numbers.Number)
        r = Operator()
        r.terms = {o: c/x for (o,c) in self.terms}
        return r

    def __str__(self):
        return ' + '.join(f'{c}*{str(o)}' for (o,c) in self.terms)

    def __iter__(self):
        yield from self.terms.items()


##############
# Hamiltonian

# There are two species
N1 = int(sys.argv[1])
N2 = int(sys.argv[2])
# One dimension
D = 1
# Harmonic trap
m = float(sys.argv[3])
omega = float(sys.argv[4])
# Interaction potential
V0 = float(sys.argv[5])
r0 = float(sys.argv[6])

if not SECOND_QUANTIZED:
    # First-quantized Hamiltonian (TODO)
    H = Operator()
    # Species 1
    for n in range(N1):
        pass
    # Species 2
    for n in range(N2):
        pass
else:
    # Second-quantized Hamiltonian

    # Maximum HO mode
    K = 5

    H = Operator()
    # Free
    for k in range(K):
        a0 = Operator((k,0))
        # TODO
        n = a.dag()*a
        H += n

    # Interactions
    # TODO

    if True:
        print(H)


########
# Basis

basis = {b for b, c in H}
if not SECOND_QUANTIZED:
    # TODO
    pass
else:
    # TODO
    pass

basis = list(basis)

########
# Solve

evars = {op: picos.ComplexVariable(f'<{op}>') for op in basis}

def expect(op):
    if isinstance(op,BasisOperator):
        return evars[op]
    r = picos.Constant(0)
    for (b,c) in op:
        r += c*evars[b]
    return r

problem = picos.Problem(verbosity=0)
problem.set_objective('min', expect(H).real)
if not SECOND_QUANTIZED:
    # TODO impose fermionic
    pass
M = picos.HermitianVariable("M", len(basis))
problem += M >> 0
for (i1, b1) in enumerate(basis):
    for (i2, b2) in enumerate(basis):
        try:
            problem += M[i1,i2] == expect(b1.dag()*b2)
        except KeyError as missing:
            print('Missing: ', missing)
problem += expect(Operator()) == 1.
sol = problem.solve(solver='mosek', primals=True, duals=None)
print(sol.reported_value)

