#!/usr/bin/env python

import itertools
import numbers
import numpy as np
import picos
import sys

##########
# Momenta

#momenta = [-2.,-1.,0.,1.,2.]
momenta = [-1.,0.,1.]
momentum_eps = 1e-6

def momentum_index(p):
    for (i,k) in enumerate(momenta):
        if abs(k-p) < momentum_eps:
            return i
    raise ValueError(p)


#########################
# Manipulating operators

class BasisOperator:
    def __init__(self, k=None):
        if k is None:
            self.modes = {}
        else:
            self.modes = {momentum_index(k): (0,1)}

    def boost(self, k):
        b = BasisOperator()
        b.modes = {momentum_index(k+momenta[ki]): o for (ki,o) in self.modes.items()}
        return b

    def dag(self):
        b = BasisOperator()
        b.modes = {ki: (ds,cs) for (ki,(cs,ds)) in self.modes.items()}
        return b

    def __len__(self):
        return len(self.modes)

    def __repr__(self):
        return repr(self.modes)

    def __hash__(self):
        return hash(repr(self))

    def __eq__(self, o):
        return repr(self) == repr(o)

    def __str__(self):
        if len(self.modes) == 0:
            return '1'
        return '*'.join(f'({momenta[ki]})({cs},{ds})' for (ki,(cs,ds)) in self.modes.items())

    def __add__(self, op):
        return Operator(self) + Operator(op)

    def __mul__(self, op):
        """
        Multiplication of two BasisOperators. Note that this generally returns
        an Operator, not a BasisOperator.
        """

        def product_terms(cs,ds,cs_,ds_):
            if ds == 0 or cs_ == 0:
                yield ((cs+cs_,ds+ds_),1)
                return
            yield from product_terms(cs+1,ds,cs_-1,ds_)
            for ((c,d),n) in product_terms(cs,ds-1,cs_-1,ds_):
                yield ((c,d),n*ds)

        if len(op.modes) == 0:
            r = BasisOperator()
            r.modes = self.modes.copy()
            return r
        if len(op.modes) == 1:
            ki, (cs_, ds_) = list(op.modes.items())[0]
            if ki not in self.modes:
                r = BasisOperator()
                r.modes = self.modes.copy()
                r.modes[ki] = (cs_,ds_)
                return r
            cs, ds = self.modes[ki]
            pterms = list(product_terms(cs,ds,cs_,ds_))
            if len(pterms) == 1 and pterms[0][1] == 1:
                (c,d),n = pterms[0]
                b = BasisOperator()
                b.modes = self.modes.copy()
                if c > 0 or d > 0:
                    b.modes[ki] = (c,d)
                return b
            r = Operator()
            r.terms = []
            for (c,d),n in pterms:
                b = BasisOperator()
                b.modes = self.modes.copy()
                if c > 0 or d > 0:
                    b.modes[ki] = (c,d)
                else:
                    b.modes = {}
                r += n*b
            return r
        ks = list(op.modes)
        b1 = BasisOperator()
        brest = BasisOperator()
        b1.modes = {ks[0]: op.modes[ks[0]]}
        brest.modes = {ki: op.modes[ki] for ki in ks[1:]}
        return (self*b1)*brest

    def __rmul__(self, x):
        assert isinstance(x, numbers.Number)
        return x*Operator(self)

class Operator:
    def __init__(self, b=BasisOperator()):
        self.terms = [(b, 1.)]

    def boost(self, k):
        r = Operator()
        r.terms = [(o.boost(k),c) for (o,c) in self.terms]
        return r

    def dag(self):
        r = Operator()
        r.terms = [(o.dag(),c.conjugate()) for (o,c) in self.terms]
        return r

    def __add__(self, op):
        r = Operator()
        terms = {}
        for (o,c) in itertools.chain(self.terms, op.terms):
            if o not in terms:
                terms[o] = 0.
            terms[o] += c
        r.terms = terms.items()
        return r

    def __sub__(self, op):
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __mul__(self, op):
        if isinstance(op,BasisOperator):
            op = Operator(op)
        r = Operator()
        r.terms = []
        for (o,c) in self.terms:
            for (o_,c_) in op.terms:
                r += c*c_*(o*o_)
        return r

    def __truediv__(self, x):
        assert isinstance(x, numbers.Number)
        r = Operator()
        r.terms = [(o,c/x) for (o,c) in self.terms]
        return r

    def __radd__(self, x):
        assert x == 0
        r = Operator()
        r.terms = self.terms.copy()
        return r

    def __rmul__(self, x):
        r = Operator()
        r.terms = [(o,c*x) for (o,c) in self.terms]
        return r

    def __str__(self):
        return ' + '.join(f'{c}*{str(o)}' for (o,c) in self.terms)

a = BasisOperator(0.)
adag = a.dag()
n = adag*a


##############
# Hamiltonian

m2 = float(sys.argv[1])
lamda = float(sys.argv[2])

H_free = sum((m2+k**2) * n.boost(k) for k in momenta)
H = H_free
for k in itertools.product(momenta, momenta, momenta, momenta):
    if abs(sum(k)) < momentum_eps:
        omega = [np.sqrt(m2+p**2) for p in k]
        # TODO fix normalization
        # TODO wick ordering
        f = lamda / np.sqrt(np.prod(omega))
        opk = [a.boost(p) + adag.boost(-p) for p in k]
        op = opk[0]*opk[1]*opk[2]*opk[3]
        H += f * op

print(f'(Partial) Hamiltonian: {H}')


#############################
# Operators and expectations

basis = [BasisOperator()]
#lops = [a, adag, adag*a, a*a, adag*adag, a*a*a, adag*adag*adag, a*a*a*a, adag*adag*adag*adag, adag*adag*adag*a, adag*a*a*a, adag*adag*a*a, adag*a*a, adag*adag*a, adag*adag*adag*adag*adag, adag*adag*adag*adag*a, adag*adag*adag*a*a, adag*adag*a*a*a, adag*a*a*a*a, a*a*a*a*a, adag*adag*adag*adag*adag*adag, adag*adag*adag*adag*adag*a, adag*adag*adag*adag*a*a, adag*adag*adag*a*a*a, adag*adag*a*a*a*a, adag*a*a*a*a*a, a*a*a*a*a*a]
lops = [a, adag, adag*a, a*a, adag*adag, adag*adag*adag, adag*adag*a, adag*a*a, a*a*a, adag*adag*adag*adag, adag*adag*adag*a, adag*adag*a*a, adag*a*a*a, a*a*a*a]
basis += [o.boost(k) for k in momenta for o in lops]

evars = {b: picos.ComplexVariable(f'<{b}>') for b in basis}

def expect(op):
    if isinstance(op,BasisOperator):
        return evars[op]
    r = picos.Constant(0)
    for (b,c) in op.terms:
        try:
            r += c*evars[b]
        except KeyError as missing:
            print('Missing: ', missing)
    return r


############################
# Semi-definite programming

problem = picos.Problem(verbosity=1)

# Target the expectation value of H
problem.set_objective('min', expect(H).real)

# Positivity bounds
M = picos.HermitianVariable("M", len(basis))
problem += M >> 0
for (i1, b1) in enumerate(basis):
    for (i2, b2) in enumerate(basis):
        try:
            problem += M[i1,i2] == expect(b1.dag()*b2)
        except KeyError as missing:
            print('Missing: ', missing)

# Fix normalization
problem += expect(Operator()) == 1.

#print(problem)
sol = problem.solve(solver='mosek', primals=None, duals=None)
print(sol.reported_value)
print(expect(adag*a))

