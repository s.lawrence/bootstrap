#!/usr/bin/env python

"""
At the moment, this approach appears not to work, because commutators involve
delta functions, which will quickly become an unholy mess.
"""

import itertools
import numbers
import numpy as np
import picos
import sys

#########################
# Manipulating operators

class BasisOperator:
    def __init__(self):
        pass # TODO

    def __repr__(self):
        # TODO
        return 'TODO'

    def __hash__(self):
        return hash(repr(self))

    def __eq__(self, o):
        return repr(self) == repr(o)

    def dag(self):
        pass # TODO

    def __add__(self, op):
        pass # TODO

    def __mul__(self, op):
        pass # TODO

    def __rmul__(self, x):
        pass # TODO

class Operator:
    def __init__(self, b=BasisOperator()):
        self.terms = {b: 1.}

    def copy(self):
        r = Operator()
        r.terms = self.terms.copy()
        return r

    def dag(self):
        r = Operator()
        r.terms = {o.dag(): c.conjugate() for (o,c) in self.terms}
        return r

    def __iadd__(self, op):
        for (o,c) in op.terms:
            if o not in self.terms:
                self.terms[o] = 0.
            self.terms[o] += c

    def __add__(self, op):
        r = self.copy()
        r += op
        return r

    def __sub__(self, op):
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __radd__(self, x):
        assert x == 0
        return self.copy()

    def __mul__(self, op):
        if isinstance(op,BasisOperator):
            op = Operator(op)
        r = Operator()
        r.terms = []
        for (o,c) in self.terms:
            for (o_,c_) in op.terms:
                r += c*c_*(o*o_)
        return r

    def __rmul__(self, x):
        r = Operator()
        r.terms = [(o,c*x) for (o,c) in self.terms]
        return r

    def __truediv__(self, x):
        assert isinstance(x, numbers.Number)
        r = Operator()
        r.terms = {o: c/x for (o,c) in self.terms}
        return r

    def __str__(self):
        return ' + '.join(f'{c}*{str(o)}' for (o,c) in self.terms)

    def __iter__(self):
        yield from self.terms.items()


##############
# Hamiltonian

m2 = float(sys.argv[1])
lamda = float(sys.argv[2])

H = Operator() # TODO


#################
# Basis operatos

basis = {BasisOperator(), *(b for b,c in H)}
basis = list(basis)
# TODO


###############
# Expectations

evars = {op: picos.ComplexVariable(f'<{op}>') for op in basis}

def expect(op):
    if isinstance(op,BasisOperator):
        return evars[op]
    r = picos.Constant(0)
    for (b,c) in op:
        r += c*evars[b]
    return r


############################
# Semi-definite programming

problem = picos.Problem(verbosity=0)
problem.set_objective('min', expect(H).real)
M = picos.HermitianVariable("M", len(basis))
problem += M >> 0
for (i1, b1) in enumerate(basis):
    for (i2, b2) in enumerate(basis):
        try:
            problem += M[i1,i2] == expect(b1.dag()*b2)
        except KeyError as missing:
            print('Missing: ', missing)
problem += expect(Operator()) == 1.
sol = problem.solve(solver='mosek', primals=True, duals=None)
print(sol.reported_value)

