#!/bin/sh

set -e 

m2=0.04
for lambda in `seq 0 0.005 0.1`; do
	./ground.py $m2 $lambda 1 2
	./ground.py $m2 $lambda 2 3
	./ground.py $m2 $lambda 3 4
	./ground.py $m2 $lambda 4 5
	./ground.py $m2 $lambda 5 6
	./ground.py $m2 $lambda 6 7
	./ground.py $m2 $lambda 7 8
done
