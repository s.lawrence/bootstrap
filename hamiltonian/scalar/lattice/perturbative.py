#!/usr/bin/env python

import numpy as np
import sys

L = int(sys.argv[1])
m2 = float(sys.argv[2])

def E_free(L):
    n = np.arange(L)
    k = 2*np.pi * n / L
    E = np.sqrt(m2 + (2*np.sin(k/2))**2)/2
    return np.sum(E)

print(L, E_free(L), E_free(L)/L)
