#!/usr/bin/env python

import itertools
import numbers
import numpy as np
import picos
import sys

#########################
# Manipulating operators

def product_terms(x1,p1,x2,p2):
    if p1 == 0 or x2 == 0:
        yield ((x1+x2,p1+p2), 1.)
        return
    yield from product_terms(x1+1,p1,x2-1,p2)
    for ((x,p),c) in product_terms(x1,p1-1,x2-1,p2):
        yield ((x,p),-1j*p1*c)

def _term_name(t):
    if len(t) == 0:
        return '1'
    return '*'.join(f'{s}({x})' for (s,x) in t)

def _term_str(t,c):
    if len(t) == 0:
        return f'{c}'
    return f'{c}*' + '*'.join(f'{s}({x})' for (s,x) in t)

def _local_at(l, dx):
    if l == '':
        return ''
    else:
        s,x = l
        return (s,x+dx)

def _to_x_p(op):
    x = op.count('x')
    p = op.count('p')
    assert op == 'x'*x + 'p'*p
    return (x,p)

def _term_mul(t,tp):
    # Collect operators that act on the same site.
    sites, sitesp = dict(), dict()
    for op, x in t:
        if x not in sites:
            sites[x] = ''
        sites[x] = sites[x] + op
    for op, x in tp:
        if x not in sitesp:
            sitesp[x] = ''
        sitesp[x] = sitesp[x] + op

    # Convert into (x,p) format.
    sites = {x: _to_x_p(op) for x,op in sites.items()}
    sitesp = {x: _to_x_p(op) for x,op in sitesp.items()}

    # Get generators for each site.
    rs = list(set(itertools.chain(sites.keys(),sitesp.keys())))
    gens = []
    for r in rs:
        if r in sites:
            x, p = sites[r]
        else:
            x, p = 0, 0
        if r in sitesp:
            xp, pp = sitesp[r]
        else:
            xp, pp = 0, 0
        gens.append(product_terms(x, p, xp, pp))

    # Take product and yield
    for term in itertools.product(*gens):
        ops = set()
        c = 1.
        for (i,((x,p),cp)) in enumerate(term):
            c *= cp
            if x > 0 or p > 0:
                ops.add(('x'*x+'p'*p,rs[i]))
        yield frozenset(ops), c

class Operator:
    def __init__(self, local=''):
        if isinstance(local, numbers.Number):
            self.terms = {frozenset(): local}
        elif local == '':
            self.terms = {frozenset(): 1.}
        else:
            self.terms = {frozenset({(local,0)}): 1.}

    def at(self, dx):
        """ Translate by dx. """
        r = Operator()
        r.terms = {frozenset({_local_at(l, dx) for l in t}): c for (t,c) in self.terms.items()}
        return r

    def dag(self):
        """ Return complex conjugate of self. """
        r = Operator()
        r.terms = {}
        for (t,c) in self.terms.items():
            term = Operator(1)
            # Collect operators that act on the same site
            sites = {}
            for op, x in t:
                if x not in sites:
                    sites[x] = ''
                sites[x] = op + sites[x]
            for site, opstr in sites.items():
                op = Operator()
                for opc in opstr:
                    op = Operator(opc) * op
                op = op.at(site)
                term *= op
            r += c.conjugate() * term
        return r

    def __add__(self, op):
        r = Operator(0)
        r.terms = {}
        for (t,c) in itertools.chain(self.terms.items(), op.terms.items()):
            if t in r.terms:
                r.terms[t] += c
            else:
                r.terms[t] = c
        return r

    def __sub__(self, op):
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __mul__(self, op):
        r = Operator(0)
        r.terms = {}
        for (t,c) in self.terms.items():
            for (tp,cp) in op.terms.items():
                for tprod, cprod in _term_mul(t,tp):
                    cprod *= c*cp
                    if tprod in r.terms:
                        r.terms[tprod] += cprod
                    else:
                        r.terms[tprod] = cprod
        return r

    def __truediv__(self, x):
        assert isinstance(x, numbers.Number)
        r = Operator()
        r.terms = {t: c/x for (t,c) in self.terms.items()}
        return r

    def __rmul__(self, x):
        r = Operator()
        r.terms = {t: c*x for (t,c) in self.terms.items()}
        return r

    def __str__(self):
        return ' + '.join(f'{_term_str(t,c)}' for (t,c) in self.terms.items())

phi = Operator('x')
pi = Operator('p')


######################
# Hamiltonian density

mass2 = float(sys.argv[1])
coup = float(sys.argv[2])

phi2 = phi*phi
pi2 = pi*pi
phi4 = phi2*phi2

H_free = 2*0.5*(phi-phi.at(1))*(phi-phi.at(1)) + mass2/2.*(phi2+phi2.at(1)) + 1/2.*(pi2+pi2.at(1))

FREE = True

if FREE:
    H = H_free
else:
    H = H_free + coup/4. * (phi4 + phi4.at(1))

#H_free = 0.5*(phi-phi.at(1))*(phi-phi.at(1)) + mass2 * phi*phi / 2. + pi*pi/2.
#H_free = mass2 * phi*phi / 2. + pi*pi/2.  # one site
#H = H_free + coup/4 * phi*phi*phi*phi


############################
# Semi-definite programming

def bound(ops):
    evar_names = set()
    # Expectation variables.
    for i1, o1 in enumerate(ops):
        for i2, o2 in enumerate(ops):
            op = o1.dag()*o2
            for t in op.terms:
                evar_names.add(t)

    evars = {n: picos.ComplexVariable(f'<{_term_name(n)}>') for n in evar_names}

    problem = picos.Problem(verbosity=1)

    # We target the energy
    eH = picos.Constant(0)
    for (t,c) in H.terms.items():
        eH += c*evars[t]
    problem.set_objective('min', eH.real)

    # Positivity bounds
    M = picos.HermitianVariable("M", len(ops))
    for i1, o1 in enumerate(ops):
        for i2, o2 in enumerate(ops):
            op = o1.dag()*o2
            e = picos.Constant(0)
            for (t,c) in op.terms.items():
                e += c*evars[t]
            problem.add_constraint(M[i1,i2] == e)
    problem.add_constraint(M >> 0)

    # Fix normalization
    problem.add_constraint(evars[list(Operator().terms.keys())[0]] == 1.)

    sol = problem.solve(solver = 'mosek', primals=None, duals=None)
    phiterm = list(phi.terms.keys())[0]
    phi2term = list((phi*phi).terms.keys())[0]
    return sol.reported_value, evars[phiterm], evars[phi2term]

# The set of operators that generate the positivity bounds, initially.
if FREE:
    ops = [Operator(), phi, phi.at(1), pi, pi.at(1)]
else:
    ops = [Operator(), phi, phi.at(1), phi*phi, phi*phi.at(1), (phi*phi).at(1), pi, pi.at(1), phi*pi, phi*pi.at(1), phi.at(1)*pi, pi2, pi2.at(1), (phi*pi).at(1)]

base, ephi, ephi2 = bound(ops)
print(base, ephi, ephi2)

