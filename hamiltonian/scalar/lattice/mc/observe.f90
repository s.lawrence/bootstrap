program observe
	use common

	character(len=50) ntstr, nxstr, m2str, lambdastr, dtstr
	integer nt, nx
	real mass2, lambda, dt

    if (command_argument_count() /= 5) then
        stop "arguments: nt nx m2 lambda dt"
    end if

	call get_command_argument(1, ntstr)
	call get_command_argument(2, nxstr)
	call get_command_argument(3, m2str)
	call get_command_argument(4, lambdastr)
    call get_command_argument(5, dtstr)
	read (ntstr,*) nt
	read (nxstr,*) nx
	read (m2str,*) mass2
	read (lambdastr,*) lambda
    read (dtstr,*) dt

	call main
contains

subroutine main
	integer ios, t, x, tp, xp
	real phi(nt,nx)
	real m1, m2, m4, potential, kinetic, virial

	do
		read (*, *, iostat=ios) phi
		if (ios /= 0) exit

		! Moments of the distribution of phi
		m1 = sum(phi) / (nt*nx)
		m2 = sum(phi**2) / (nt*nx)
		m4 = sum(phi**4) / (nt*nx)

		! Spit out the potential energy density.
		potential = mass2/2.*m2 + lambda*m4
		do x = 1,nx
			xp = mod(x, nx)+1
			potential = potential + sum((phi(:,xp)-phi(:,x))**2)/(2*nt*nx)
		end do

		! Calculate the kinetic energy density directly.
		kinetic = 0.5
		do t = 1,nt
			tp = mod(t, nt)+1
			kinetic = kinetic - sum((phi(t,:)-phi(tp,:))**2)/(2*nt*nx)
		end do

		! Calculate the kinetic energy density via the virial theorem.
		virial = mass2/2.*m2 + 2*lambda*m4
		do x = 1,nx
			xp = mod(x, nx)+1
			virial = virial + sum((phi(:,xp)-phi(:,x))**2)/(2*nt*nx)
		end do

		print *, m1, m2, m4, potential+virial
	end do
end subroutine

end program
