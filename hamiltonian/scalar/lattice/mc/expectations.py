#!/usr/bin/env python3

from numpy import *
import sys

K = int(sys.argv[1])

def bootstrap(x, N=1000, K=1):
    xs = []
    for i in range(int(len(x)/K)):
        xs.append(mean(x[i*K:i*K+K]))
    bs = []
    for i in range(N):
        bs.append(mean(random.choice(xs, int(len(xs)))))
    return mean(xs), std(bs)


M = 6

dat = array([[float(x) for x in l.split()[:M]] for l in sys.stdin.readlines()])

if False:
    for m in range(M):
        val, err = bootstrap(dat[:,m], K=K)
        sys.stdout.write(f' {val} {err}')
    sys.stdout.write("\n")

#print(*bootstrap(dat[:,0], K=K), *bootstrap(dat[:,1], K=K), *bootstrap(dat[:,2], K=K))

if True:
    for n in range(4):
        d = dat[:,n]
        print(*bootstrap(d, K=K))

