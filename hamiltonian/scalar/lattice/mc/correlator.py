#!/usr/bin/env python3

from numpy import *
import sys

K = int(sys.argv[1])

def bootstrap(x, N=1000, K=1):
    xs = []
    for i in range(int(len(x)/K)):
        xs.append(mean(x[i*K:i*K+K]))
    bs = []
    for i in range(N):
        bs.append(mean(random.choice(xs, int(len(xs)))))
    return mean(xs), std(bs)


dat = array([[float(x) for x in l.split()] for l in sys.stdin.readlines()])

N = dat.shape[1]
for n in range(N):
    d = dat[:,n]
    print(*bootstrap(d, K=K))

