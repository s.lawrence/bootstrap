program metropolis
	use common

	integer, parameter :: ntherm = 100, nskip = 2, nmeas = 10000

	character(len=50) ntstr, nxstr, m2str, lambdastr
	integer nt, nx
	real m2, lambda
	real del
	integer accepted

	call get_command_argument(1, ntstr)
	call get_command_argument(2, nxstr)
	call get_command_argument(3, m2str)
	call get_command_argument(4, lambdastr)
	read (ntstr,*) nt
	read (nxstr,*) nx
	read (m2str,*) m2
	read (lambdastr,*) lambda

	del = 0.5
	accepted = 0


	call main
contains

subroutine main
	!use iso_fortran_env, only : error_unit
	integer step
	real phi(nt,nx)
	
	phi = 0
	do step = 1,ntherm*nskip*nt*nx
		call update(phi)
		if (mod(step,nskip*nt*nx) == 0) then
			if (accepted > nskip*nt*nx*0.6) del = del*1.1
			if (accepted < nskip*nt*nx*0.2) del = del*0.9
			!write (error_unit,'(F6.2)') accepted/real(nskip*nt*nx)*100.
			accepted = 0
		end if
	end do
	!write (error_unit,'(F6.2)') del

	do step = 1,nmeas*nskip*nt*nx
		call update(phi)
		if (mod(step,nskip*nt*nx) == 0) then
			print *, phi
		end if
	end do
end subroutine

function action(phi) result(S)
	real, intent(in) :: phi(nt, nx)
	real S

	S = 0.5*m2 * sum(phi*phi) + lambda * sum(phi*phi*phi*phi)
	S = S + 0.5 * sum((phi(:,2:) - phi(:,:nx-1))**2)
	S = S + 0.5 * sum((phi(2:,:) - phi(:nt-1,:))**2)
	S = S + 0.5 * sum((phi(:,1) - phi(:,nx))**2)
	S = S + 0.5 * sum((phi(1,:) - phi(nt,:))**2)
end function 

subroutine update(phi)
	real, intent(inout) :: phi(nt, nx)
	real :: phip(nt, nx)
	real S, Sp, x

	phip = phi
	call propose(phip, del)
	S = action(phi)
	Sp = action(phip)
	call random_number(x)
	if (x < exp(S - Sp)) then
		phi = phip
		accepted = accepted+1
	end if
end subroutine

subroutine propose(phi, del)
	real, intent(inout) :: phi(nt, nx)
	real, intent(in) :: del
	integer t, x
	real r
	do t = 1,nt
		do x = 1,nx
			call random_normal(r, del/sqrt(real(nt*nx)))
			phi(t,x) = phi(t,x) + r
		end do
	end do
end subroutine

end program

