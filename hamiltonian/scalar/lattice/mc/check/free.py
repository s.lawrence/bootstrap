#!/usr/bin/env python

# This script computes the thermodynamics of a free theory on the lattice. It
# is done in the Hamiltonian formalism, so trotterization effects are not
# included. The lattice spacing is assumed to be 1. No zero-point subtraction
# is performed, so the output of this script should correspond to results
# calculated from a simple Monte Carlo.

import sys
from math import *

beta = int(sys.argv[1])
L = int(sys.argv[2])
m = float(sys.argv[3])

T = 1/beta

energy = 0.

# Loop over all momentum modes.
for k in range(L):
    p = 2 * pi / L * k
    omega = sqrt(m**2 + (2*sin(p/2))**2)
    energy += omega /(exp(beta * omega) - 1)
    #energy += omega * (0.5 + 1./(exp(beta * omega) - 1))
    #energy += omega/2 / tanh(beta * omega/2)

print(energy/L)
