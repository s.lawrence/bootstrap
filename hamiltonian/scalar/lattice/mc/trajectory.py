#!/usr/bin/env python3

"""
We take in two arguments: a dimensionless coupling and a renormalized mass in
lattice units. We output the bare mass and coupling in lattice units.
"""

from numpy import *
import sys

m = float(sys.argv[1])
f = float(sys.argv[2])

lamda = f * m**2

def loop(m2):
    ts = linspace(0,20,50000)
    int1 = trapz(exp(-m2*ts) * (exp(-2*ts)*i0(2*ts))**2, ts)
    ts = linspace(20,200,50000)
    int2 = trapz(exp(-m2*ts) * (exp(-2*ts)*i0(2*ts))**2, ts)
    ts = linspace(200,100000,200000)
    int3 = trapz(exp(-m2*ts) * (1. / sqrt(4*pi*ts) + 1. / sqrt(128*pi*(2*ts)**3))**2, ts)
    return int1+int2+int3

print(m**2 -12 * lamda * loop(m**2), lamda)


