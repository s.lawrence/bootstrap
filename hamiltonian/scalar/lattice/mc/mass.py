#!/usr/bin/env python3

import sys
from numpy import *
from scipy.optimize import curve_fit

nt = int(sys.argv[1])
t = list(range(nt))
K = 100

def bootstrap(x, f, N=100):
    bs = []
    for n in range(N):
        idxs = random.choice(range(len(x)), len(x))
        s = array([x[i] for i in idxs])
        bs.append(f(mean(s,axis=0)))
    # Exclude any
    return f(mean(x,axis=0)), std(bs)

def fitf(x, m, a, b):
    return a * cosh(m*(x-b))

bnd=([0,-inf,nt/2-1e-2],[inf,inf,nt/2+1e-2])
def mass(x):
    skip = int(nt/8)
    fit, cov = curve_fit(fitf, t[skip:-skip], x[skip:-skip], bounds=bnd, p0=[0.1,0.1,nt/2])
    return fit[0]

dat = array([[float(x) for x in l.split()] for l in sys.stdin.readlines()[:]])[:,-nt:]

# Form blocks of size K
ds = []
for i in range(int(len(dat)/K)):
    d = []
    for j in range(dat.shape[1]):
        d.append(mean(dat[i*K:i*K+K,j]))
    ds.append(d)

dat = ds

#print(*bootstrap(dat, mass, N=50))
print(*bootstrap(dat, mass))

t = range(nt)
corr = mean(dat,axis=0)

if False:
    from matplotlib.pyplot import *
    scatter(t, corr)
    show()


if False:

    dat = array([[float(x) for x in l.split()] for l in sys.stdin.readlines()])[-nt:,:]
    print(dat.shape)

    dt = 1
    t = range(nt)[::dt]

    corr = dat[::dt,0]
    errs = dat[::dt,1]

    emass = log(corr[:-1] / corr[1:])
    eerrs = sqrt((errs[:-1]/corr[:-1])**2 + (errs[1:]/corr[1:])**2)

    from matplotlib.pyplot import *
    if False:
        figure()
        errorbar(t[:-1], emass/dt, yerr=eerrs/dt, fmt='none')

    if False:
        figure()
        errorbar(t, log(corr), yerr=errs/corr, fmt='none')
        show()

    if True:
        from scipy.optimize import curve_fit

        def f(x, m, a, b):
            return a * cosh(m*(x-b))

        t = t[6:-6]
        corr = corr[6:-6]
        errs = errs[6:-6]

        fit, cov = curve_fit(f, t, corr, sigma=errs, absolute_sigma=True)
        print(fit)
        print(cov)


        from matplotlib.pyplot import *
        errorbar(t, corr, yerr=errs, fmt='none')
        plot(t, f(t, *fit))
        show()

    show()
