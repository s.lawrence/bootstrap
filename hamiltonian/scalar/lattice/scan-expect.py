#!/bin/sh

set -euo pipefail

for lambda in `seq 0.0 0.005 0.1`; do
    fn=mc/data/cluster,1500,30,0.04,$lambda,0.02
    dat=`mc/expectations.py 20 < $fn | tr '\n' ' '`
    echo "$lambda $dat"
done
