#!/bin/sh

#nt=2500
#nx=50
nt=1500
nx=30
dt=0.02

for lambda in `seq 0.0 0.005 0.1`; do
    fn=mc/data/cluster,$nt,$nx,0.04,$lambda,$dt
    echo "mc/cluster $nt $nx 0.04 $lambda $dt 2000 | mc/observe $nt $nx 0.04 $lambda $dt > $fn"
done
