import numpy as np
import picos


# Perturbative

m2 = 0.04

def E_0(L):
    n = np.arange(L)
    k = 2*np.pi * n / L
    E = np.sqrt(m2 + (2*np.sin(k/2))**2)/2
    return np.sum(E)

def E_1(L):
    return 0

def E_perturbative(L, lamda):
    return E_0(L) + lamda * E_1(L)

import matplotlib
import matplotlib.pyplot as plt

matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

# Read data
with open('bounds.dat') as f:
    bounds = np.array([[float(x) for x in l.split()] for l in f.readlines()])

with open('expects.dat') as f:
    expects = np.array([[float(x) for x in l.split()] for l in f.readlines()])

L1N2 = np.logical_and(bounds[:,2] == 1, bounds[:,3] == 2)
L2N3 = np.logical_and(bounds[:,2] == 2, bounds[:,3] == 3)
L3N4 = np.logical_and(bounds[:,2] == 3, bounds[:,3] == 4)
L4N5 = np.logical_and(bounds[:,2] == 4, bounds[:,3] == 5)
L5N6 = np.logical_and(bounds[:,2] == 5, bounds[:,3] == 6)
L6N7 = np.logical_and(bounds[:,2] == 6, bounds[:,3] == 7)
L7N8 = np.logical_and(bounds[:,2] == 7, bounds[:,3] == 8)

lamdas = np.linspace(0,0.1,30)

plt.figure(figsize=(5,4))
plt.errorbar(expects[:,0], expects[:,7], yerr=expects[:,8], fmt='.', label='Monte Carlo')
plt.plot(bounds[L1N2,1], bounds[L1N2,4], label='$L=1$, $N=2$', ls='-.')
#plt.plot(bounds[L2N3,1], bounds[L2N3,4], label='$L=2$, $N=3$')
plt.plot(bounds[L3N4,1], bounds[L3N4,4], label='$L=3$, $N=4$', ls='-.')
#plt.plot(bounds[L4N5,1], bounds[L4N5,4], label='$L=4$, $N=5$')
plt.plot(bounds[L5N6,1], bounds[L5N6,4], label='$L=5$, $N=6$', ls='--')
#plt.plot(bounds[L6N7,1], bounds[L6N7,4], label='$L=6$, $N=7$')
plt.plot(bounds[L7N8,1], bounds[L7N8,4], label='$L=7$, $N=8$', ls='-')
#plt.ylim(bottom=0.56)
plt.ylabel('$\epsilon_0$')
plt.xlabel('$\lambda$')
plt.legend(loc='lower right')
plt.tight_layout()
plt.savefig('scalar-energy.png', dpi=300)
