import numpy as np
import picos
import sys

L = 8
Jx = 1/3.
Jy = 1/2.
Jz = 1.


#########
# EXACT #
#########

def gse(mu):

    pauli_i = np.array([[1,0],[0,1]],dtype=np.complex128)
    pauli_x = np.array([[0,1],[1,0]],dtype=np.complex128)
    pauli_y = np.array([[0,-1j],[1j,0]],dtype=np.complex128)
    pauli_z = np.array([[1,0],[0,-1]],dtype=np.complex128)

    sigma_x = [np.ones((1,1), dtype=np.complex128) for _ in range(L)]
    sigma_y = [np.ones((1,1), dtype=np.complex128) for _ in range(L)]
    sigma_z = [np.ones((1,1), dtype=np.complex128) for _ in range(L)]

    for x in range(L):
        for y in range(L):
            if x == y:
                sigma_x[x] = np.kron(sigma_x[x], pauli_x)
                sigma_y[x] = np.kron(sigma_y[x], pauli_y)
                sigma_z[x] = np.kron(sigma_z[x], pauli_z)
            else:
                sigma_x[x] = np.kron(sigma_x[x], pauli_i)
                sigma_y[x] = np.kron(sigma_y[x], pauli_i)
                sigma_z[x] = np.kron(sigma_z[x], pauli_i)

    D = 2**L
    H = np.zeros((D,D), dtype=np.complex128)
    for x in range(L):
        xp = (x+1)%L
        H -= mu*sigma_x[x]
        if L == 1:
            continue
        if L == 2 and xp == 0:
            continue
        H -= Jx*sigma_x[x]@sigma_x[xp]
        H -= Jy*sigma_y[x]@sigma_y[xp]
        H -= Jz*sigma_z[x]@sigma_z[xp]

    vals, vecs = np.linalg.eigh(H)
    return vals[0]


#######
# SDP #
#######

#########################
# Manipulating operators

def pauli_mul_1(a, b):
    if a == 'I':
        return b, 1.+0j
    if b == 'I':
        return a, 1.+0j
    if a == b:
        return 'I', 1.+0j
    if a == 'X' and b == 'Y':
        return 'Z', 1j
    if a == 'Y' and b == 'Z':
        return 'X', 1j
    if a == 'Z' and b == 'X':
        return 'Y', 1j
    r, c = pauli_mul(b, a)
    return r, -c

def pauli_mul(a, b):
    assert len(a) == len(b)
    c = 1.
    r = []
    for i in range(len(a)):
        s, d = pauli_mul_1(a[i], b[i])
        r.append(s)
        c *= d
    return ''.join(r), c

class Operator:
    def __init__(self, pauli=None, N=None):
        assert pauli is None or N is None
        assert pauli is not None or N is not None
        if N is not None:
            self.N = N
            self.terms = dict()
        if pauli is not None:
            self.N = len(pauli)
            self.terms = {pauli: 1.+0j}

    def __iadd__(self, op):
        assert self.N == op.N
        for s, c in op.terms.items():
            if s not in self.terms:
                self.terms[s] = 0.j
            self.terms[s] += c
        return self

    def __sub__(self, op):
        assert self.N == op.N
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __add__(self, op):
        assert self.N == op.N
        r = Operator(N=self.N)
        r += self
        r += op
        return r

    def __mul__(self, op):
        assert self.N == op.N
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            for sp, cp in op.terms.items():
                ns, nc = pauli_mul(s,sp)
                nc *= c*cp
                r.terms[ns] = nc
        return r

    def __rmul__(self, x):
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            r.terms[s] = x*c
        return r

    def dag(self):
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            r.terms[s] = c.conjugate()
        return r

    def __str__(self):
        return ' + '.join([f'{self.terms[t]}*{str(t)}' for t in self.terms])



# The set of operators that generate the positivity bounds.
def rot(l, x):
    return l[-x:] + l[:-x]

ops1 = ['I'*L]
ops1 += [rot('X' + 'I'*(L-1),x) for x in range(L)]
ops1 += [rot('XX' + 'I'*(L-2),x) for x in range(L)]
#ops1 += [rot('YY' + 'I'*(L-2),x) for x in range(L)]
ops1 += [rot('ZZ' + 'I'*(L-2),x) for x in range(L)]
ops1 = list(set(ops1))


ops2 = ops1.copy()
ops2 += [rot('YY' + 'I'*(L-2),x) for x in range(L)]
if False:
    ops2 += [rot('Y' + 'I'*(L-1),x) for x in range(L)]
    ops2 += [rot('Z' + 'I'*(L-1),x) for x in range(L)]
    ops2 += [rot('XY' + 'I'*(L-2),x) for x in range(L)]
    ops2 += [rot('YX' + 'I'*(L-2),x) for x in range(L)]
    ops2 += [rot('YZ' + 'I'*(L-2),x) for x in range(L)]
    ops2 += [rot('ZY' + 'I'*(L-2),x) for x in range(L)]
    ops2 += [rot('ZX' + 'I'*(L-2),x) for x in range(L)]
    ops2 += [rot('XZ' + 'I'*(L-2),x) for x in range(L)]
    #ops2 += [rot('XIX'+'I'*(L-3),x) for x in range(L)]
    #ops2 += [rot('YIY'+'I'*(L-3),x) for x in range(L)]
    #ops2 += [rot('ZIZ'+'I'*(L-3),x) for x in range(L)]
ops2 = list(set(ops2))


def bound(mu, ops):

    ##############
    # Hamiltonian

    H = 0.*Operator('I'*L)
    for x in range(L):
        H -= mu*Operator('I'*x + 'X' + 'I'*(L-x-1))
    for x in range(L-1):
        H -= Jx * Operator('I'*x + 'XX' + 'I'*(L-x-2))
        H -= Jy * Operator('I'*x + 'YY' + 'I'*(L-x-2))
        H -= Jz * Operator('I'*x + 'ZZ' + 'I'*(L-x-2))
    if L > 2:
        H -= Jx * Operator('X'+'I'*(L-2)+'X')
        H -= Jy * Operator('Y'+'I'*(L-2)+'Y')
        H -= Jz * Operator('Z'+'I'*(L-2)+'Z')


    ############################
    # Semi-definite programming

    # Now we can list all the operators we need.
    evar_names = ['I'*L]
    for t1 in ops:
        for t2 in ops:
            i1 = ops.index(t1)
            i2 = ops.index(t2)
            op = Operator(t1).dag() * Operator(t2)
            for (s,c) in op.terms.items():
                if s not in evar_names:
                    evar_names.append(s)

    evars = {n: picos.ComplexVariable(f'<{n}>') for n in evar_names}
    idx = {evar_names[i]: i for i in range(len(evar_names))}

    problem = picos.Problem(verbosity=0)

    # We target the energy
    eH = 0
    for (s,c) in H.terms.items():
        eH += c*evars[s]
    problem.set_objective('min', eH.real)

    # Positivity bounds
    M = picos.HermitianVariable("M", len(ops))
    for t1 in ops:
        for t2 in ops:
            i1 = ops.index(t1)
            i2 = ops.index(t2)
            op = Operator(t1).dag() * Operator(t2)
            e = picos.Constant(0)
            for (s,c) in op.terms.items():
                e += c*evars[s]
            problem.add_constraint(M[i1,i2] == e)
    problem.add_constraint(M >> 0)

    # Fix normalization
    problem.add_constraint(evars['I'*L] == 1.)

    sol = problem.solve(solver = 'mosek', primals=None, duals=None)
    return sol.reported_value



import matplotlib
import matplotlib.pyplot as plt

matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1


#mus = np.linspace(0,1.4,21)
mus = np.linspace(0,1.4,11)
#mus = np.linspace(0.25,0.4,21)

plt.figure(figsize=(6,4))
plt.plot(mus, np.vectorize(gse)(mus), label='Exact', c='black')
plt.plot(mus, np.vectorize(lambda m: bound(m, ops1))(mus), label='SDP1', ls=':', c='blue')
plt.plot(mus, np.vectorize(lambda m: bound(m, ops2))(mus), label='SDP2', ls='--', c='red')
plt.ylabel('$E_0$')
plt.xlabel('$\mu$')
#plt.ylim(top=0)
plt.legend(loc='best')
plt.tight_layout()
plt.savefig('spin.png', dpi=300)
