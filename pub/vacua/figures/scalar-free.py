import itertools
import numbers
import numpy as np
import picos

#########################
# Manipulating operators

def product_terms(x1,p1,x2,p2):
    if p1 == 0 or x2 == 0:
        yield ((x1+x2,p1+p2), 1.)
        return
    yield from product_terms(x1+1,p1,x2-1,p2)
    for ((x,p),c) in product_terms(x1,p1-1,x2-1,p2):
        yield ((x,p),-1j*p1*c)

def _term_name(t):
    if len(t) == 0:
        return '1'
    return '*'.join(f'{s}({x})' for (s,x) in t)

def _term_str(t,c):
    if len(t) == 0:
        return f'{c}'
    return f'{c}*' + '*'.join(f'{s}({x})' for (s,x) in t)

def _local_at(l, dx):
    if l == '':
        return ''
    else:
        s,x = l
        return (s,x+dx)

def _to_x_p(op):
    x = op.count('x')
    p = op.count('p')
    assert op == 'x'*x + 'p'*p
    return (x,p)

def _term_mul(t,tp):
    # Collect operators that act on the same site.
    sites, sitesp = dict(), dict()
    for op, x in t:
        if x not in sites:
            sites[x] = ''
        sites[x] = sites[x] + op
    for op, x in tp:
        if x not in sitesp:
            sitesp[x] = ''
        sitesp[x] = sitesp[x] + op

    # Convert into (x,p) format.
    sites = {x: _to_x_p(op) for x,op in sites.items()}
    sitesp = {x: _to_x_p(op) for x,op in sitesp.items()}

    # Get generators for each site.
    rs = list(set(itertools.chain(sites.keys(),sitesp.keys())))
    gens = []
    for r in rs:
        if r in sites:
            x, p = sites[r]
        else:
            x, p = 0, 0
        if r in sitesp:
            xp, pp = sitesp[r]
        else:
            xp, pp = 0, 0
        gens.append(product_terms(x, p, xp, pp))

    # Take product and yield
    for term in itertools.product(*gens):
        ops = set()
        c = 1.
        for (i,((x,p),cp)) in enumerate(term):
            c *= cp
            if x > 0 or p > 0:
                ops.add(('x'*x+'p'*p,rs[i]))
        yield frozenset(ops), c

class Operator:
    def __init__(self, local=''):
        if isinstance(local, numbers.Number):
            self.terms = {frozenset(): local}
        elif local == '':
            self.terms = {frozenset(): 1.}
        else:
            self.terms = {frozenset({(local,0)}): 1.}

    def at(self, dx):
        """ Translate by dx. """
        r = Operator()
        r.terms = {frozenset({_local_at(l, dx) for l in t}): c for (t,c) in self.terms.items()}
        return r

    def dag(self):
        """ Return complex conjugate of self. """
        r = Operator()
        r.terms = {}
        for (t,c) in self.terms.items():
            term = Operator(1)
            # Collect operators that act on the same site
            sites = {}
            for op, x in t:
                if x not in sites:
                    sites[x] = ''
                sites[x] = op + sites[x]
            for site, opstr in sites.items():
                op = Operator()
                for opc in opstr:
                    op = Operator(opc) * op
                op = op.at(site)
                term *= op
            r += c.conjugate() * term
        return r

    def __add__(self, op):
        r = Operator(0)
        r.terms = {}
        for (t,c) in itertools.chain(self.terms.items(), op.terms.items()):
            if t in r.terms:
                r.terms[t] += c
            else:
                r.terms[t] = c
        return r

    def __sub__(self, op):
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __mul__(self, op):
        r = Operator(0)
        r.terms = {}
        for (t,c) in self.terms.items():
            for (tp,cp) in op.terms.items():
                for tprod, cprod in _term_mul(t,tp):
                    cprod *= c*cp
                    if tprod in r.terms:
                        r.terms[tprod] += cprod
                    else:
                        r.terms[tprod] = cprod
        return r

    def __truediv__(self, x):
        assert isinstance(x, numbers.Number)
        r = Operator()
        r.terms = {t: c/x for (t,c) in self.terms.items()}
        return r

    def __rmul__(self, x):
        r = Operator()
        r.terms = {t: c*x for (t,c) in self.terms.items()}
        return r

    def __str__(self):
        return ' + '.join(f'{_term_str(t,c)}' for (t,c) in self.terms.items())

phi = Operator('x')
pi = Operator('p')


######################
# Hamiltonian density

mass2 = 0.1**2

H_free = 0.5*(phi-phi.at(1))*(phi-phi.at(1)) + mass2 * phi*phi / 2. + pi*pi/2.
H = H_free

############################
# Semi-definite programming


def bound(L):
    print(L)
    ops = [Operator()]
    ops += [phi.at(x) for x in range(-L,L+1)]
    ops += [pi.at(x) for x in range(-L,L+1)]

    evar_names = set()
    # Expectation variables.
    for i1, o1 in enumerate(ops):
        for i2, o2 in enumerate(ops):
            op = o1.dag()*o2
            for t in op.terms:
                evar_names.add(t)

    evars = {n: picos.ComplexVariable(f'<{_term_name(n)}>') for n in evar_names}

    problem = picos.Problem(verbosity=0)

    # We target the energy
    eH = picos.Constant(0)
    for (t,c) in H.terms.items():
        eH += c*evars[t]
    problem.set_objective('min', eH.real)

    # Positivity bounds
    M = picos.HermitianVariable("M", len(ops))
    for i1, o1 in enumerate(ops):
        for i2, o2 in enumerate(ops):
            op = o1.dag()*o2
            e = picos.Constant(0)
            for (t,c) in op.terms.items():
                e += c*evars[t]
            problem.add_constraint(M[i1,i2] == e)
    problem.add_constraint(M >> 0)

    # Translational invariance
    for t in evars:
        op1 = Operator()
        op1.terms = {t: 1.}
        for x in range(-L,L+1):
            op2 = op1.at(x)
            assert len(op2.terms) == 1
            tp = list(op2.terms.keys())[0]
            try:
                problem.add_constraint(evars[t] == evars[tp])
            except:
                pass

    # Commutators with the Hamiltonian must vanish.
    if False:
        for op in ops:
            com = H*op - op*H
            try:
                ecom = picos.Constant(0)
                for (t,c) in com.terms.items():
                    ecom += c*evars[t]
                print(ecom)
                problem.add_constraint(ecom == 0)
            except KeyError as err:
                print(err)

    # Fix normalization
    problem.add_constraint(evars[list(Operator().terms.keys())[0]] == 1.)

    sol = problem.solve(solver = 'mosek', primals=None, duals=None)
    return sol.reported_value

########
# Exact

def E_free(L):
    n = np.arange(L)
    k = 2*np.pi * n / L
    E = np.sqrt(mass2 + (2*np.sin(k/2))**2)/2
    return np.sum(E)

import matplotlib
import matplotlib.pyplot as plt

matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

L = np.arange(1,10)

plt.figure(figsize=(6,4))
plt.plot(L,0*L+E_free(1000)/1000, c='black', label='Exact')
plt.plot(L,np.vectorize(bound)(L), label='SDP', c='red', ls='', marker='.')
plt.ylabel('$\epsilon_0$')
plt.xlabel('$L$')
plt.ylim(0.6,0.66)
plt.legend(loc='lower right')
plt.tight_layout()
plt.savefig('scalar-free.png', dpi=300)
