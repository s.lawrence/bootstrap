import numpy as np
import picos


###########################
# True ground state energy

def gse(lamda):
    m = 1.
    g = lamda
    N = 500

    a = np.zeros((N,N))
    for i in range(N-1):
        a[i,i+1] = np.sqrt(i+1)*np.sqrt(m)
    adag = a.transpose().conjugate()

    x = 1/(m*np.sqrt(2)) * (a + adag)
    p = 1j*np.sqrt(m/2) * (adag - a)
    H = adag@a + 1/2 * np.eye(N) + g/4*x@x@x@x

    E, U = np.linalg.eigh(H)
    return E[0]


#########################
# Manipulating operators

def product_terms(x1,p1,x2,p2):
    if p1 == 0 or x2 == 0:
        yield ((x1+x2,p1+p2), 1.)
        return
    yield from product_terms(x1+1,p1,x2-1,p2)
    for ((x,p),c) in product_terms(x1,p1-1,x2-1,p2):
        yield ((x,p),-1j*p1*c)

class Operator:
    def __init__(self, x=None):
        if isinstance(x, str):
            if x == 'x':
                self.c = {'x': complex(1)}
            elif x == 'p':
                self.c = {'p': complex(1)}
            else:
                raise "must be x or p"
        elif x is not None:
            self.c = {'': complex(x)}
        else:
            self.c = dict()

    def __add__(self, op):
        r = Operator()
        for term in set.union(set(self.c), set(op.c)):
            r.c[term] = 0.
            if term in self.c:
                r.c[term] += self.c[term]
            if term in op.c:
                r.c[term] += op.c[term]
        return r

    def __sub__(self, op):
        return self+(-op)

    def __neg__(self):
        r = Operator()
        for term in self.c:
            r.c[term] = -self.c[term]
        return r

    def __mul__(self, op):
        r = Operator()
        for t1 in self.c:
            for t2 in op.c:
                x1 = t1.count('x')
                p1 = t1.count('p')
                x2 = t2.count('x')
                p2 = t2.count('p')
                for ((x,p),c) in product_terms(x1,p1,x2,p2):
                    if 'x'*x+'p'*p not in r.c:
                        r.c['x'*x+'p'*p] = 0.
                    r.c['x'*x+'p'*p] += c*self.c[t1]*op.c[t2]
        return r

    def __rmul__(self, x):
        r = Operator()
        for term in self.c:
            r.c[term] = x * self.c[term]
        return r

    def __str__(self):
        return str(self.c)

def operator(desc):
    op = Operator(1.)
    for c in desc:
        op = op*Operator(c)
    return op

def commutator(a, b):
    return a*b - b*a


def bound(lamda, Nx, Np):
    Nx = Nx+1
    Np = Np+1

    ##############
    # Hamiltonian

    H_sho = 0.5*(operator('xx') + operator('pp'))
    H = H_sho + lamda/4*operator('xxxx')

    ##############
    # Constraints

    # PARAMETERS TO MODIFY:
    #Nx = 7
    #Np = 2

    Tx = 2*Nx+1
    Tp = 2*Np+1

    evar_names = ['x'*x+'p'*p for x in range(Tx) for p in range(Tp)]
    evars = {n: picos.ComplexVariable(f'<{n}>') for n in evar_names}
    idx = {evar_names[i]: i for i in range(len(evar_names))}

    problem = picos.Problem()

    # We target the energy
    eH = 0
    for (t,coef) in H.c.items():
        eH += coef*evars[t]
    problem.set_objective('min', eH.real)

    # Positivity bounds
    ops = ['x'*x for x in range(Nx)] + ['p'*p for p in range(Np)]
    M = picos.HermitianVariable("M", len(ops))
    for t1 in ops:
        for t2 in ops:
            i1 = ops.index(t1)
            i2 = ops.index(t2)
            op = operator(t1+t2[::-1])
            e = picos.Constant(0)
            for (t,c) in op.c.items():
                e += c*evars[t]
            problem.add_constraint(M[i1,i2] == e)
    problem.add_constraint(M >> 0)

    # Fix normalization
    problem.add_constraint(evars[''] == 1.)

    if False:
        # Commutators must vanish
        for x in range(Tx):
            for p in range(Tp):
                try:
                    op = operator('x'*x+'p'*p)
                    com = commutator(H,op)
                    ecom = 0
                    for (t,c) in com.c.items():
                        ecom += c*evars[t]
                except KeyError:
                    continue
                problem.add_constraint(ecom == 0)

    sol = problem.solve(solver = 'mosek', primals=None, duals=None)
    #sol = problem.solve(solver = 'mosek', primals=True, duals=False)
    #sol = problem.solve(solver = 'mosek', primals=None, duals=True)
    return sol.reported_value

#print(bound(0))
#print(bound(0.1))


import matplotlib
import matplotlib.pyplot as plt

matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1


lamdas = np.linspace(0,10,20)

plt.figure(figsize=(5,4))
plt.plot(lamdas, np.vectorize(gse)(lamdas), label='Exact', c='black')
plt.plot(lamdas, np.vectorize(lambda l: bound(l, 2, 1))(lamdas), label='$N_x = 2$', ls=':', lw=1.5)
plt.plot(lamdas, np.vectorize(lambda l: bound(l, 6, 1))(lamdas), label='$N_x = 6$', ls='-.', lw=1.5)
plt.plot(lamdas, np.vectorize(lambda l: bound(l, 8, 1))(lamdas), label='$N_x = 8$', ls='--', lw=1.5)
plt.ylabel('$E_0$')
plt.xlabel('$\lambda$')
plt.legend(loc='best')
plt.tight_layout()
plt.savefig('aho-left.png', dpi=300)
