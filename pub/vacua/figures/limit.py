import numpy as np
import pickle
import picos
from scipy.optimize import minimize


#############
# Parameters

Jx = 1/3.
Jy = 1/2.
Jz = 1.

############
# Exact-ish

def gse(mu,L):
    pauli_i = np.array([[1,0],[0,1]],dtype=np.complex128)
    pauli_x = np.array([[0,1],[1,0]],dtype=np.complex128)
    pauli_y = np.array([[0,-1j],[1j,0]],dtype=np.complex128)
    pauli_z = np.array([[1,0],[0,-1]],dtype=np.complex128)

    sigma_x = [np.ones((1,1), dtype=np.complex128) for _ in range(L)]
    sigma_y = [np.ones((1,1), dtype=np.complex128) for _ in range(L)]
    sigma_z = [np.ones((1,1), dtype=np.complex128) for _ in range(L)]

    for x in range(L):
        for y in range(L):
            if x == y:
                sigma_x[x] = np.kron(sigma_x[x], pauli_x)
                sigma_y[x] = np.kron(sigma_y[x], pauli_y)
                sigma_z[x] = np.kron(sigma_z[x], pauli_z)
            else:
                sigma_x[x] = np.kron(sigma_x[x], pauli_i)
                sigma_y[x] = np.kron(sigma_y[x], pauli_i)
                sigma_z[x] = np.kron(sigma_z[x], pauli_i)

    D = 2**L
    H = np.zeros((D,D), dtype=np.complex128)
    for x in range(L):
        xp = (x+1)%L
        H -= mu*sigma_x[x]
        if L == 1:
            continue
        if L == 2 and xp == 0:
            continue
        H -= Jx*sigma_x[x]@sigma_x[xp]
        H -= Jy*sigma_y[x]@sigma_y[xp]
        H -= Jz*sigma_z[x]@sigma_z[xp]

    vals, vecs = np.linalg.eigh(H)
    return vals[0]/L



######
# MPS

def mps_gse(mu,D,tries=1):
    print(f'    running mps_gse({mu},{D})')
    L = 25

    best = 1e100

    for try_ in range(tries):
        ######################
        # Hamiltonian density

        s0 = np.eye(2,dtype=np.complex64)
        sx = np.array([[0,1],[1,0]],dtype=np.complex64)
        sy = np.array([[0,-1j],[1j,0]],dtype=np.complex64)
        sz = np.array([[1,0],[0,-1]],dtype=np.complex64)

        H = -0.5*mu*(np.einsum('ij,kl->ijkl',sx,s0) + np.einsum('ij,kl->ijkl',s0,sx))
        H -= Jx*np.einsum('ij,kl->ijkl',sx,sx)
        H -= Jy*np.einsum('ij,kl->ijkl',sy,sy)
        H -= Jz*np.einsum('ij,kl->ijkl',sz,sz)


        ###################
        # MPS contractions

        def mps_norm(A):
            B = np.einsum('sij,skl->ijkl', A, A.conj())
            C = B
            for n in range(L-1):
                C = np.einsum('ijkl,jmln->imkn', C, B)
            return np.einsum('iijj->', C).real

        def mps_op1(A, M):
            B = np.einsum('sij,skl->ijkl', A, A.conj())
            Bop = np.einsum('sij,st,tkl->ijkl', A, M, A.conj())
            C = Bop
            for n in range(L-1):
                C = np.einsum('ijkl,jmln->imkn', C, B)
            return np.einsum('iijj->', C).real

        def mps_op2(A, M):
            B = np.einsum('sij,skl->ijkl', A, A.conj())
            Bop = np.einsum('dab,ebc,dfeh,fij,hjk->acik', A, A, M, A.conj(), A.conj())
            C = Bop
            for n in range(L-2):
                C = np.einsum('ijkl,jmln->imkn', C, B)
            return np.einsum('iijj->', C).real

        #########
        # Search

        def energy(A_ri):
            A_ri = np.array(A_ri).reshape(2,D,D,2)
            A = A_ri[...,0] + 1j*A_ri[...,1]
            return mps_op2(A,H) / mps_norm(A)

        opt = minimize(energy, np.random.normal(size=(2,D,D,2)))
        A_ri = opt.x.reshape(2,D,D,2)
        A = A_ri[...,0] + 1j*A_ri[...,1]
        Edens = mps_op2(A,H) / mps_norm(A)
        if Edens < best:
            best = Edens
    return best


#########################
# Manipulating operators

def pauli_mul_1(a, b):
    if a == 'I':
        return b, 1.+0j
    if b == 'I':
        return a, 1.+0j
    if a == b:
        return 'I', 1.+0j
    if a == 'X' and b == 'Y':
        return 'Z', 1j
    if a == 'Y' and b == 'Z':
        return 'X', 1j
    if a == 'Z' and b == 'X':
        return 'Y', 1j
    r, c = pauli_mul(b, a)
    return r, -c

def pauli_mul(a, b):
    assert len(a) == len(b)
    c = 1.
    r = []
    for i in range(len(a)):
        s, d = pauli_mul_1(a[i], b[i])
        r.append(s)
        c *= d
    return ''.join(r), c

class Operator:
    def __init__(self, pauli=None, N=None):
        assert pauli is None or N is None
        assert pauli is not None or N is not None
        if N is not None:
            self.N = N
            self.terms = dict()
        if pauli is not None:
            self.N = len(pauli)
            self.terms = {pauli: 1.+0j}

    def __iadd__(self, op):
        assert self.N == op.N
        for s, c in op.terms.items():
            if s not in self.terms:
                self.terms[s] = 0.j
            self.terms[s] += c
        return self

    def __sub__(self, op):
        assert self.N == op.N
        return self+(-op)

    def __neg__(self):
        return -1*self

    def __add__(self, op):
        assert self.N == op.N
        r = Operator(N=self.N)
        r += self
        r += op
        return r

    def __mul__(self, op):
        assert self.N == op.N
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            for sp, cp in op.terms.items():
                ns, nc = pauli_mul(s,sp)
                nc *= c*cp
                r.terms[ns] = nc
        return r

    def __rmul__(self, x):
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            r.terms[s] = x*c
        return r

    def dag(self):
        r = Operator(N=self.N)
        for s, c in self.terms.items():
            r.terms[s] = c.conjugate()
        return r

    def __str__(self):
        return ' + '.join([f'{self.terms[t]}*{str(t)}' for t in self.terms])


# Size of operators to consider.
pad = 1
L = 2*pad+2
padI = 'I'*pad

# Starting set of operators.
ops1 = ['I'*L, padI+'XI'+padI, padI+'IX'+padI]
ops1 += [padI+'XX'+padI, padI+'ZZ'+padI]

#ops1 += [padI+'XX'+padI, padI+'YY'+padI, padI+'ZZ'+padI]
ops2 = ops1 + [padI+'YY'+padI]
ops3 = ops2 + ['ZIZI']
#ops2 = ['IIII', 'IXII', 'IIXI', 'IZZI', 'IYYI', 'ZIZI']
#ops2 = ['IIII', 'IXII', 'IIXI', 'IXXI', 'IZZI', 'IYYI', 'ZIZI']
#ops2 = ['IIII', 'IYZI', 'IXII', 'IIXI', 'IXXI', 'IZZI', 'IYYI', 'ZZII', 'XXII', 'YYII', 'XYII']

# Possible operators to add
candidates = []
candidates += [padI+a+b+padI for a in ['X','Y','Z'] for b in ['X','Y','Z']]

def bound(mu,ops):
    ##############
    # Hamiltonian
    H = 0.*Operator('I'*L)
    H -= mu/2*(Operator(padI+'XI'+padI) + Operator(padI+'IX'+padI))
    #H -= mu*(Operator(padI+'XI'+padI))
    H -= Jx*Operator(padI+'XX'+padI)
    H -= Jy*Operator(padI+'YY'+padI)
    H -= Jz*Operator(padI+'ZZ'+padI)

    ############################
    # Semi-definite programming

    # Now we can list all the operators we need.
    evar_names = ['I'*L]
    for t1 in ops:
        for t2 in ops:
            i1 = ops.index(t1)
            i2 = ops.index(t2)
            op = Operator(t1).dag() * Operator(t2)
            for (s,c) in op.terms.items():
                if s not in evar_names:
                    evar_names.append(s)

    evars = {n: picos.ComplexVariable(f'<{n}>') for n in evar_names}
    idx = {evar_names[i]: i for i in range(len(evar_names))}

    problem = picos.Problem(verbosity=0)

    # We target the energy
    eH = 0
    for (s,c) in H.terms.items():
        eH += c*evars[s]
    problem.set_objective('min', eH.real)

    # Positivity bounds
    M = picos.HermitianVariable("M", len(ops))
    for t1 in ops:
        for t2 in ops:
            i1 = ops.index(t1)
            i2 = ops.index(t2)
            op = Operator(t1).dag() * Operator(t2)
            e = picos.Constant(0)
            for (s,c) in op.terms.items():
                e += c*evars[s]
            problem.add_constraint(M[i1,i2] == e)
    problem.add_constraint(M >> 0)

    # Translational invariance.
    def rot(l, x):
        return l[-x:] + l[:-x]

    # TODO this is only correct by coincidence
    for op in ops:
        for x in range(-4,4):
            opp = rot(op, x)
            try:
                problem.add_constraint(evars[op] == evars[opp])
            except:
                pass


    # Fix normalization
    problem.add_constraint(evars['I'*L] == 1.)

    sol = problem.solve(solver = 'mosek', primals=None, duals=None)
    return sol.reported_value


import matplotlib
import matplotlib.pyplot as plt

matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

mus = np.linspace(0,1.4,15)

try:
    with open('limit-mps.pickle', 'rb') as f:
        mus_mps, gse_D_1, gse_D_2 = pickle.load(f)
except:
    mus_mps = np.linspace(0,1.4,51)
    gse_D_1 = np.vectorize(lambda m: mps_gse(m,1,tries=50))(mus_mps)
    gse_D_2 = np.vectorize(lambda m: mps_gse(m,2,tries=2))(mus_mps)
    with open('limit-mps.pickle', 'wb') as f:
        pickle.dump((mus_mps, gse_D_1, gse_D_2), f)

plt.figure(figsize=(6,4))
plt.plot(mus_mps, gse_D_1, label='MPS ($D=1$)', c='black')
plt.plot(mus_mps, gse_D_2, label='MPS ($D=2$)', c='blue')
#plt.plot(mus, np.vectorize(lambda m: mps_gse(m,3))(mus), label='MPS ($D=3$)')
#plt.plot(mus, np.vectorize(lambda m: gse(m,8))(mus), label='Exact ($L=8$)')
#plt.plot(mus, np.vectorize(lambda m: gse(m,9))(mus), label='Exact ($L=9$)')
plt.plot(mus, np.vectorize(lambda m: bound(m,ops1))(mus), label='$N = 4$', ls='--', c='black')
plt.plot(mus, np.vectorize(lambda m: bound(m,ops2))(mus), label='$N = 5$', ls='--', c='blue')
plt.plot(mus, np.vectorize(lambda m: bound(m,ops3))(mus), label='$N = 6$', ls='--', c='red')
plt.ylabel('$\epsilon_0$')
plt.xlabel('$\mu$')
plt.legend(loc='best')
plt.tight_layout()
plt.savefig('limit.png', dpi=300)
