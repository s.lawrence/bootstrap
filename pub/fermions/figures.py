#!/usr/bin/env python

import sys

import numpy as np

import matplotlib
from matplotlib import colors
import matplotlib.pyplot as plt

matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

def read_data(fn, dtype=np.float64):
    with open(fn) as f:
        dat = np.array([[dtype(x) for x in l.split()] for l in f.readlines()])
    return dat

def making(fnum):
    return str(fnum) in sys.argv or len(sys.argv)==1

if making(1):
    print("Making Figure 1")
    dat = read_data('data/masses.dat')
    plt.figure(figsize=(4.8,3.2))
    plt.plot(dat[:,0], dat[:,1])
    plt.plot(dat[:,0], dat[:,0]*0+1., ls=':', c='black')
    plt.ylabel('$\\frac{m_B}{m_F}$')
    plt.xlabel('$g^2$')
    plt.tight_layout()
    plt.savefig('figure1.png')

if making(2):
    print("Making Figure 2")
    dat = read_data('data/check.dat')

    plt.figure(figsize=(4.8,3.2), dpi=400)
    plt.xlabel('$\\mu$')
    plt.ylabel('$E + \\mu \\frac{L}{2}$')
    plt.plot(dat[:,0], dat[:,1]+dat[:,0]*5, label='Exact', c='black')
    plt.plot(dat[:,0], dat[:,3]+dat[:,0]*5, label='H0', c='darkgreen', ls=':')
    plt.plot(dat[:,0], dat[:,5]+dat[:,0]*5, label='H1', c='blue', ls='-.')
    plt.plot(dat[:,0], dat[:,7]+dat[:,0]*5, label='H2', c='darkred', ls='--')
    plt.plot(dat[:,0], dat[:,9]+dat[:,0]*5, label='C1', c='darkgoldenrod', ls='-')
    #plt.ylim([0,1.])
    plt.tight_layout()
    plt.legend(loc='best', fontsize='12')
    plt.savefig('figure2-left.png')

    plt.figure(figsize=(4.8,3.2), dpi=400)
    plt.xlabel('$\\mu$')
    plt.ylabel('$N$')
    plt.plot(dat[:,0], dat[:,2], label='Exact')
    plt.plot(dat[:,0], dat[:,4], label='H0', c='darkgreen', ls=':')
    plt.plot(dat[:,0], dat[:,6], label='H1', c='blue', ls='-.')
    plt.plot(dat[:,0], dat[:,8], label='H2', c='darkred', ls='--')
    plt.plot(dat[:,0], dat[:,10], label='C1', c='darkgoldenrod', ls='-')
    plt.ylim([0,6.])
    plt.legend(loc='best', fontsize='12')
    plt.tight_layout()
    plt.savefig('figure2-right.png')

if making(3):
    print("Making Figure 3")

    dat = read_data('data/convg.dat')
    plt.figure(figsize=(4.8,3.2))
    plt.xlabel('$g^2$')
    plt.ylabel('$E_{\\mathrm{exact}} - E$')
    idx = dat[:,0]==0.5
    plt.plot(dat[idx,1], dat[idx,2]-dat[idx,4], label='H0', c='darkgreen', ls=':')
    plt.plot(dat[idx,1], dat[idx,2]-dat[idx,6], label='H1', c='blue', ls='-.')
    plt.plot(dat[idx,1], dat[idx,2]-dat[idx,8], label='H2', c='darkred', ls='--')
    plt.plot(dat[idx,1], dat[idx,2]-dat[idx,10], label='C1', c='darkgoldenrod', ls='-')
    plt.tight_layout()
    plt.legend(loc='best', fontsize='12')
    plt.savefig('figure3-left.png')

    dat = read_data('data/convmu.dat')
    plt.figure(figsize=(4.8,3.2))
    plt.xlabel('$\\mu$')
    plt.ylabel('$E_{\\mathrm{exact}} - E$')
    idx = dat[:,1]==0.1
    plt.plot(dat[idx,0], dat[idx,2]-dat[idx,4], label='H0', c='darkgreen', ls=':')
    plt.plot(dat[idx,0], dat[idx,2]-dat[idx,6], label='H1', c='blue', ls='-.')
    plt.plot(dat[idx,0], dat[idx,2]-dat[idx,8], label='H2', c='darkred', ls='--')
    plt.plot(dat[idx,0], dat[idx,2]-dat[idx,10], label='C1', c='darkgoldenrod', ls='-')
    plt.tight_layout()
    plt.legend(loc='best', fontsize='12')
    plt.savefig('figure3-right.png')

if making(4):
    print("Making Figure 4")
    fig = plt.figure(figsize=(5.1,5.7))
    axtop = plt.subplot(211)
    axbot = plt.subplot(212, sharex=axtop)
    axbot.set_xlabel('$L$')
    axtop.set_ylabel('$n$')
    axtop.tick_params(labelbottom=False)
    axbot.set_ylabel('$\\frac{|n_{\\mathrm{SDP}} - n_{\\mathrm{exact}}|}{n_{\\mathrm{exact}}}$')
    dats = {}
    dats[0.1] = read_data('data/free-0.1.dat')
    dats[0.3] = read_data('data/free-0.3.dat')
    dats[1.0] = read_data('data/free-1.0.dat')
    colors = {0.1: 'red', 0.3: 'green', 1.0: 'blue'}
    markers = {0.1: 's', 0.3: 'o', 1.0: '^'}
    for m in dats:
        dat = dats[m]
        axtop.plot(dat[:,0], dat[:,2], label=f'Exact, $m={m}$', c=colors[m])
    for m in dats:
        dat = dats[m]
        axtop.scatter(dat[:,0], dat[:,4], label=f'SDP, $m={m}$', color=colors[m], marker=markers[m])
        ex, sdp = dat[:,2], dat[:,4]
        axbot.scatter(dat[:,0], abs(sdp-ex)/ex, label=f'SDP, $m={m}$', color=colors[m], marker=markers[m])
    axtop.set_ylim([-0.05,1.2])
    axbot.set_ylim([0.003,5.05])
    axbot.set_yscale('log')
    fig.tight_layout()
    axtop.legend(loc='best', ncol=2, fontsize=11)
    fig.subplots_adjust(hspace=0)
    fig.savefig('figure4.png')

if making(5):
    print("Making Figure 5")

    dat = read_data('data/inf-0.1.dat')
    plt.figure(figsize=(4.8,3.2))
    plt.xlabel('$\\mu$')
    plt.ylabel('$n$')
    #plt.plot(dat[:,0], dat[:,3], label='$L=2$, H0', c='darkgreen', ls=':')
    #plt.plot(dat[:,0], dat[:,5], label='$L=2$, H1', c='blue', ls='-.')
    #plt.plot(dat[:,0], dat[:,7], label='$L=2$, H2', c='darkred', ls='--')
    plt.plot(dat[:,0], dat[:,9], label='$L=2$, C1', c='darkgoldenrod', ls=':')
    #plt.plot(dat[:,0], dat[:,11], label='$L=4$, H0', c='darkgreen', ls='-')
    #plt.plot(dat[:,0], dat[:,13], label='$L=4$, H1', c='blue', ls='-')
    #plt.plot(dat[:,0], dat[:,15], label='$L=4$, H2', c='darkred', ls='-')
    plt.plot(dat[:,0], dat[:,17], label='$L=4$, C1', c='darkgoldenrod', ls='-.')
    plt.plot(dat[:,0], dat[:,19], label='$L=6$, H0', c='darkgreen', ls='-')
    plt.plot(dat[:,0], dat[:,21], label='$L=6$, H1', c='blue', ls='-')
    plt.plot(dat[:,0], dat[:,23], label='$L=6$, H2', c='darkred', ls='-')
    plt.plot(dat[:,0], dat[:,25], label='$L=6$, C1', c='darkgoldenrod', ls='-')
    plt.legend(loc='best', fontsize='12')
    plt.ylim([0,1.2])
    plt.tight_layout()
    plt.savefig('figure5-left.png')

    dat = read_data('data/inf-0.5.dat')
    plt.figure(figsize=(4.8,3.2))
    plt.xlabel('$\\mu$')
    plt.ylabel('n')
    #plt.plot(dat[:,0], dat[:,3], label='$L=2$, H0', c='darkgreen', ls=':')
    #plt.plot(dat[:,0], dat[:,5], label='$L=2$, H1', c='blue', ls='-.')
    #plt.plot(dat[:,0], dat[:,7], label='$L=2$, H2', c='darkred', ls='--')
    plt.plot(dat[:,0], dat[:,9], label='$L=2$, C1', c='darkgoldenrod', ls=':')
    #plt.plot(dat[:,0], dat[:,11], label='$L=4$, H0', c='darkgreen', ls='-')
    #plt.plot(dat[:,0], dat[:,13], label='$L=4$, H1', c='blue', ls='-')
    #plt.plot(dat[:,0], dat[:,15], label='$L=4$, H2', c='darkred', ls='-')
    plt.plot(dat[:,0], dat[:,17], label='$L=4$, C1', c='darkgoldenrod', ls='-.')
    plt.plot(dat[:,0], dat[:,19], label='$L=6$, H0', c='darkgreen', ls='-')
    plt.plot(dat[:,0], dat[:,21], label='$L=6$, H1', c='blue', ls='-')
    plt.plot(dat[:,0], dat[:,23], label='$L=6$, H2', c='darkred', ls='-')
    plt.plot(dat[:,0], dat[:,25], label='$L=6$, C1', c='darkgoldenrod', ls='-')
    plt.legend(loc='best', fontsize='12')
    plt.ylim([0,1.2])
    plt.tight_layout()
    plt.savefig('figure5-right.png')


