\documentclass[11pt,letterpaper]{article}
\begin{document}
\centerline{\textbf{Response to Referree on DA13279}}
I am truly grateful to the referree for their comments on the paper. Several of the comments revealed a lack of clarity in the paper. As detailed below, I have made various additions to the paper to make more clear how the algorithm works, and what the motivation is.

The referee's comments are in italics, and my responses follow in-line. I have taken the liberty of numbering the comments and questions for easy reference.

\begin{enumerate}
\item
\textit{I think the aim of this paper is to develop a numerical algorithm to address the physics of many-body problems using the Hamiltonian formalism. If this is not correct, I have failed in my understanding of this paper and another referee should be consulted.}

This is correct.

\item
\textit{The first equation in the paper confused me. Is $M_{ij}$ a matrix of size defined by the number of operators and that the right-hand side of that equation is evaluated in a fixed state $\psi$?}

This statement is correct as far as it goes. However, it seems to be linked to understandable confusion later in the report (questions 4 and 7 below). Let me try and clarify here, and describe what changes I have made to the introduction. Changes made elsewhere to clarify related points (in response to 4 and 7 below) are mentioned further down.

For any state $|\psi\rangle$, we can construct a matrix $M$, defined as in equation (1). We know that this matrix $M$ must be positive semi-definite. As $M$ is constructed from expectation values, a bound on the behavior of $M$ is a bound on the behavior of expectation values, which must be obeyed by every quantum state. One particular type of bound is of interest to us: lower bounds on the energy. These have obvious direct relevance to ground-state physics, and in the limit of a large number of basis operators, are guaranteed to converge to the true ground state energy.

Note that this procedure does not involve fixing or selecting any single state. We are proving a property that must be true of all quantum states.

To help future readers, I have added, at the beginning of the sixth paragraph of the introduction: ``The computational strategy of the quantum-mechanical bootstrap is to use (1) to derive lower bounds on the expectation value $\langle H\rangle$, which must be obeyed by all states. These are bounds on the ground-state energy, and as more operators are used in the construction of $M$, the bounds converge to the true ground state energy.'' Additionally, in order to make more clear the connection between lower bounds and the optimization problem, after equation (2) I have added ``Because this optimization problem is convex, the minimum obtained is guaranteed to be the global minimum, and therefore a lower bound on $\langle H \rangle$ that is valid for all states.''

Separately from all of these concerns, I have switched the paper to discuss density matrices $\rho$ rather than pure states $|\psi\rangle$ in several places. This doesn't have any impact on this discussion, but I have found that for some audiences it makes for a substantially cleaner explanation.

\item
\textit{I do not understand the inequality in equation (1). I interpret $v^\dagger M v$ as $\langle \psi |O^\dagger | \psi\rangle$ where $O = \sum_j v_j O_j$. If so, I agree that $v^\dagger M v \ge 0$ and this implies that all eigenvalues of M are non-negative. But, this does not imply all matrix
elements have to be non-negative.}

Correct on all points---however, at no point does the paper say that all matrix elements have to be non-negative! I think the confusion here arises from the notation $M \succeq 0$, used to denote that a matrix is positive semidefinite. Note that ``$\succeq$'' is a different symbol from ``$\ge$''. The notation is standard (so that I will not pick a different one), but I have added a footnote to clarify the situation for any readers for whom it is new.

\item \textit{The author starts section III by saying the algorithm has four
steps. Assuming the ”second” in the middle of the second paragraph of section
III refers to the second step, where does one use the constraints. Is the size
of the matrix reduced. If ``Finally'' in the fourth paragraph of section III
refers to the fourth step, this just seems to be a study of the results.
Nowhere in the steps of the algorithm is the choice of $\psi$ mentioned. Is it
a fixed state or not?
}

I have added explicit ``second step'' and ``fourth step'' phrasing to make the start of that section less confusing.

The size of the matrix is not reduced by the choice of constraints---rather, the matrix elements are constrained to be linearly related to each other. (This does reduce the dimension of the space over which the minimization must be performed, of course.) Without these constraints, no non-trivial bounds could be proven. The constraints are constructed in the second step and used in the third, where the SDP is solved. To make this explicit I have modified the end of the second paragraph of this section to read: ``In the second step of the algorithm, these two sets of constraints are combined to construct a semi-definite program. The quantity to be minimized is the expectation value of the Hamiltonian, which can be expressed as a linear combination of matrix elements of $M$. The constraints are the linear relations between elements of $M$ derived from commutation relations, and the nonlinear requirement $M \succeq 0$.''

\item \textit{The sign problem usually is a problem of the Lagrangian formalism. It is not present in the Hamiltonian formalism.
So, the algorithm in this paper does not solve the sign problem in any sense. What does the paper achieve toward a
solution of the problem?
}

Unfortunately it is not correct that the Hamiltonian formalism lacks a sign problem. As an example, diffusion Monte Carlo methods are usually cast in the Hamiltonian formalism, but certainly suffer from sign problems at finite fermion density!

I believe the referee may have in mind a superficially similar fact: algorithms that work explicitly with quantum states in memory (such as direct diagonalization) do not suffer from a sign problem. This is true; however (again as the referee mentions later on) these algorithms do suffer from the need to keep track of a number of amplitudes exponential in the volume of the system. (I'll discuss variational methods in response to 6 below.)

This paper proposes an algorithm that does not experience a sign problem, but simultaneously does not appear to require an exponential amount of resources in order to obtain quantitatively accurate results. Even in cases where the results are not trusted to be quantitatively accurate, the algorithm yields a rigorous lower bound on the ground state energy, which can be useful on its own.

As this addresses the question ``what is the point of this paper'', I have re-written the abstract to try and avoid it with future readers.

\item \textit{If one is using the Hamiltonian formalism, why not use a variational technique or a related technique like the
Lanczos algorithm to find the ground state energy? Why is this method superior.
}

The paper presents no argument that this method is superior, and I don't mean to make that claim here either. The two claims I would like to make are as follows. First, that this method is substantially different, and therefore may be successful in cases where variational methods historically haven't been (and, of course, vice versa). When there is no single algorithm that is guaranteed to be successful, as variational methods are not, it is useful to have many options. Methods based on semidefinite programs are one, introduced only recently, and the purpose of this paper is to explore what problems they may be useful for.

Second, this method is a useful counterpart to variational methods in particular, due to the fact that it yields a rigorous lower bound on the ground-state energy to complement the variational upper bound. This latter point was previously mentioned in the introduction, but I have also added it to the discussion section, adding what is now the fourth paragraph.

\item\textit{Is the state $\psi$ mentioned in the beginning of the paper fixed. If so, how is this choice made and how does it affect
the physics. If not, the number of states grows exponentially with L. This is a serious numerical bottleneck. I see a
remark to this issue at the bottom left side of page 4 but I do not understand it since I do now what $\psi$ is.}

As discussed above, the state is not fixed. Rather, the bounds proven are true for all quantum states. The estimates on expectation values obtained are valid for that quantum state which minimized the Hamiltonian expectation value, i.e.~the ground state.

No step in the algorithm involves explicit reference to any quantum state, and therefore there is no exponential cost in $L$. It is conceivable that, in order to obtain a quantitatively good bound on the ground state energy, an exponential number of operators must be used to construct the matrix $M$, but this is not observed in this work. I have added to the first paragraph of the discussion section: ``The SDPs used are small relative to the size of the Hilbert space of the system under study''.

\end{enumerate}

\end{document}
