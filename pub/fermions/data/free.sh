#!/bin/sh

set -e

free=../../../hamiltonian/thirring/free.py
infinite=../../../hamiltonian/thirring/infinite.py

source ../../../env/bin/activate

{
	m=1.0
	mu=1.333
	for L in `seq 1 15`; do
		echo -n "$L $mu "
		echo -n "`$free $m $mu` "
		echo -n "`$infinite $L $m $mu 0. -1` "
		echo
	done
} > free-1.0.dat

{
	m=0.3
	mu=0.4
	for L in `seq 1 15`; do
		echo -n "$L $mu "
		echo -n "`$free $m $mu` "
		echo -n "`$infinite $L $m $mu 0. -1` "
		echo
	done
} > free-0.3.dat

{
	m=0.1
	mu=0.1333
	for L in `seq 1 15`; do
		echo -n "$L $mu "
		echo -n "`$free $m $mu` "
		echo -n "`$infinite $L $m $mu 0. -1` "
		echo
	done
} > free-0.1.dat

