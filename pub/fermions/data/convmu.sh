#!/bin/sh

set -e

exact=../../../hamiltonian/thirring/exact.py
lattice=../../../hamiltonian/thirring/lattice.py

source ../../../env/bin/activate

N=10

for mu in `seq 0. 0.02 2.`; do
	for g2 in 0. 0.1 0.5; do
		echo -n "$mu $g2 "
		echo -n "`$exact $N 0.05 $mu $g2` "
		echo -n "`$lattice $N 0.05 $mu $g2 0` "
		echo -n "`$lattice $N 0.05 $mu $g2 1` "
		echo -n "`$lattice $N 0.05 $mu $g2 2` "
		echo -n "`$lattice $N 0.05 $mu $g2 3` "
		echo
	done
done

