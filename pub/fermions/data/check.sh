#!/bin/sh

set -e

exact=../../../hamiltonian/thirring/exact.py
lattice=../../../hamiltonian/thirring/lattice.py

source ../../../env/bin/activate

N=10

for mu in `seq 0. 0.02 2.`; do
	echo -n "$mu "
	echo -n "`$exact $N 0.05 $mu 0.5` "
	echo -n "`$lattice $N 0.05 $mu 0.5 0` "
	echo -n "`$lattice $N 0.05 $mu 0.5 1` "
	echo -n "`$lattice $N 0.05 $mu 0.5 2` "
	echo -n "`$lattice $N 0.05 $mu 0.5 3` "
	echo
done
