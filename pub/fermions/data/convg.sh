#!/bin/sh

set -e

exact=../../../hamiltonian/thirring/exact.py
lattice=../../../hamiltonian/thirring/lattice.py
bare=../../../hamiltonian/thirring/bare.py

source ../../../env/bin/activate

N=10

for mu in 0. 0.5 1.; do
	for g2 in `seq 0. 0.02 0.7`; do
		m=`$bare $N 0.2 $g2`
		echo -n "$mu $g2 "
		echo -n "`$exact $N $m $mu $g2` "
		echo -n "`$lattice $N $m $mu $g2 0` "
		echo -n "`$lattice $N $m $mu $g2 1` "
		echo -n "`$lattice $N $m $mu $g2 2` "
		echo -n "`$lattice $N $m $mu $g2 3` "
		echo
	done
done
