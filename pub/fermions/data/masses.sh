#!/bin/sh

tuned=../../../hamiltonian/thirring/tuned.py

for g2 in `seq 0. 0.02 0.6`; do
	echo -n "$g2 "
	$tuned 10 0.2 $g2
done
