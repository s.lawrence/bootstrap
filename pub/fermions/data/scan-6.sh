#!/bin/sh

lattice=`dirname $0`/../../../hamiltonian/thirring/lattice.py
exact=`dirname $0`/../../../hamiltonian/thirring/exact.py

L=6
m=0.5
g2=1.

for level in 0 1 2; do
	for mu in `seq 0 0.1 3`; do
		echo -n "$level $mu "
		echo -n "`$lattice $L $m $mu $g2 $level` "
		$exact $L $m $mu $g2
	done
done
