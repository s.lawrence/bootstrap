#!/bin/sh

set -e

infinite=../../../hamiltonian/thirring/infinite.py

source ../../../env/bin/activate

for g2 in 0.1 0.5; do
	{
		for mu in `seq 0. 0.02 2.`; do
			echo -n "$mu $g2 "
			for L in 2 4 6; do
				echo -n "`$infinite $L 0.05 $mu $g2 0` "
				echo -n "`$infinite $L 0.05 $mu $g2 1` "
				echo -n "`$infinite $L 0.05 $mu $g2 2` "
				echo -n "`$infinite $L 0.05 $mu $g2 3` "
			done
			echo
		done
	} > inf-$g2.dat
done
