#!/usr/bin/env python

"""
Bootstrapping a small, periodic lattice.
"""

import itertools
from math import cos,pi,sin
import os
import picos
import psutil
import sys

def memstat():
    return psutil.Process(os.getpid()).memory_info().rss / (1 << 20)

def odd(op):
    return len(op) % 2 == 1

def even(op):
    return not odd(op)

L = 4

m2 = 0.5
lamda = 0.1

theta = 2*pi*float(sys.argv[1])

K1 = 7

problem = picos.Problem(verbosity=0)
ops = [()]
# One-point functions
for k in range(1,K1):
    ops += [(a,)*k for a in range(L)]

# Two-point functions
ops += [(a,b) for a in range(L) for b in range(a+1,L)]

if False:
    ops += [(a,a,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,b,b) for a in range(L) for b in range(a+1,L)]

if True:
    ops += [(a,a,a,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,b,b,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,a,b,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,a,b,b) for a in range(L) for b in range(a+1,L)]

if False:
    ops += [(a,a,a,a,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,a,a,b,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,a,b,b,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,b,b,b,b) for a in range(L) for b in range(a+1,L)]

if False:
    ops += [(a,a,a,a,a,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,b,b,b,b,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,a,a,a,b,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,a,b,b,b,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,a,a,b,b,b) for a in range(L) for b in range(a+1,L)]

if False:
    ops += [(a,a,a,a,a,a,a,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,a,a,a,a,a,b,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,a,a,a,a,b,b,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,a,a,a,b,b,b,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,a,a,b,b,b,b,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,a,b,b,b,b,b,b) for a in range(L) for b in range(a+1,L)]
    ops += [(a,b,b,b,b,b,b,b) for a in range(L) for b in range(a+1,L)]

# Three-point functions
if False:
    ops += [(a,b,c) for a in range(L) for b in range(a+1,L) for c in range(b+1,L)]
    ops += [(a,b,c,c) for a in range(L) for b in range(a+1,L) for c in range(b+1,L)]
    ops += [(a,b,b,c) for a in range(L) for b in range(a+1,L) for c in range(b+1,L)]
    ops += [(a,a,b,c) for a in range(L) for b in range(a+1,L) for c in range(b+1,L)]

# Four-point functions
if False:
    ops += [(a,b,c,d) for a in range(L) for b in range(a+1,L) for c in range(b+1,L) for d in range(c+1,L)]
    ops += [(a,a,b,b,c,c,d,d) for a in range(L) for b in range(a+1,L) for c in range(b+1,L) for d in range(c+1,L)]

E = {op: picos.RealVariable(f'<{op}>') for op in ops}
N = len(ops)
problem += E[()] == 1

M = picos.SymmetricVariable('M', N)
for i1, op1 in enumerate(ops):
    for i2, op2 in enumerate(ops):
        op = tuple(sorted(op1+op2))
        try:
            problem += M[i1,i2] == E[op]
        except:
            pass
problem += M >> 0

"""
Schwinger dyson equations. The action is:

  S = (∂ phi)**2 / 2 + m**2 * phi**2 / 2 + lambda * phi**4 / 4.

The general form for an S-D equation is: <∂f> = <f∂S>.
"""
for op in ops:
    for x in range(L):
        try:
            # Differentiate op with respect to x to get lhs.
            if op.count(x) > 0:
                i = op.index(x)
                lhs = op.count(x)*E[op[:i]+op[i+1:]]
            else:
                lhs = 0
            # Potential
            rhs = m2 * E[tuple(sorted((x,)+op))]
            rhs += lamda * E[tuple(sorted((x,x,x)+op))]
            # Kinetic (up and down)
            xp = (x+1)%L
            xm = (x-1)%L
            rhs += E[tuple(sorted(op+(x,)))] - E[tuple(sorted(op+(xp,)))]
            rhs += E[tuple(sorted(op+(x,)))] - E[tuple(sorted(op+(xm,)))]
            problem += lhs == rhs
        except:
            pass

if False:
    prob_min = problem.clone()
    prob_max = problem.clone()
    # Target some observable.
    prob_min.set_objective('min', E[(0,0)])
    prob_max.set_objective('max', E[(0,0)])
    sol_min = prob_min.solve(solver='mosek', primals=None, duals=None)
    sol_max = prob_max.solve(solver='mosek', primals=None, duals=None)

    print(sol_min.reported_value, sol_max.reported_value)

problem.set_objective('min', cos(theta)*E[(0,0)] + sin(theta)*E[(0,0,0,0)])
sol = problem.solve(solver='mosek', primals=None, duals=None)
print(theta, sol.reported_value)
