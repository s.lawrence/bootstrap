#!/usr/bin/env python

import picos
import matplotlib.pyplot as plt
import numpy as np

import matplotlib
matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1



x = np.linspace(-5,5,1000)
boltz = np.exp(-x**2/2 - x**4/4)
exact = np.trapz(x**2 * boltz,x)/np.trapz(boltz,x)

Ns = [6,8,10,12,14,16,18]
los = []
his = []
for N in Ns:
    problem = picos.Problem(verbosity=0)
    phi = [picos.RealVariable(f'phi{n}') for n in range(N)]
    problem.add_constraint(phi[0] == 1.)

    M = picos.SymmetricVariable('M', N//2)
    for n in range(N//2):
        for m in range(N//2):
            problem.add_constraint(M[n,m] == phi[n+m])
    problem.add_constraint(M >> 0)

    # Schwinger dyson equations. The action is: S = phi**2 / 2 + phi**4 / 4. The
    # general form for an S-D equation is: <∂f> = <f∂S>.
    for n in range(1,N-3):
        problem.add_constraint(n*phi[n-1] == phi[n+1] + phi[n+3])


    prob_min = problem.clone()
    prob_max = problem.clone()

    # Target some observable.
    prob_min.set_objective('min', phi[2])
    prob_max.set_objective('max', phi[2])
    sol_min = prob_min.solve(solver='mosek', primals=None, duals=None)
    sol_max = prob_max.solve(solver='mosek', primals=None, duals=None)
    los.append(sol_min.reported_value)
    his.append(sol_max.reported_value)
    print(N, sol_min.reported_value, sol_max.reported_value)


fig = plt.figure(figsize=(5,4))

top,bot = fig.subplots(nrows=2, sharex=True)

top.set_ylabel('$\\langle x^2\\rangle$')
top.set_xlabel('$N$')
top.plot([0,19],[exact,exact],color='black',label='Exact')
top.plot(Ns,los,color='red')
top.plot(Ns,his,color='red')
top.fill_between(Ns,los,his,color='#aaaaff',label='Bound')
top.set_xlim([5,19])
top.set_xticks([6,8,10,12,14,16,18])
top.set_ylim([0,0.7])
top.legend(loc='best')

bot.set_ylabel('$\\langle x^2\\rangle_{\mathrm{max}} - \\langle x^2\\rangle_{\mathrm{min}}$')
bot.set_xlabel('$N$')
bot.set_yscale('log')
bot.scatter(Ns,np.array(his)-np.array(los),color='blue')
plt.tight_layout()

plt.subplots_adjust(hspace=0.)
plt.savefig('integral.png', dpi=300)

