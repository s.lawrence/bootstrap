#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib
matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

plt.figure(figsize=(5,4))
plt.ylabel('$\\langle x^2\\rangle$')
plt.xlabel('$N$')
plt.tight_layout()
plt.savefig('sk.png', dpi=300)

