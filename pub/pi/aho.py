#!/usr/bin/env python

import numpy as np

import matplotlib.pyplot as plt
import matplotlib
matplotlib.style.use('classic')
matplotlib.rcParams['font.family'] = 'STIXGeneral'
matplotlib.rcParams['axes.prop_cycle'] = plt.cycler(color='krbg')
matplotlib.rcParams['legend.numpoints'] = 1

plt.figure(figsize=(5,4))
plt.xlabel('$\\langle x^2\\rangle$')
plt.ylabel('$\\langle x^4\\rangle$')

def plot_bounds(fname, **kwargs):
    with open(fname) as f:
        dat = np.array([[float(x) for x in l.split()] for l in f.readlines()])
    theta = dat[:,0]
    bound = dat[:,1]
    xs = []
    ys = []
    for i in range(len(dat)-1):
        ip = (i+1)%len(dat)
        th = theta[i]
        thp = theta[ip]
        print(th,thp)
        b = bound[i]
        bp = bound[ip]
        # Each bound defines a line: cos(th)*x+sin(th)*y==b.
        bs = np.array([b,bp])
        A = np.array([[np.cos(th),np.sin(th)],[np.cos(thp),np.sin(thp)]])
        x,y = np.linalg.inv(A)@bs
        xs.append(x)
        ys.append(y)
    xs.append(xs[0])
    ys.append(ys[0])
    plt.plot(xs,ys,**kwargs)

plot_bounds('gendata/aho-bounds1', ls='--')
plot_bounds('gendata/aho-bounds2', ls='-')

#plt.legend(loc='best')
plt.tight_layout()
plt.savefig('aho.png', dpi=300)

