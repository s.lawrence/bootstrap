\documentclass[reprint,twocolumn,superscriptaddress,showpacs,nofootinbib,notitlepage]{revtex4-2}

\usepackage{graphicx}
\usepackage{latexsym,amsmath,amssymb,lmodern,float,url}
\usepackage{natbib}
\usepackage{xcolor}
\usepackage{microtype}
\usepackage{comment}


\usepackage[colorlinks=true,backref=false, linktocpage=true,
citecolor=blue,urlcolor=blue,linkcolor=blue,pdfpagemode=UseOutlines]{hyperref}

\hypersetup{%
  bookmarksnumbered=true,
  pdftitle = {},
  pdfsubject = {},
  pdfauthor = {},
  pdfkeywords = {}
}

\def\Re {\operatorname{{Re}}}
\def\Im {\operatorname{{Im}}}
\def\Tr {\operatorname{{Tr}}}

\newcommand{\todo}{\colorbox{pink}{\textsc{Todo}}}

\begin{document}
\title{Semidefinite Programs for Lattice Path Integrals}

\author{Scott Lawrence}
\email{scott.lawrence-1@colorado.edu}
\affiliation{Department of Physics, University of Colorado, Boulder, CO 80309, USA}
\date{\today}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
This paper constructs semidefinite programs for obtaining bounds on observables computed from lattice path integrals. This approach is similar to recent methods for bootstrapping quantum mechanical systems in the Hamiltonian formalism, but enables non-zero temperatures to be treated. Finally, semidefinite programs can be formulated in a similar way for systems at finite fermion density, as well as, in principle, Schwinger-Keldysh path integrals. Both of these are regimes in which standard lattice Monte Carlo methods fall prey to sign problems.
\end{abstract}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}\label{sec:introduction}

Lattice field theory provides a non-perturbative approach to defining quantum field theories. Starting from the Euclidean path integral for a field theory at finite volume and temperature, space and time are discretized, yielding a finite-dimensional integral free of ultraviolet divergences. Lattice field theories are most frequently studied by Monte Carlo methods, which are often the only technique available for non-perturbative computations of expectation values in quantum field theories. Unfortunately, Monte Carlo methods encounter the \emph{sign problem} for a wide class of models, including relativistic fermions at non-zero density, and real-time path integrals. The sign problem represents the primary obstacle to the first-principles computation of non-perturbative physics in many systems, from out-of-equilibrium matter to the high-density nuclear equation of state. Thus the sign problem motivates the search for computational approaches to lattice field theories that do not rely on Monte Carlo methods.

It was recently shown that a variety of quantum mechanical systems can be efficiently studied via ``the bootstrap,'' i.e.~semidefinite programming~\cite{Han:2020bkb,Berenstein:2021dyf,Berenstein:2021loy}. Briefly, the requirement that the Hilbert space inner product be positive definite, combined with the imposition of commutation relations, yields a tight lower bound on the permissible energies. Moreover, the task of solving for the lowest permitted energy (the ground state energy) can be performed in time polynomial in the number of operators.

In typical quantum systems, there are an infinite number of linearly independent operators to be considered. To obtain a tractable computation, one can construct a sequence of semidefinite programs by considering successively larger bases of operators. This idea---constraining a quantum system by requiring positivity on increasingly large operator bases---has appeared in the past, notably in the NPA hierarchy used to constrain quantum correlations~\cite{navascues2007bounding,navascues2008convergent}.

The most striking success of semidefinite programs in quantum physics has been in their application to conformal field theories~\cite{Kos:2016ysd,Simmons-Duffin:2015qma,Rattazzi:2008pe}. The \emph{conformal bootstrap} (see~\cite{Poland:2018epd} for a review) proceeds in a broadly similar fashion to the quantum mechanical bootstrap discussed above, only using positivity on the radially quantized Hilbert space, and the powerful constraints of conformal symmetry in place of commutation relations. Recently, the same computational tools have been applied to bound the space of permissible S-matrices~\cite{He:2018uxa,Caron-Huot:2020cmc,Albert:2022oes}, returning the field to its historical origins~\cite{chew1961s}.

Quantum field theories can often be approximated, in the Hamiltonian formulation, by a sequence of lattice systems, each of which is an ordinary quantum-mechanical system with a Hilbert space of finite (or at most countably infinite) dimension. As a result the quantum mechanical bootstrap of~\cite{Han:2020bkb,Berenstein:2021dyf,Berenstein:2021loy} can be rather straightforwardly applied to the investigation of the ground state properties of field theories~\cite{barthel2012solving,Lawrence:2021msm}. This approach leaves large gaps, however, being unable to approach either finite-temperature physics or real-time dynamics.

This paper shows how a different class of semidefinite programs can be constructed for quantum systems in the path integral formalism, similar to~\cite{Kazakov:2022xuh}. There are two key advantages to this approach, over the previously examined Hamiltonian bootstrap. First, working with lattice path integrals, rather than directly with the Hamiltonian, makes the computation of finite-temperature and real-time properties natural. Furthermore, each observable can now be individually bounded from above and below; previously, only the ground state energy was bounded, and only from below.

Section~\ref{sec:sdp} defines semidefinite programs, and discusses in general how they may be obtained from various physical models. The next three sections demonstrate the use of semidefinite programs as a computational tool for four different systems. First, in Section~\ref{sec:aho}, the Euclidean path integral for the anharmonic oscillator is considered. This is arguably the simplest case, as it is an integral over scalar variables with a positive real Boltzmann factor. Section~\ref{sec:ising} switches to the Ising model, which has some computational advantages due to the finite number of linearly independent operators available on any fixed lattice. Fermions at finite density and temperature are considered in Section~\ref{sec:fermions}, demonstrating that SDPs can evade the fermion sign problem which obstructs Monte Carlo calculations. Finally, Section~\ref{sec:discussion} discusses the many remaining open questions about this family of methods.

All code used for this paper is available online~\cite{code}. Semidefinite programs are solved using the MOSEK toolkit~\cite{mosek}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Semidefinite Programs}\label{sec:sdp}
A semidefinite program (SDP) is a particular convex optimization problem over a space of positive semidefinite matrices. An SDP may be specified by a Hermitian $N\times N$ matrix $C$, and $m$ pairs of $N\times N$ matrices $A_i$ and complex numbers $b_i$. The solution to the SDP is the matrix $X$ that minimizes $\Tr C^\dagger X$ subject to the $m$ constraints $\Tr A_i^\dagger C = b_i$.

Semidefinite programs can arise from physical axioms in many different ways. In the context of the Hamiltonian bootstrap~\cite{Han:2020bkb,Berenstein:2021dyf,Berenstein:2021loy,barthel2012solving,Lawrence:2021msm}, the underyling physical requirements are: that the norm on the Hilbert space be positive definite, and that appropriate commutation relations be obeyed. For any state $|\Psi\rangle$, we have $\langle \Psi|\Psi\rangle \ge 0$, with equality achieved only if $|\Psi\rangle = 0$. As a result, for any basis of operators $\mathcal O_i$, the matrix of expectation values $M_{ij} \equiv \langle \mathcal O_i^\dagger \mathcal O_j\rangle$ must be positive semidefinite: $M \succeq 0$. The commutation relations (e.g. $[x,p] = i \hbar$) then supply a host of linear relations between expectation values, completing the specification of the (convex) space of permitted expectation values. To complete the SDP, we minimize the expectation value of the Hamiltonian over this space, obtaining the ground state energy.

SDPs can also arise directly from expectation values over probability distributions. Let $p(x)$ be a probability distribution and $f(x)$ any (potentially complex-valued) function. As the integral of a non-negative real function is itself non-negative, it follows that $\int p(x) |f(x)|^2 \ge 0$. As with the Hamiltonian case discussed above, we can now consider a basis of functions $f_i$, and construct a matrix which must be positive semidefinite:
\begin{equation}\label{eq:psd}
M_{ij} \equiv \int p(x) f_i(x)^* f_j(x) \succeq 0
\end{equation}
Importantly, this is a much stronger constraint that simply requiring $\int p |f_i|^2 \ge 0$ for each basis function $f_i$.

By itself, this constraint does not yield any non-trivial bounds on expectation values. To make the SDP more useful, assume that the probability distribution has the form $p(x) = e^{-S(x)}$ with $S$ some polynomial of even order---as is often the case in field theory. Now the requirement that integrals of total derivatives vanish yields a Schwinger-Dyson equation for every function $f$:
\begin{equation}\label{eq:DS}
\langle \partial f \rangle = \langle f \partial S\rangle
\end{equation}
Because this constraint is linear, it is sufficient to impose it for a complete set of basis functions $f_i$. A convenient set to work with are the monomials $f_i(x) = x_i$. In the case of the action $S = \frac 1 2 x^2 + \frac \lambda 4 x^4$, the resulting $k$th Schwinger-Dyson equation reads
\begin{equation}
k \langle x^{k-1} \rangle = \langle x^{k+1}\rangle + \lambda\langle x^{k+3}\rangle
\text.
\end{equation}
Note that in the special case of $\lambda = 0$, no further constraints are needed to solve for the expectation values of arbitrary powers of $x$. The first non-trivial Schwinger-Dyson equation is $\langle x^2\rangle = 1$, and the remainder provide a recursion relation yielding the expectations of all other $x^k$.

\begin{figure}
\centering\includegraphics[width=0.9\linewidth]{integral}
\caption{SDP-obtained bounds, as a function of the number $N$ of operators considered, for the expectation value $\langle x^2\rangle$ with respect to the probability distribution $p(x) = e^{-\frac 1 2 x^2 - \frac 1 4 x^4}$. The bottom panel demonstrates the typical exponential convergence by showing the difference between the upper and lower bounds; i.e.~the size of the permitted region. \label{fig:integral}}
\end{figure}

Imposing Eqs.~(\ref{eq:psd}) and (\ref{eq:DS}) is typically sufficient to obtain arbitrarily tight bounds on any polynomial $f(x)$. However, as it is not possible to keep track of an infinite number of expectation values when solving the SDP, a practical computation must truncate the basis. Figure~\ref{fig:integral} shows the upper and lower bounds obtained on the expectation $\langle x^2\rangle$ with respect to the action $S = \frac 1 2 x^2 + \frac 1 4 x^4$, as a function of the number $N$ of basis functions $f_i(x) = x^i$ used to construct the SDP. Note that this approach differs from the Hamiltonian bootstrap in that bounds are achieved not just on the ground state energy, but on any expectation value. Moreover, both upper and lower bounds are available.

When considering real-time evolution (discussed in Section~\ref{sec:discussion}) and fermionic systems (in Section~\ref{sec:fermions}), we will find it convenient to mix these constraints. In both cases, the Boltzmann factor $e^{-S}$ is no longer positive, and therefore the positivity requirement on expectation values cannot be read directly from the path integral. Nevertheless, the Hilbert space structure (from which the path integral originally emerged) still maintains its own positivity property, which is sufficient to obtain informative SDPs.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Anharmonic Oscillator}\label{sec:aho}
We begin with the quantum anharmonic oscillator, defined by the Hamiltonian
\begin{equation}\label{eq:H_aho}
H_{\mathrm{aho}} = \frac 1 2 p^2 + \frac{\omega^2}{2} x^2 + \lambda x^4
\text.
\end{equation}
The Hamiltonian bootstrap of this system has been demonstrated to rapidly converge to yield the true ground-state energy~\cite{Han:2020bkb,Lawrence:2021msm}. Here we will pursue bounds on finite-temperature observables.

A path integral corresponding to Eq.~(\ref{eq:H_aho}) may be obtained beginning with the expression $\Tr e^{-\beta H}$ for the partition function, and Trotterizing the exponential of the Hamiltonian. Working in units where the timesteps are of size $\Delta\beta = 1$, this procedure yields the action:
\begin{equation}\label{eq:S_aho}
S_{\mathrm{aho}} = \sum_{\langle r r'\rangle} \frac 1 2 (x_r - x_{r'})^2+ \sum_r \Big[\frac{\omega^2}{2} x_r^2 + \lambda x_r^4 \Big]
\text.
\end{equation}
The continuum limit (in time) corresponds to scaling the couplings $\omega^2,\lambda$, and the position $x$, according to dimensional analysis.

Schwinger-Dyson equations for this system are obtained much as they were for the one-dimensional integral discussed in the section above. For every linearly independent operator $f(x)$, there are now $\beta$ Schwinger-Dyson equations to be imposed, obtained by choosing a site $r$ and requiring
\begin{equation}\label{eq:scalar-ds}
0 = \int d^\beta x \; \frac{\partial}{\partial x_r} \Big(f(x) e^{-S(x)}\Big)
\end{equation}
As before, it is convenient to take the linearly independent operators to be (multivariate) monomials in $x$. We may organize the monomials into the one-point functions $x_r^a$, the two-point functions $x_r^a y_r^b$, and so on.

The free theory, where $\lambda = 0$, is a special case for this quantum mechanical system just as it was for the one-dimensional integral considered in the previous section. The action is quadratic in the fields: $S(x) = \frac 1 2 x^T M x$ for some inverse propagator $M$. Two sorts of Schwinger-Dyson equations are obtained from the first-order monomials:
\begin{align}
1 &= \langle x_r (M x)_r\rangle\text{, and}\\
0 &= \langle x_{r'} (M x)_r\rangle \text{ when } r \ne r'\text.
\end{align}
Expanding the contractions with $M$, we obtain $\beta^2$ linear constraints on $\beta^2$ degrees of freedom. Solving this system yields all two-point functions, and repeating the procedure with higher-order polynomials again gives a recursion relation that can be used to solve for arbitrary expectation values.

\begin{figure}
\centering\includegraphics[width=0.9\linewidth]{aho}
\caption{Semidefinite bounds on the anharmonic oscillator at finite temperature. The parameters of the model are $\beta = 8$, $m^2 = 0.5$, and $\lambda = 0.1$. The convex permitted region from three different truncations, each described in the text, is shown.\label{fig:aho}}
\end{figure}

The last step in constructing the SDP is the selection of a truncated basis of monomials. An automated procedure for building a high-quality truncation is described in~\cite{Lawrence:2021msm}. Unfortunately, this procedure is extremely resource-intensive, although strictly polynomial in the size of the desired basis and the volume of the physical system. For the purposes of this demonstration, it is sufficient to use the physical intuition that few-point functions matter more than many-point functions.

After experimenting with their performance on a small $\beta = 4$ lattice, the three truncations chosen are as follows:
\begin{itemize}
\item \todo
\item Added to all operators in truncation 1, \todo
\item Added to all operators in truncation 2, \todo
\end{itemize}

Figure~\ref{fig:aho} demonstrates the sorts of bounds obtained by an SDP constructed in this fashion for the anharmonic oscillator at $\beta = 8$, $m^2 = 0.5$, and $\lambda = 0.1$. Shown is the convex permitted region defined by these three truncations, projected onto the plane of $\langle x^2\rangle$ and $\langle x^4 \rangle$. The chosen truncations are labelled as above.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Ising Model}\label{sec:ising}
The anharmonic oscillator (and its multi-dimensional generalization, scalar field theory) has infinitely many operators that must be considered in order to generate a tight bound. In this section we consider instead the one-dimensional Ising model on $L$ sites, which has only $2^L$ linearly independent operators. 

The action of the one-dimensional Ising model is
\begin{equation}
S_{\mathrm{Ising}} = -\mu \sum_x s_x - J \sum_{\langle x y\rangle} s_x s_y
\end{equation}
where each spin $s$ takes values in $\{-1,+1\}$. This action describes a single quantum-mechanical spin with Hamiltonian proportional to $\sigma_x$.

In order to find the appropriate Schwinger-Dyson equations---the equivalent of Eq.~(\ref{eq:scalar-ds})---we must first find a sensible notion of differentiation with respect to the discrete spin variables. A natural choice is finite differencing: $\Delta_s f(s) \equiv f(s) - f(-s)$. However, in order for the Schwinger-Dyson equations to be expressible exclusively in terms of expectation values, we must write $\Delta_s (f e^{-S})$ with an explicit $e^{-S}$ factor.

This is possible due to the fact that the function $\Delta_s f(s)$ is only defined on the corners of the hypercube $\{-1,+1\}^L$. The function $\Delta_s f(s)$ is thus defined at $2^L$ points. At the same time, there are $2^L$ linearly independent monomials available. The problem of expressing $\Delta_s f(s)$ as $e^{-S}$ times a polynomial of the spins can therefore be phrased as a lienar system of equations in $2^L$ variables. For sparse Ising models (including those that arise from the path integral representations of various lattice transverse Ising systems), only adjacent spin variables are relevant, and this linear system is of constant size in the lattice volume.

In practice it is simplest to proceed algebraically. In the case of one site, a little algebra reveals:
\begin{align}
&\Delta_s e^{\mu s}= e^{\mu s} \big(1 - \cosh 2 \mu + s \sinh 2 \mu\big)\text{,}\\
&\Delta_s \big(s e^{\mu s}\big)= e^{\mu s}\big(s + s \cosh 2 \mu - \sinh 2 \mu\big)\text.
\end{align}
For the one-dimensional chain \todo
\begin{equation}
\todo
\end{equation}

\begin{comment}
With $\Delta_s$ defined, a complete set of Schwinger-Dyson equations for the Ising model are obtained from the $2^L$ monomials $f_i(s)$:
\begin{equation}
\todo
\text.
\end{equation}
\end{comment}
It remains to write the positivity constraints. Here there are no significant differences from the scalar case. Any polynomial of $s_r$, which can be written exactly as a square, must have non-negative expectation value. As a result, having selected a complete basis of polynomials $f_i(s)$ (most conveniently, all $2^L$ monomials), the matrix $M_{ij} \equiv \langle f_i(s) f_j(s)\rangle$ must be positive semidefinite.

It is instructive to consider the one-site case, which is easily solved by hand. With action $S = -\mu s$, the only Schwinger-Dyson equation is \begin{equation}
0 = 1 - \cosh 2 \mu + \langle s \rangle \sinh 2 \mu
\text.
\end{equation}
This equation, of course, is sufficient to determine $\langle s \rangle = \tanh \mu$---the only non-trivial expectation value in this system---without the need for any positivity constraints. This is always true for Ising systems, and generally to be expected for systems with finite numbers of linearly independent operators. In practice, however, writing down all $2^L-1$ Schwinger-Dyson equations is not possible.

For completeness, the one-site positivity constraint is
\begin{equation}
\left(
\begin{matrix}
1 & \langle s \rangle \\
\langle s \rangle & 1
\end{matrix}
\right)
\succeq 0
\text,
\end{equation}
which corresponds to the constraint that $\langle s \rangle \in [-1,1]$.

\begin{figure*}
\includegraphics[width=0.45\linewidth]{ising}
\hfil
\includegraphics[width=0.45\linewidth]{ising} % TODO correlator
\caption{\todo\label{fig:ising}}
\end{figure*}
\todo truncations

Figure~\ref{fig:ising} \todo

\begin{comment}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Schwinger-Keldysh}\label{sec:sk}
Let us now consider the problem of computing dynamical behavior in a quantum system perturbed from thermal equilibrium. Our starting point is the definition of a real-time-separated expectation value:
\begin{equation}\label{eq:tsexp}
\langle \mathcal O(t) \mathcal O(0)\rangle
\equiv
\Tr e^{-\beta H} e^{i H t} \mathcal O e^{-i H t} \mathcal O
\text.
\end{equation}
\todo

There is a good deal of freedom in the derivation of this expression. The first two exponentials in Eq.~(\ref{eq:tsexp}) can be ordered arbitrarily, or indeed mixed, without changing the result of the expression. However, the resulting path integrals will apply the real- and imaginary-time evolution in different orders. The path taken through complexified time is known as the \emph{Schwinger-Keldysh contour}, and the choice taken is above is colloquially referred to as the \emph{L-contour}.

Monte Carlo algorithms for Schwinger-Keldysh path integrals were explored, for the anharmonic oscillator and scalar field theory, in~\cite{Alexandru:2016gsd,Alexandru:2017lqr}. In the case of the Ising model, the Schwinger-Keldysh action resulting from this procedure is
\begin{equation}
S_{\mathrm{SK}} = \todo
\text,
\end{equation}
with
\begin{equation}
a_t = \begin{cases}
1 & t < \beta \\
i & \beta < t < \beta+T\\
-i & \beta+T < t
\end{cases}
\end{equation}
defining the chosen Schwinger-Keldysh contour (the L-contour).

The Schwinger-Dyson equations for this action are obtained in exactly the manner of Section~\ref{sec:ising}. However, unlike the original Ising action, the Schwinger-Kelysh action is not generally real. As a result, the Boltzmann factor $e^{-S_{\mathrm{SK}}}$ is generally complex, rather than real and positive. In the previous two sections, the positivity constraints were derived from the fact that the integral of a non-negative integrand must itself be non-negative---this approach is plainly inadequate for Schwinger-Keldysh actions and other path integrals with sign problems.

Nevertheless, positivity constraints are still available on the original Hilbert space; indeed, constraints of this form were applied to spin systems that naively have a sign problem in~\cite{Lawrence:2021msm}.
\todo

\begin{figure}
\centering\includegraphics[width=0.9\linewidth]{sk}
\caption{\todo\label{fig:sk}}
\end{figure}
\todo Figure~\ref{fig:sk}
\end{comment}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Fermions}\label{sec:fermions}
\todo introduce model~\cite{Pawlowski:2013pje} \cite{Hasenfratz:1983ba}
\begin{equation}
S_{\mathrm{f}} = \todo
\end{equation}
Note that we have not introduced an auxiliary field. Unlike in Monte Carlo calculations, it is possible to work directly with integrals over Grassmann variables; indeed, it is subsantially cheaper to do so.

\todo exact solution
\begin{equation}
\langle n \rangle =
\todo
\end{equation}
where $I_n(\alpha)$ denotes the Bessel functions of the first kind.
\todo

As in the previous section, positivity constraints do not follow directly from the path integral, but rather from the inner product on the Hilbert space. \todo

\todo derive D-S equations

\begin{figure}
\centering\includegraphics[width=0.9\linewidth]{fermions}
\caption{\todo\label{fig:fermions}}
\end{figure}
\todo Figure~\ref{fig:fermions}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion}\label{sec:discussion}
In principle, the techniques discussed above may be applicable \todo Let us now consider the problem of computing dynamical behavior in a quantum system perturbed from thermal equilibrium. Our starting point is the definition of a real-time-separated expectation value:
\begin{equation}\label{eq:tsexp}
\langle \mathcal O(t) \mathcal O(0)\rangle
\equiv
\Tr e^{-\beta H} e^{i H t} \mathcal O e^{-i H t} \mathcal O
\text.
\end{equation}
\todo

There is a good deal of freedom in the derivation of this expression. The first two exponentials in Eq.~(\ref{eq:tsexp}) can be ordered arbitrarily, or indeed mixed, without changing the result of the expression. However, the resulting path integrals will apply the real- and imaginary-time evolution in different orders. The path taken through complexified time is known as the \emph{Schwinger-Keldysh contour}, and the choice taken is above is colloquially referred to as the \emph{L-contour}.

Monte Carlo algorithms for Schwinger-Keldysh path integrals were explored, for the anharmonic oscillator and scalar field theory, in~\cite{Alexandru:2016gsd,Alexandru:2017lqr}. In the case of the Ising model, the Schwinger-Keldysh action resulting from this procedure is
\begin{equation}
S_{\mathrm{SK}} = \todo
\text,
\end{equation}
with
\begin{equation}
a_t = \begin{cases}
1 & t < \beta \\
i & \beta < t < \beta+T\\
-i & \beta+T < t
\end{cases}
\end{equation}
defining the chosen Schwinger-Keldysh contour (the L-contour).

The Schwinger-Dyson equations for this action are obtained in exactly the manner of Section~\ref{sec:ising}. However, unlike the original Ising action, the Schwinger-Kelysh action is not generally real. As a result, the Boltzmann factor $e^{-S_{\mathrm{SK}}}$ is generally complex, rather than real and positive. In the previous two sections, the positivity constraints were derived from the fact that the integral of a non-negative integrand must itself be non-negative---this approach is plainly inadequate for Schwinger-Keldysh actions and other path integrals with sign problems.

Nevertheless, positivity constraints are still available on the original Hilbert space; indeed, constraints of this form were applied to spin systems that naively have a sign problem in~\cite{Lawrence:2021msm}.
\todo


\todo

\begin{acknowledgments}
I am grateful to Paul Romatschke and Yukari Yamauchi for discussions on the Schwinger-Dyson equations for Ising systems, and to \todo for reviewing a draft of this manuscript.

This material is based upon work supported by the U.S. Department of Energy, Office of Science, Office of Nuclear Physics program under Award Number DE-SC-0017905.
\end{acknowledgments}
\bibliographystyle{apsrev4-2}
\bibliography{References}
\end{document}

