#!/usr/bin/env python
"""
Bootstrapping a small, periodic lattice.
"""

import itertools
from math import cosh,sinh
import picos
import sys

def operator(*a):
    r = set()
    for x in a:
        if x in r:
            r.remove(x)
        else:
            r.add(x)
    return tuple(sorted(r))

L = 8
J = 1.
K = 2  # or 2
mu = 0.3

ops = [()]
# One-point functions
ops += [(a,) for a in range(L)]
# Two-point functions
ops += [(a,b) for a in range(L) for b in range(a+1,L)]
# Three-point functions
ops += [(a,b,c) for a in range(L) for b in range(a+1,L) for c in range(b+1,L)]
# Four-point functions
ops += [(a,b,c,d) for a in range(L) for b in range(a+1,L) for c in range(b+1,L) for d in range(c+1,L)]

problem = picos.Problem(verbosity=0)

E = {op: picos.RealVariable(f'<{op}>') for op in ops}
N = len(E)
problem += E[()] == 1

M = picos.SymmetricVariable('M', N)
for i1, op1 in enumerate(ops):
    for i2, op2 in enumerate(ops):
        op = tuple(sorted(set(op1)^set(op2)))
        if len(op) <= K:
            try:
                problem += M[i1,i2] == E[op]
            except:
                pass
problem += M >> 0

"""
Schwinger-Dyson equations.
"""

# From the Boltzmann factor alone
for x in range(L):
    xp = (x+1)%L
    xm = (x-1)%L

    ex = E[(x,)]
    exp = E[tuple(sorted((x,xp)))]
    exm = E[tuple(sorted((x,xm)))]
    ep = E[tuple(sorted((xp,)))]
    em = E[tuple(sorted((xm,)))]
    epm = E[tuple(sorted((xp,xm)))]
    expm = E[tuple(sorted((x,xp,xm)))]

    zero = 1.
    # Eight nontrivial terms
    c2m,s2m = cosh(2*mu),-sinh(2*mu)
    c2J,s2J = cosh(2*J),-sinh(2*J)

    zero -= c2m*c2J*c2J
    zero -= c2m*c2J*s2J*exm
    zero -= c2m*s2J*c2J*exp
    zero -= c2m*s2J*s2J*epm

    zero -= s2m*c2J*c2J*ex
    zero -= s2m*c2J*s2J*em
    zero -= s2m*s2J*c2J*ep
    zero -= s2m*s2J*s2J*expm

    problem += zero == 0.

# From one-point functions
for y in range(L):
    for x in range(L):
        xp = (x+1)%L
        xm = (x-1)%L

        if y == x:
            e1 = E[operator(y)]
            ex = E[operator(x,y)]
            exp = E[operator(x,xp,y)]
            exm = E[operator(x,xm,y)]
            ep = E[operator(xp,y)]
            em = E[operator(xm,y)]
            epm = E[operator(xp,xm,y)]
            expm = E[operator(x,xp,xm,y)]

            zero = e1
            # Eight nontrivial terms
            c2m,s2m = cosh(2*mu),-sinh(2*mu)
            c2J,s2J = cosh(2*J),-sinh(2*J)

            zero += c2m*c2J*c2J*e1
            zero += c2m*c2J*s2J*exm
            zero += c2m*s2J*c2J*exp
            zero += c2m*s2J*s2J*epm

            zero += s2m*c2J*c2J*ex
            zero += s2m*c2J*s2J*em
            zero += s2m*s2J*c2J*ep
            zero += s2m*s2J*s2J*expm

            problem += zero == 0.

        else:
            e1 = E[operator(y)]
            ex = E[operator(x,y)]
            exp = E[operator(x,xp,y)]
            exm = E[operator(x,xm,y)]
            ep = E[operator(xp,y)]
            em = E[operator(xm,y)]
            epm = E[operator(xp,xm,y)]
            expm = E[operator(x,xp,xm,y)]

            zero = e1
            # Eight nontrivial terms
            c2m,s2m = cosh(2*mu),-sinh(2*mu)
            c2J,s2J = cosh(2*J),-sinh(2*J)

            zero -= c2m*c2J*c2J*e1
            zero -= c2m*c2J*s2J*exm
            zero -= c2m*s2J*c2J*exp
            zero -= c2m*s2J*s2J*epm

            zero -= s2m*c2J*c2J*ex
            zero -= s2m*c2J*s2J*em
            zero -= s2m*s2J*c2J*ep
            zero -= s2m*s2J*s2J*expm

            problem += zero == 0.

prob_min = problem.clone()
prob_max = problem.clone()

# Target some observable.
if True:
    prob_min.set_objective('min', E[(0,3)])
    prob_max.set_objective('max', E[(0,3)])
else:
    prob_min.set_objective('min', E[(0,)])
    prob_max.set_objective('max', E[(0,)])
sol_min = prob_min.solve(solver='mosek', primals=None, duals=None)
sol_max = prob_max.solve(solver='mosek', primals=None, duals=None)

print(sol_min.reported_value, sol_max.reported_value)

