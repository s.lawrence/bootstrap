# Bootstrap methods for quantum physics

## Papers

If using this code, or code derived from it, it may be appropriate to cite one
or more of the following papers.

- [Bootstrapping Lattice Vacua](https://arxiv.org/abs/2111.13007)

