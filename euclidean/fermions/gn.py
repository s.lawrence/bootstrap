#!/usr/bin/env python

"""
A fermion on a single site. With N=1, the Hilbert space is dimension 4 (because
we have a two-component spinor).
"""

import numpy as np
import sys

N = 1
m = float(sys.argv[1])
mu = float(sys.argv[2])
g = float(sys.argv[3])

D = 2**(2*N)

# Pauli operators
pauli_i = np.eye(2,dtype=np.complex128)
pauli_x = np.array([[0,1],[1,0]],dtype=np.complex128)
pauli_y = np.array([[0,-1j],[1j,0]],dtype=np.complex128)
pauli_z = np.array([[1,0],[0,-1]],dtype=np.complex128)
pauli_p = (pauli_x + 1j*pauli_y)/2
pauli_m = (pauli_x - 1j*pauli_y)/2

# Creation and annihilation operators (2*N)
c = [np.eye(1) for i in range(2*N)]
for i in range(2*N):
    for j in range(2*N):
        if i < j:
            c[i] = np.kron(pauli_x, c[i])
        elif i == j:
            c[i] = np.kron(pauli_p, c[i])
        else:
            c[i] = np.kron(pauli_i, c[i])
cdag = [x.T.conj() for x in c]

a,bdag = c[::2], c[1::2]
adag,b = cdag[::2], cdag[1::2]

H = np.zeros((D,D), dtype=np.complex128)
G = np.zeros((D,D), dtype=np.complex128)
for i in range(N):
    H += m * (adag[i]@a[i] + bdag[i]@b[i])
    H += mu * (adag[i]@a[i] - bdag[i]@b[i])
    G += adag[i]@a[i] + bdag[i]@b[i]

H -= g * G@G

vals, vecs = np.linalg.eigh(H)
print(vals)


if True:
    T = 0.5
    @np.vectorize
    def cor(t):
        rho = vecs @ np.diag(np.exp(-vals/T)) @ vecs.conj().T
        rho /= rho.trace()
        U = vecs @ np.diag(np.exp(-1j*t*vals)) @ vecs.conj().T
        return (rho @ U.conj().T @ adag[0] @ U @ a[0]).trace()

    import matplotlib.pyplot as plt

    plt.figure()
    ts = np.linspace(0,10,51)
    plt.plot(ts,cor(ts))
    plt.show()

