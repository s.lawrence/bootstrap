#!/usr/bin/env python

import picos

problem = picos.Problem(verbosity=1)

dens = picos.RealVariable(f'<n>')

prob_min = problem.clone()
prob_max = problem.clone()

prob_min.set_objective('min', dens)
prob_max.set_objective('max', dens)
sol_min = prob_min.solve(solver='mosek', primals=None, duals=None)
sol_max = prob_max.solve(solver='mosek', primals=None, duals=None)

print(sol_min.reported_value, sol_max.reported_value)

