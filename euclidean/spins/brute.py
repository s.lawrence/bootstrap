#!/usr/bin/env python

import itertools
from math import exp

L = 4
J = 1.
mu = 0.4

num1 = 0.
num2 = 0.
num2_ = 0.
num3 = 0.
Z = 0.

for s in itertools.product((-1,1),repeat=L):
    sp = s[1:] + s[:1]
    S = -mu * sum(s)
    S += -J * sum(a*b for a,b in zip(s,sp))
    Z += exp(-S)
    num1 += exp(-S) * s[0]
    num2 += exp(-S) * s[0] * s[1]
    num2_ += exp(-S) * s[0] * s[2]
    num3 += exp(-S) * s[0] * s[1] * s[2]

print(num1/Z)
print(num2/Z)
print(num2_/Z)
print(num3/Z)

