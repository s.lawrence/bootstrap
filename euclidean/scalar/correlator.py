#!/usr/bin/env python

"""
Bootstrapping a one-dimensional lattice. Attempting to bound the correlator.
"""

import itertools
import picos
import sys

L = 5
r = int(sys.argv[1])

m2 = 0.5
lamda = 0.5

ops = [(),(0,),(0,0),(0,0,0,0)]
# Two-point functions
ops += [(0,a) for a in range(1,L)]

candidates = []
candidates += [(0,0,0,a) for a in range(1,L)]
candidates += [(0,a,a,a) for a in range(1,L)]
candidates += [(0,0,a,a) for a in range(1,L)]
candidates += [(0,0,a,a) for a in range(1,L)]
candidates += [(0,0,a) for a in range(L)]
candidates += [(0,0,0,0,a) for a in range(L)]
candidates += [(0,0,0,0,0,a) for a in range(L)]
candidates += [(0,0,0,0,0,0,a) for a in range(L)]
candidates += [(0,0,0,0,0,0,0,a) for a in range(L)]
candidates += [(0,0,0,0,0,a) for a in range(1,L)]
candidates += [(0,a,a,a,a,a) for a in range(1,L)]
candidates += [(0,0,0,0,a,a) for a in range(1,L)]
candidates += [(0,0,a,a,a,a) for a in range(1,L)]
candidates += [(0,0,0,a,a,a) for a in range(1,L)]
candidates += [(0,a,b) for a in range(1,L) for b in range(a+1,L)]
candidates += [(0,0,a,b) for a in range(1,L) for b in range(a+1,L)]
candidates += [(0,a,b,c) for a in range(1,L) for b in range(a+1,L) for c in range(b+1,L)]

def bound(ops):
    """ Add all translations. """
    for op in ops:
        for x in range(L):
            op_tr = tuple(sorted((y+x)%L for y in op))
            if op_tr not in ops:
                ops.append(op_tr)

    problem = picos.Problem(verbosity=0)

    E = {op: picos.RealVariable(f'<{op}>') for op in ops}
    N = len(E)
    problem += E[()] == 1

    M = picos.SymmetricVariable('M', N)
    for i1, op1 in enumerate(ops):
        for i2, op2 in enumerate(ops):
            op = tuple(sorted(op1+op2))
            try:
                problem += M[i1,i2] == E[op]
            except:
                pass
    problem += M >> 0

    """
    Schwinger dyson equations. The action is:

      S = (∂ phi)**2 / 2 + m**2 * phi**2 / 2 + lambda * phi**4 / 4.

    The general form for an S-D equation is: <∂f> = <f∂S>.
    """
    for op in ops:
        for x in range(L):
            try:
                # Differentiate op with respect to x to get lhs.
                if op.count(x) > 0:
                    i = op.index(x)
                    lhs = op.count(x)*E[op[:i]+op[i+1:]]
                else:
                    lhs = 0
                # Potential
                rhs = m2 * E[tuple(sorted((x,)+op))]
                rhs += lamda * E[tuple(sorted((x,x,x)+op))]
                # Kinetic (up and down)
                xp = (x+1)%L
                xm = (x-1)%L
                rhs += E[tuple(sorted(op+(x,)))] - E[tuple(sorted(op+(xp,)))]
                rhs += E[tuple(sorted(op+(x,)))] - E[tuple(sorted(op+(xm,)))]
                problem += lhs == rhs
            except:
                pass

    """
    Translational invariance.
    """
    for op in ops:
        for x in range(L):
            op_tr = tuple(sorted((y+x)%L for y in op))
            problem += E[op] == E[op_tr]

    prob_min = problem.clone()
    prob_max = problem.clone()

    # Target some observable.
    prob_min.set_objective('min', E[(0,r)])
    prob_max.set_objective('max', E[(0,r)])
    sol_min = prob_min.solve(solver='mosek', primals=None, duals=None)
    sol_max = prob_max.solve(solver='mosek', primals=None, duals=None)

    return sol_min.reported_value, sol_max.reported_value


lo, hi = bound(ops)
print(f'{lo} {hi}')
while True:
    best = hi-lo + 0.5
    best_op = None
    for op in candidates:
        if op in ops:
            continue
        ops_ = ops+[op]
        lo, hi = bound(ops_)
        if hi-lo < best:
            best = hi-lo
            best_op = op
    ops.append(best_op)
    lo, hi = bound(ops)
    print(f'{lo} {hi}        (added {best_op})')

