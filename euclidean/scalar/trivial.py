#!/usr/bin/env python

"""
Bootstrapping a small, periodic lattice.
"""

import itertools
import picos
import sys

L = 4

m2 = 0.5

problem = picos.Problem(verbosity=0)
ops = [()]
# One-point functions
ops += [(a,) for a in range(L)]
ops += [(0,0)]
#ops += [(a,a) for a in range(L)]
#ops += [(a,a,a) for a in range(L)]
#ops += [(a,a,a,a) for a in range(L)]

# Two-point functions
#ops += [(a,b) for a in range(L) for b in range(a+1,L)]
ops += [(0,a) for a in range(L)]

E = {op: picos.RealVariable(f'<{op}>') for op in ops}
N = len(E)
problem += E[()] == 1

"""
Schwinger dyson equations. The action is:

  S = (∂ phi)**2 / 2 + m**2 * phi**2 / 2 + lambda * phi**4 / 4.

The general form for an S-D equation is: <∂f> = <f∂S>.
"""
for op in ops:
    for x in range(L):
        try:
            # Differentiate op with respect to x to get lhs.
            if op.count(x) > 0:
                i = op.index(x)
                lhs = op.count(x)*E[op[:i]+op[i+1:]]
            else:
                lhs = 0
            # Potential
            rhs = m2 * E[tuple(sorted((x,)+op))]
            # Kinetic (up and down)
            xp = (x+1)%L
            xm = (x-1)%L
            rhs += E[tuple(sorted(op+(x,)))] - E[tuple(sorted(op+(xp,)))]
            rhs += E[tuple(sorted(op+(x,)))] - E[tuple(sorted(op+(xm,)))]
            problem += lhs == rhs
        except:
            pass

prob_min = problem.clone()
prob_max = problem.clone()

# Target some observable.
prob_min.set_objective('min', E[(0,0)])
prob_max.set_objective('max', E[(0,0)])
sol_min = prob_min.solve(solver='mosek', primals=None, duals=None)
sol_max = prob_max.solve(solver='mosek', primals=None, duals=None)

print(sol_min.reported_value, sol_max.reported_value)

