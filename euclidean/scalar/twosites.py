#!/usr/bin/env python

"""
A two-dimensional integral.
"""

import picos
import sys

N = 7

m2 = 1.
lamda = 1.0

problem = picos.Problem(verbosity=1)
ops = [(n,m) for m in range(N) for n in range(N)]
evars = [picos.RealVariable(f'<{op[0]},{op[1]}>') for op in ops]
E = {op: evars[ops.index(op)] for op in ops}
problem += E[(0,0)] == 1.

M = picos.SymmetricVariable('M', N*N)
for n in range(N):
    for m in range(N):
        for np in range(N):
            for mp in range(N):
                i = ops.index((n,m))
                j = ops.index((np,mp))
                try:
                    problem += M[i,j] == E[(n+np,m+mp)]
                except:
                    pass
problem += M >> 0

"""
Schwinger dyson equations. The action is:

  S = (phi1-phi2)**2 / 2 + m**2 * phi**2 / 2 + lambda * phi**4 / 4.

The general form for an S-D equation is: <∂f> = <f∂S>.
"""

for n in range(N):
    for m in range(N):
        try:
            if n == 0:
                lhs = 0
            else:
                lhs = n * E[n-1,m]
            problem += lhs == E[n+1,m] - E[n,m+1] + m2*E[n+1,m] + lamda * E[n+3,m]
        except:
            pass
        try:
            if m == 0:
                lhs = 0
            else:
                lhs = m * E[n,m-1]
            problem += lhs == E[n,m+1] - E[n+1,m] + m2*E[n,m+1] + lamda * E[n,m+3]
        except:
            pass

prob_min = problem.clone()
prob_max = problem.clone()

# Target some observable.
prob_min.set_objective('min', E[(2,0)])
prob_max.set_objective('max', E[(2,0)])
sol_min = prob_min.solve(solver='mosek', primals=None, duals=None)
sol_max = prob_max.solve(solver='mosek', primals=None, duals=None)

print(sol_min.reported_value, sol_max.reported_value)


########
# EXACT

import numpy as np
from scipy.integrate import dblquad

def f(phi1, phi2):
    S = (phi1-phi2)**2/2 + m2/2*(phi1**2+phi2**2) + lamda/4*(phi1**4+phi2**4)
    return np.exp(-S)

def g(phi1, phi2):
    return phi1**2 * f(phi1,phi2)

print(dblquad(g, -6,6,-6,6)[0] / dblquad(f, -6,6,-6,6)[0])
