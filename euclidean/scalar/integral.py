#!/usr/bin/env python

import picos
import sys

for N in [4,6,8,10,12,14,16,18,20,22,24]:
    problem = picos.Problem(verbosity=0)
    phi = [picos.RealVariable(f'phi{n}') for n in range(N)]
    problem.add_constraint(phi[0] == 1.)

    M = picos.SymmetricVariable('M', N//2)
    for n in range(N//2):
        for m in range(N//2):
            problem.add_constraint(M[n,m] == phi[n+m])
    problem.add_constraint(M >> 0)

    # Schwinger dyson equations. The action is: S = phi**2 / 2 + phi**4 / 4. The
    # general form for an S-D equation is: <∂f> = <f∂S>.
    for n in range(1,N-3):
        problem.add_constraint(n*phi[n-1] == phi[n+1] + phi[n+3])


    prob_min = problem.clone()
    prob_max = problem.clone()

    # Target some observable.
    prob_min.set_objective('min', phi[2])
    prob_max.set_objective('max', phi[2])
    sol_min = prob_min.solve(solver='mosek', primals=None, duals=None)
    sol_max = prob_max.solve(solver='mosek', primals=None, duals=None)

    print(N, sol_min.reported_value, sol_max.reported_value)

import numpy as np

phi = np.linspace(-5,5,10000)
boltz = np.exp(-phi**2/2 - phi**4/4)
print(np.trapz(phi**2 * boltz,phi) / np.trapz(boltz,phi))
